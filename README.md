# `memiliflow`
This project combines [`memilio`](https://github.com/DLR-SC/memilio/) with a Python environment containing various tools for graph computation and (Bayesian) parameter estimation.


## Setting up a dev environment

To create the Python environment we recommend [`mamba`](https://mamba.readthedocs.io/en/latest/installation.html) because it's fast:

```bash
mamba create -n memilienv -f environment.yml
```

Next you can run the test suite:

```bash
pytest -v --cov=memiliflow --cov-report term-missing .
```

We standardize code style with a `pre-commit`, so please do either of the following:

```bash
pre-commit install  # just once to run automatically before every commit
# or
pre-commit run --all  # manually before making a commit
```


## Updating C++ build tools for CI

Update Dockerfile-ci and then build and upload the resulting image:

```bash
# use personal access token for logging in:
docker login registry.hzdr.de
docker build -t registry.hzdr.de/loki/memiliflow/ci -f Dockerfile-ci .
docker push registry.hzdr.de/loki/memiliflow/ci
```

## Dev-Container

```bash
docker build -t memiliflow-dev .
docker run --name memiliflow-dev-container -v $(pwd)/example:/dependencies/example -it -p 8888:8888 memiliflow-dev
```
