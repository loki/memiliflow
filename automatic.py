"""Combines automatic download of public generated data via memilio-epidata,
extrapolation into compartments via cpp code and upload to ESID database"""

import datetime
import json
import os
import subprocess
from datetime import date
from zipfile import ZipFile

import click
from memilio.epidata import getCaseData as gcd
from memilio.epidata import getDIVIData as gdd
from memilio.epidata import getPopulationData as gpd
from memilio.epidata import getVaccinationData as gvd
from memilio.epidata import progress_indicator


def read_input_data(end_date, path_to_input_data, username="", password=""):
    """! Reads data needed to compute number of individuals in compartments.

    Downloads case data, population data, DIVI data and vaccination data.

    @param[in] start_date Start date of data.
    @param[in] path_to_input_data Path where data is saved.
    """
    arg_dict = {
        "out_folder": "{}/pydata".format(path_to_input_data),
        "end_date": end_date,
        "moving_average": 7,
    }
    arg_dict_cases = {
        "out_folder": "{}/pydata".format(path_to_input_data),
        "end_date": end_date,
        "moving_average": 7,
    }
    arg_dict_pop = {
        "out_folder": "{}/pydata".format(path_to_input_data),
        "username": "",
        "password": "",
    }

    progress_indicator.ProgressIndicator.disable_indicators(True)
    gcd.get_case_data(**arg_dict_cases)
    # gpd.get_population_data(**arg_dict_pop)
    gdd.get_divi_data(**arg_dict)
    gvd.get_vaccination_data(**arg_dict)


def compute_compartments_from_input_data(start_date, path_to_data, num_days_sim):
    """! Executes script to compute compartment data.

    Creates 'Results.h5' and 'Results_sum.h5' and store them in path_to_data.

    @param[in] start_date Start date of extrapolation.
    @param[in] path_to_data Path where data is saved.
    @param[in] num_days_sim Number of days that data is extrapolated.
    """
    year = start_date.year
    month = start_date.month
    day = start_date.day
    run = subprocess.run(
        [
            "build/bin/generate_extrapolated {} {} {} {} {}".format(
                path_to_data, str(year), str(month), str(day), str(num_days_sim)
            )
        ],
        shell=True,
        check=True,
        capture_output=True,
        text=True,
    )
    print("STDOUT:")
    print(run.stdout)
    print("STDERR:")
    print(run.stderr)


def run_baseline_simulation(start_date, path_to_data, num_days_sim, num_runs=5):
    """! Executes script to compute compartment data.

    Creates 'Results.h5' and 'Results_sum.h5' and store them in path_to_input_data.

    @param[in] start_date Start date of simulation.
    @param[in] path_to_data Path where data is saved.
    @param[in] num_days_sim Number of days that is simulated.
    """
    year = start_date.year
    month = start_date.month
    day = start_date.day
    run = subprocess.run(
        [
            "build/bin/simulate_scenario {} {} {} {} {} {}".format(
                path_to_data, str(year), str(month), str(day), str(num_days_sim), str(num_runs)
            )
        ],
        shell=True,
        check=True,
        capture_output=True,
        text=True,
    )
    print("STDOUT:")
    print(run.stdout)
    print("STDERR:")
    print(run.stderr)


def prepare_case_data_for_backend(start_date, path_to_data):
    """! Prepares case data for backend.

    Creates metadata.json and zip-folder containing metadata.json, Results.h5
    and Results_sum.h5.

    @param[in] start_date Start date of extrapolated data.
    @param[in] path_to_data Path to data.
    """
    # Define dict with metadata
    metadata_dict = {
        "startDay": "{}".format(start_date.strftime("%Y-%m-%d")),
        "datasets": ["Group1", "Group2", "Group3", "Group4", "Group5", "Group6", "Total"],
        "groupMapping": {
            "Group1": "age_0",
            "Group2": "age_1",
            "Group3": "age_2",
            "Group4": "age_3",
            "Group5": "age_4",
            "Group6": "age_5",
            "Total": "total",
        },
        "compartmentOrder": ["MildInfections", "Hospitalized", "ICU", "Dead"],
    }

    # write metadata.json
    with open(os.path.join(path_to_data, "metadata_rki.json"), "w") as outfile:
        json.dump(metadata_dict, outfile, indent=4, sort_keys=False)

    # make zip
    with ZipFile(os.path.join(path_to_data, "rki.zip"), "w") as zip_object:
        zip_object.write(os.path.join(path_to_data, "metadata_rki.json"), "metadata.json")
        zip_object.write(os.path.join(path_to_data, "Results.h5"), "Results.h5")
        zip_object.write(os.path.join(path_to_data, "Results_sum.h5"), "Results_sum.h5")


def prepare_simulation_data_for_backend(
    meta_data, start_date_baseline, num_days_sim_baseline, path_to_data
):
    """! Prepares simulation data for backend.

    Creates metadata.json and zip-folder containing metadata.json and folders with 25, 50 and 75
    percentiles with Results.h5, Results_sum.h5 and GraphNodes.

    @param[in] start_date_baseline Start date of baseline scenario.
    @param[in] num_days_sim_baseline Number of daysthat are simulated in baseline scenario.
    @param[in] path_to_data Path to data.
    """

    # Define dict with metadata for baseline simulation
    baseline_metadata_dict = {
        "key": meta_data["key"],
        "name": meta_data["name"],
        "description": meta_data["description"],
        "scenario": meta_data["scenario"],
        "startDay": "{}".format(start_date_baseline.strftime("%Y-%m-%d")),
        "numberOfDays": num_days_sim_baseline,
        "datasets": ["Group1", "Group2", "Group3", "Group4", "Group5", "Group6", "Total"],
        "groupMapping": {
            "Group1": "age_0",
            "Group2": "age_1",
            "Group3": "age_2",
            "Group4": "age_3",
            "Group5": "age_4",
            "Group6": "age_5",
            "Total": "total",
        },
        "compartmentOrder": ["MildInfections", "Hospitalized", "ICU", "Dead"],
    }

    # write metadata.json
    with open(os.path.join(path_to_data, "metadata_baseline.json"), "w") as outfile:
        json.dump(baseline_metadata_dict, outfile, indent=4, sort_keys=False)

    # make zip
    with ZipFile(os.path.join(path_to_data, "baseline_simulation.zip"), "w") as zip_object:
        zip_object.write(os.path.join(path_to_data, "metadata_baseline.json"), "metadata.json")
        # percentile 5
        for file in os.listdir(os.path.join(path_to_data, "p05")):
            zip_object.write(
                os.path.join(path_to_data, "p05", file),
                os.path.join(os.path.dirname(path_to_data), "5", file),
            )
        # percentile 15
        for file in os.listdir(os.path.join(path_to_data, "p15")):
            zip_object.write(
                os.path.join(path_to_data, "p15", file),
                os.path.join(os.path.dirname(path_to_data), "15", file),
            )
        # percentile 25
        for file in os.listdir(os.path.join(path_to_data, "p25")):
            zip_object.write(
                os.path.join(path_to_data, "p25", file),
                os.path.join(os.path.dirname(path_to_data), "25", file),
            )
        # percentile 50
        for file in os.listdir(os.path.join(path_to_data, "p50")):
            zip_object.write(
                os.path.join(path_to_data, "p50", file),
                os.path.join(os.path.dirname(path_to_data), "50", file),
            )
        # percentile 75
        for file in os.listdir(os.path.join(path_to_data, "p75")):
            zip_object.write(
                os.path.join(path_to_data, "p75", file),
                os.path.join(os.path.dirname(path_to_data), "75", file),
            )
        # percentile 85
        for file in os.listdir(os.path.join(path_to_data, "p85")):
            zip_object.write(
                os.path.join(path_to_data, "p85", file),
                os.path.join(os.path.dirname(path_to_data), "85", file),
            )
        # percentile 95
        for file in os.listdir(os.path.join(path_to_data, "p95")):
            zip_object.write(
                os.path.join(path_to_data, "p95", file),
                os.path.join(os.path.dirname(path_to_data), "95", file),
            )

def prepare_case_data_for_new_backend(path_to_data):
    """! Prepares case data for backend.

    Creates metadata.json and zip-folder containing metadata.json, Results.h5
    and Results_sum.h5.

    @param[in] path_to_data Path to data.
    """

    # make zip
    with ZipFile(os.path.join(path_to_data, "casedata.zip"), "w") as zip_object:
        zip_object.write(os.path.join(
            path_to_data,  "Results.h5"), "50/Results.h5")
        zip_object.write(os.path.join(
            path_to_data, "Results_sum.h5"), "50/Results_sum.h5")


def prepare_simulation_data_for_new_backend(path_to_data):
    """! Prepares simulation data for backend.

    Creates metadata.json and zip-folder containing metadata.json and folders with 25, 50 and 75
    percentiles with Results.h5, Results_sum.h5 and GraphNodes.

    @param[in] path_to_data Path to data.
    """
    # make zip
    with ZipFile(os.path.join(path_to_data, "simulation_results.zip"), "w") as zip_object:
        # percentile 25
        for file in os.listdir(os.path.join(
                path_to_data, "p25")):
            zip_object.write(os.path.join(
                path_to_data, "p25", file), os.path.join(
                os.path.dirname(path_to_data), "25", file))
        # percentile 50
        for file in os.listdir(os.path.join(
                path_to_data, "p50")):
            zip_object.write(os.path.join(
                path_to_data, "p50", file), os.path.join(
                os.path.dirname(path_to_data), "50", file))
        # percentile 75
        for file in os.listdir(os.path.join(
                path_to_data, "p75")):
            zip_object.write(os.path.join(
                path_to_data, "p75", file), os.path.join(
                os.path.dirname(path_to_data), "75", file))

def import_rki_data_to_backend(path_to_esid, path_to_data):
    """! Imports case data into backend.

    First, copy output data in backend folder from ESID.
    Then, import it into database.

    @param[in] path_to_esid Path to ESID repository.
    @param[in] path_to_data Path to data.
    """

    path_to_backend = os.path.join(path_to_esid, "backend")

    run = subprocess.run(
        ["cp -f {}/rki.zip {}/rki.zip".format(path_to_data, path_to_backend)],
        shell=True,
        check=True,
        capture_output=True,
        text=True,
    )
    os.chdir(path_to_backend)
    run = subprocess.run(
        ["USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose -f docker-compose.dev.yml up -d"],
        shell=True,
        check=True,
        capture_output=True,
        text=True,
    )

    run = subprocess.run(
        [
            "USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose -f docker-compose.dev.yml run --rm backend python manage.py import_rki rki.zip"
        ],
        shell=True,
        check=True,
        capture_output=True,
        text=True,
    )

    print("STDOUT:")
    print(run.stdout)
    print("STDERR:")
    print(run.stderr)


def import_simulation_data_to_backend(path_to_esid, path_to_data):
    """! Imports simulation data into backend.

    First, copy data in backend folder from ESID.
    Then, import it into database.

    @param[in] path_to_esid Path to ESID repository.
    @param[in] path_to_data Path to data.
    """

    path_to_backend = os.path.join(path_to_esid, "backend")

    run = subprocess.run(
        [
            "cp -f {}/baseline_simulation.zip {}/baseline_simulation.zip".format(
                path_to_data, path_to_backend
            )
        ],
        shell=True,
        # check=True,
        # capture_output=True,
        # text=True,
    )

    # # the baseline.json needs to be saved in the ESID/backend folder, but this need to be done only once
    # run = subprocess.run(
    #     ["cp -f {}/baseline.json {}/baseline.json".format(
    #         path_to_data, path_to_backend)],
    #     shell=True,
    #     check=True,
    #     capture_output=True,
    #     text=True,
    # )

    os.chdir(path_to_backend)

    # run = subprocess.run(
    #     ["USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose -f docker-compose.dev.yml up -d"],
    #     shell=True,
    #     check=True,
    #     capture_output=True,
    #     text=True,
    # )

    # # this only needs to be run once for each simulation and not every day when we get new data
    # run = subprocess.run(
    #     [
    #         "USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose -f docker-compose.dev.yml run --rm backend python manage.py import_scenario baseline.json"
    #     ],
    #     shell=True,
    #     check=True,
    #     capture_output=True,
    #     text=True,
    # )

    run = subprocess.Popen(
        [
            "USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose -f docker-compose.dev.yml run --rm backend python manage.py import_simulation baseline_simulation.zip"
        ],
        shell=True,
        text=True,
        stdin=subprocess.PIPE,
        # stdout=subprocess.PIPE,
        # stderr=subprocess.PIPE,
    )
    run2 = run.communicate(input="1")

    # print("STDOUT:")
    # print(run.stdout)
    print("STDERR:")
    print(run.stderr)


def main():
    """Main program to execute all steps."""
    # Number of days to take into account
    num_days_sim = 10
    num_days_sim_baseline = 5

    # Set start date relative to current date
    # Add 1 because the data we fetch today only contains data until yesterday,
    # so start_date needs to be computed wrt to yesterday
    start_date = date.today() - datetime.timedelta(days=num_days_sim + 1)

    start_date_baseline = date.today() - datetime.timedelta(days=num_days_sim_baseline + 1)
    # Set end_date to yesterday because we can only fetch data from the day before
    end_date = date.today() - datetime.timedelta(days=1)

    # Set paths to folder where we store input and output data
    # Attention: At the moment, we read from and save to the same directory
    path_to_data = "./data"

    # Set path to ESID
    path_to_esid = "/localdata1/wend_aa/ESID"

    # Steps of pipeline

    # Download data
    # read_input_data(end_date, path_to_data)

    # Extrapolate case data und upload to ESID
    # compute_compartments_from_input_data(
    #     start_date, path_to_data, num_days_sim)

    # prepare_case_data_for_backend(
    #     start_date, path_to_data)

    # import_rki_data_to_backend(path_to_esid, path_to_data)

    # Run baseline scenario and upload to ESID
    run_baseline_simulation(start_date_baseline, path_to_data, num_days_sim_baseline)

    prepare_simulation_data_for_backend(start_date_baseline, num_days_sim_baseline, path_to_data)

    import_simulation_data_to_backend(path_to_esid, path_to_data)


if __name__ == "__main__":

    main()
