FROM registry.hzdr.de/loki/memiliflow/ci

USER root
# creates directories for xcom logs and other stuff
RUN mkdir /dependencies && mkdir /airflow && mkdir /airflow/xcom && chmod -R 777 /airflow && chown -R $MAMBA_USER:$MAMBA_USER /dependencies
USER $MAMBA_USER

ARG MAMBA_DOCKERFILE_ACTIVATE=1
ENV PYTHONPATH="${PYTHONPATH}:/dependencies"

# TODO: move automatic to pipeline in memiliflow
COPY environment.yml automatic.py .memilio_commit_hash install_memilio.sh /dependencies/
COPY examples /dependencies/examples
COPY memiliflow /dependencies/memiliflow
COPY tests /dependencies/tests

WORKDIR /dependencies

# activate micromamba such that memilio python packages are directly installed into the correct env
# While pyross is incompatible with newer numpy versions, we have to install an older one.
# Also twill breaks population download of epidate in version 3.2
# urllib3==1.26.18 fixes issue in boto3, see https://github.com/boto/botocore/issues/3111
RUN cp /dependencies/environment.yml /tmp/env.yaml && \
    eval "$(micromamba shell hook --shell=bash)" && \
    micromamba install -y -n base -f /tmp/env.yaml && \
    micromamba clean --all --yes && \
    micromamba activate base && \
    ./install_memilio.sh -c "$(<.memilio_commit_hash)" && \
    python -m pip install torch --index-url https://download.pytorch.org/whl/cpu && \
    python -m pip install numpy==1.25.2 twill==3.1 urllib3==1.26.18 sbi && \
    mv /dependencies/automatic.py /dependencies/memilio_src && \
    micromamba clean -a && \
    pip cache purge && \
    pytest -v --cov=memiliflow --cov-report term-missing ./memiliflow ./tests
