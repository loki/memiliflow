from datetime import date, timedelta
from pathlib import Path

import numpy as np
import pandas as pd
import pytest
from memiliflow.data.case_data import CaseData


class TestCaseData:
    def test_from_dir_parses_json(self):
        # Tests whether CaseData can be correctly read from the format it is downloaded to
        expected_case_data = {
            "cases_all_age": CaseData(
                pd.read_parquet(
                    Path(__file__).parent.resolve()
                    / Path("resources/Germany/cases_all_age.parquet")
                )
            ),
            "cases_all_age_ma7": CaseData(
                pd.read_parquet(
                    Path(__file__).parent.resolve()
                    / Path("resources/Germany/cases_all_age_ma7.parquet")
                )
            ),
            "zensus": CaseData(
                pd.read_parquet(
                    Path(__file__).parent.resolve() / Path("resources/Germany/zensus.parquet")
                )
            ),
        }

        actual_case_data = CaseData.read_json_from_dir(
            data_dir=Path(__file__).parent.resolve() / Path("resources/Germany")
        )

        assert actual_case_data == expected_case_data

    def test_from_dir_parses_specified_json(self):
        # Tests whether CaseData can be correctly read from the format it is downloaded to
        expected_case_data = {
            "cases_all_age": CaseData(
                pd.read_parquet(
                    Path(__file__).parent.resolve()
                    / Path("resources/Germany/cases_all_age.parquet")
                )
            )
        }

        actual_case_data = CaseData.read_json_from_dir(
            data_dir=Path(__file__).parent.resolve() / Path("resources/Germany"),
            keys=["cases_all_age"],
        )

        assert actual_case_data == expected_case_data

    def test_case_data_to_oseir_compartments(self):
        start_date = date.fromisoformat("2020-04-01")
        num_days = 100
        confirmed_per_day = 5

        time_infected = 10
        time_exposed = 5
        death_probability = 0.01

        expected_compartments = pd.DataFrame.from_dict(
            data={
                CaseData.Label.DATE.value: [
                    str(start_date + timedelta(days=x)) for x in range(num_days)
                ],
                "COMPARTMENT_EXPOSED": [
                    float(confirmed_per_day * time_exposed) for x in range(num_days - time_exposed)
                ]
                + [np.NaN for _ in range(time_exposed)],
                "COMPARTMENT_INFECTED": [np.NaN for _ in range(time_infected)]
                + [
                    float(confirmed_per_day * time_infected)
                    for x in range(num_days - time_infected)
                ],
                "COMPARTMENT_RECOVERED": [
                    float(max(0, confirmed_per_day * (x - (time_exposed + time_infected))))
                    for x in range(num_days)
                ],
            }
        )

        print(
            [
                death_probability * max(0, confirmed_per_day * (x - (time_exposed + time_infected)))
                for x in range(num_days)
            ]
        )

        simulated_data = {
            CaseData.Label.DATE.value: [
                str(start_date + timedelta(days=x)) for x in range(num_days)
            ],
            CaseData.Label.CONFIRMED.value: [confirmed_per_day * x for x in range(num_days)],
            CaseData.Label.RECOVERED.value: [
                (1 - death_probability)
                * max(0, confirmed_per_day * (x - (time_exposed + time_infected)))
                for x in range(num_days)
            ],
            CaseData.Label.DEATHS.value: [
                death_probability * max(0, confirmed_per_day * (x - (time_exposed + time_infected)))
                for x in range(num_days)
            ],
        }

        case_data = CaseData(pd.DataFrame.from_dict(data=simulated_data))

        actual_compartments = case_data.to_oseir_compartments(
            time_exposed=time_exposed, time_infected=time_infected
        )

        pd.testing.assert_frame_equal(actual_compartments, expected_compartments)

    def test_case_data_to_pyross_oseir_array(self):
        start_date = date.fromisoformat("2020-04-01")
        num_days = 100
        confirmed_per_day = 5

        time_infected = 10
        time_exposed = 5
        death_probability = 0.01

        expected_compartments = pd.DataFrame.from_dict(
            data={
                CaseData.Label.DATE.value: [
                    str(start_date + timedelta(days=x)) for x in range(num_days)
                ],
                "COMPARTMENT_EXPOSED": [
                    float(confirmed_per_day * time_exposed) for x in range(num_days - time_exposed)
                ]
                + [np.NaN for _ in range(time_exposed)],
                "COMPARTMENT_INFECTED": [np.NaN for _ in range(time_infected)]
                + [
                    float(confirmed_per_day * time_infected)
                    for x in range(num_days - time_infected)
                ],
                "COMPARTMENT_RECOVERED": [
                    float(max(0, confirmed_per_day * (x - (time_exposed + time_infected))))
                    for x in range(num_days)
                ],
            }
        )
        expected_compartments["COMPARTMENT_SUSCEPTIBLE"] = (
            83e6
            - expected_compartments["COMPARTMENT_EXPOSED"]
            - expected_compartments["COMPARTMENT_INFECTED"]
            - expected_compartments["COMPARTMENT_RECOVERED"]
        )

        expected_compartments = expected_compartments[
            [
                "COMPARTMENT_SUSCEPTIBLE",
                "COMPARTMENT_EXPOSED",
                "COMPARTMENT_INFECTED",
                "COMPARTMENT_RECOVERED",
            ]
        ].to_numpy()

        simulated_data = {
            CaseData.Label.DATE.value: [
                str(start_date + timedelta(days=x)) for x in range(num_days)
            ],
            CaseData.Label.CONFIRMED.value: [confirmed_per_day * x for x in range(num_days)],
            CaseData.Label.RECOVERED.value: [
                (1 - death_probability)
                * max(0, confirmed_per_day * (x - (time_exposed + time_infected)))
                for x in range(num_days)
            ],
            CaseData.Label.DEATHS.value: [
                death_probability * max(0, confirmed_per_day * (x - (time_exposed + time_infected)))
                for x in range(num_days)
            ],
        }

        case_data = CaseData(pd.DataFrame.from_dict(data=simulated_data))

        actual_compartments = case_data.to_pyross_oseir_array(
            total_population=83e6,
            time_exposed=time_exposed,
            time_infected=time_infected,
        )

        np.testing.assert_almost_equal(actual_compartments, expected_compartments)

    def test_case_data_eq_false_when_type_missmatch(self):
        case_data = CaseData(
            {
                "cases_all_age": pd.read_parquet(
                    Path(__file__).parent.resolve()
                    / Path("resources/Germany/cases_all_age.parquet")
                ),
                "cases_all_age_ma7": pd.read_parquet(
                    Path(__file__).parent.resolve()
                    / Path("resources/Germany/cases_all_age_ma7.parquet")
                ),
                "zensus": pd.read_parquet(
                    Path(__file__).parent.resolve() / Path("resources/Germany/zensus.parquet")
                ),
            }
        )
        assert not case_data == 5

    def test_download_raises_when_data_dir_is_missing(self):
        with pytest.raises(FileNotFoundError):
            CaseData.download_to_filesystem(
                "./non-existing-dir", start_date=date.today(), end_date=date.today()
            )

    def test_download_raises_for_unsupported_file_format(self):
        with pytest.raises(ValueError):
            CaseData.download_to_filesystem(
                "./non-existing-dir",
                start_date=date.today(),
                end_date=date.today(),
                file_format="jpeg",
            )
