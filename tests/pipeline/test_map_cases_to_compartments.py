import tempfile
from pathlib import Path

from click.testing import CliRunner
from memiliflow.pipeline.map_cases_to_compartments_local import (
    map_cases_to_compartments,
)


class TestDownload:
    def test_data_is_mapped(self):
        input_dir = Path(__file__).parent.resolve() / Path("resources")
        runner = CliRunner()
        with tempfile.TemporaryDirectory() as tmp_dir:
            params = [
                "--input_dir",
                input_dir,
                "--output_dir",
                tmp_dir,
                "--execution_date",
                "2023-02-23",
            ]
            result = runner.invoke(map_cases_to_compartments, params)
            print("result.stdout")
            print(result.stdout)

            assert result.exit_code == 0
            expected_mapped_data = Path(tmp_dir, "mapped_compartments.json")
            assert expected_mapped_data.exists()

    def test_exception_is_thrown_when_case_data_does_not_exist(self):
        input_dir = Path("non_existing_dir")
        runner = CliRunner()
        params = [
            "--input_dir",
            input_dir,
            "--output_dir",
            "doesnt-matter",
            "--execution_date",
            "2023-02-23",
        ]
        result = runner.invoke(map_cases_to_compartments, params)
        print(result.stdout)

        assert result.exit_code == 1
        assert type(result.exception) == FileNotFoundError
        assert (
            str(result.exception) == "Data_dir non_existing_dir/2023-02-23/Germany does not exist."
        )
