# import tempfile
# from datetime import date
# from pathlib import Path
#
# from click.testing import CliRunner
# from memiliflow.pipeline.download_local import download
#
#
# class TestDownload:
#     def test_data_is_downloaded(self):
#         with tempfile.TemporaryDirectory() as tmp_dir:
#             runner = CliRunner()
#             data_dir = Path(tmp_dir, str(date.today()))
#             params = [
#                 "--data_dir",
#                 tmp_dir,
#                 "--execution_date",
#                 str(date.today()),
#                 "--start_date",
#                 "2020-06-01",
#                 "--end_date",
#                 "2020-06-07",
#                 "--moving_average",
#                 "7",
#             ]
#             result = runner.invoke(download, params)
#
#             assert result.exit_code == 0
#             expected_data_path = data_dir / Path("Germany")
#             assert expected_data_path.exists()
#             # Many files will be downloaded. Here we assert that the most important ones exist
#             assert (expected_data_path / Path("cases_all_germany_ma7.json")).exists()
#             # assert (expected_data_path / Path("cases_all_germany.json")).exists()
#
#     def test_exception_thrown_when_data_dir_does_not_exist(self):
#         tmp_dir = "non_existing_dir"
#
#         runner = CliRunner()
#         params = [
#             "--data_dir",
#             tmp_dir,
#             "--execution_date",
#             str(date.today()),
#             "--start_date",
#             "2020-06-01",
#             "--end_date",
#             "2020-06-07",
#         ]
#         result = runner.invoke(download, params)
#
#         assert result.exit_code == 1
#         assert type(result.exception) == FileNotFoundError
#         assert str(result.exception) == "Data_dir non_existing_dir does not exist."
