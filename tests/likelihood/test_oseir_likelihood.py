# deprecated
# import time
#
# import numpy as np
# import pytest
# from memiliflow.likelihood.oseir_likelihood import (
#     make_memilio_oseir_SDE_approx_likelihood,
#     make_py_oseir_SDE_approx_likelihood,
# )
# from memiliflow.memilio.oseir_builder import OseirBuilder
# from memiliflow.memilio.oseir_interface import OseirInterface
#
#
# class TestOseirLikelihood:
#     observations = np.array(
#         [
#             [
#                 0.00000000e00,
#                 1.00000000e00,
#                 2.00000000e00,
#                 3.00000000e00,
#                 4.00000000e00,
#                 5.00000000e00,
#                 6.00000000e00,
#                 7.00000000e00,
#                 8.00000000e00,
#                 9.00000000e00,
#                 1.00000000e01,
#             ],
#             [
#                 8.00000000e08,
#                 7.99974430e08,
#                 7.99947800e08,
#                 7.99921183e08,
#                 7.99891616e08,
#                 7.99862092e08,
#                 7.99832577e08,
#                 7.99799748e08,
#                 7.99767297e08,
#                 7.99733809e08,
#                 7.99700308e08,
#             ],
#             [
#                 1.00000000e05,
#                 1.05436000e05,
#                 1.10548000e05,
#                 1.15925000e05,
#                 1.21706000e05,
#                 1.26753000e05,
#                 1.32015000e05,
#                 1.37970000e05,
#                 1.43827000e05,
#                 1.49548000e05,
#                 1.55326000e05,
#             ],
#             [
#                 1.00000000e05,
#                 1.03089000e05,
#                 1.06801000e05,
#                 1.10623000e05,
#                 1.14866000e05,
#                 1.19617000e05,
#                 1.23923000e05,
#                 1.28836000e05,
#                 1.33673000e05,
#                 1.38980000e05,
#                 1.44198000e05,
#             ],
#             [
#                 1.00000000e03,
#                 1.80450000e04,
#                 3.58510000e04,
#                 5.32690000e04,
#                 7.28120000e04,
#                 9.25380000e04,
#                 1.12485000e05,
#                 1.34446000e05,
#                 1.56203000e05,
#                 1.78663000e05,
#                 2.01168000e05,
#             ],
#         ]
#     )
#
#     parameters = np.array([5.2, 6, 0.25])
#     total_population = 8e8 + 1e5 + 1e5 + 1e3
#
#     contact_matrix = np.array([[1]])
#
#     @pytest.mark.skip(reason="Depracted, this likelihood is now part of MEmilio")
#     def test_make_py_oseir_likelihood(self):
#         expected_likelihood = -447.35373837411612  # memilio value
#         builder = OseirBuilder(
#             *self.parameters,
#             population=self.total_population,
#             initial_exposed=1e5,
#             initial_infected=1e5,
#             initial_recovered=1e3,
#         )
#         memilio_interface = OseirInterface(builder)
#
#         ode_solver_method = "LSODA"
#         likelihood_function = make_py_oseir_SDE_approx_likelihood(
#             memilio_interface,
#             self.observations,
#             ode_solver_method=ode_solver_method,
#             simulation_step=1e-2,
#         )
#
#         actual_likelihood = likelihood_function(self.parameters)
#         # Less than 1% deviation from c++ memilio is measured.
#         assert (actual_likelihood - expected_likelihood) / np.min(
#             [actual_likelihood, expected_likelihood]
#         ) < 0.01
#
#     def test_make_oseir_likelihood(self):
#         expected_likelihood = -447.35373837411612  # memilio value
#         builder = OseirBuilder(
#             *self.parameters,
#             population=self.total_population,
#             initial_exposed=1e5,
#             initial_infected=1e5,
#             initial_recovered=1e3,
#         )
#         memilio_interface = OseirInterface(builder)
#
#         likelihood_function = make_memilio_oseir_SDE_approx_likelihood(
#             memilio_interface, self.observations
#         )
#         actual_likelihood = likelihood_function(self.parameters)
#         assert (actual_likelihood - expected_likelihood) / np.min(
#             [actual_likelihood, expected_likelihood]
#         ) < 0.01
#
#     @pytest.mark.skip(reason="Depracted, this likelihood is now part of MEmilio")
#     def test_make_py_oseir_likelihood_benchmark(self):
#         builder = OseirBuilder(
#             *self.parameters,
#             population=self.total_population,
#             initial_exposed=1e5,
#             initial_infected=1e5,
#             initial_recovered=1e3,
#         )
#         memilio_interface = OseirInterface(builder)
#
#         start = time.time()
#         ode_solver_method = "RK45"
#         for i in range(10):
#             actual_likelihood = make_py_oseir_SDE_approx_likelihood(
#                 memilio_interface, self.observations, ode_solver_method=ode_solver_method
#             )(self.parameters)
#         end = time.time()
#         print("\npython implementation: 10 simulations took", end - start)
#
#     def test_make_oseir_likelihood_benchmark(self):
#         expected_likelihood = 447.35373837411612  # memilio value
#         builder = OseirBuilder(
#             *self.parameters,
#             population=self.total_population,
#             initial_exposed=1e5,
#             initial_infected=1e5,
#             initial_recovered=1e3,
#         )
#         memilio_interface = OseirInterface(builder)
#
#         start = time.time()
#         for i in range(10):
#             actual_likelihood = make_memilio_oseir_SDE_approx_likelihood(
#                 memilio_interface, self.observations
#             )(self.parameters)
#         end = time.time()
#         print("\ncpp bindings: 10 simulations took", end - start)
