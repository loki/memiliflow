import memiliflow


def test_has_version():
    assert hasattr(memiliflow, "__version__")
    assert isinstance(memiliflow.__version__, str)


def test_dependency_imports():
    import arviz
    import boto3
    import matplotlib
    import pandas
    import pymc
