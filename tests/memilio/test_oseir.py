import time

import numpy as np
import pytest
from memiliflow.memilio.oseir import Oseir


class TestOseir:
    def test_numpy_to_dict_compartments(self):
        expected = {
            Oseir.Compartment.SUSCEPTIBLE: 1,
            Oseir.Compartment.EXPOSED: 2,
            Oseir.Compartment.INFECTED: 3,
            Oseir.Compartment.RECOVERED: 4,
        }
        compartment_array = np.array([1, 2, 3, 4])
        actual = Oseir.numpy_to_dict(compartment_array, Oseir.Compartment)

        assert actual == expected

    def test_numpy_to_dict_parameters(self):
        expected = {
            Oseir.Parameter.TIME_EXPOSED: 1.0,
            Oseir.Parameter.TIME_INFECTED: 2.0,
            Oseir.Parameter.TRANSMISSION_PROBABILITY: 3.0,
            Oseir.Parameter.CONTACT_BASELINE: 4.0,
            Oseir.Parameter.CONTACT_MINIMUM: 5.0,
            Oseir.Parameter.CONTACT_DAMPING: 6.0,
        }
        compartment_array = np.array([1, 2, 3, 4, 5, 6])
        actual = Oseir.numpy_to_dict(compartment_array, Oseir.Parameter)

        assert actual == expected

    def test_call_simulation_1e3_times(self):
        initial_condition = np.array(
            [
                10000,  # Susceptible
                9000,  # Exposed
                8000,  # Infected
                7000,  # Recovered
            ]
        )
        start = time.perf_counter()
        for i in range(int(1e3)):
            parameters = np.array(
                [
                    np.random.uniform(1.0, 10.0),  # TimeExposed
                    np.random.uniform(1.0, 10.0),  # TimeInfected
                    np.random.uniform(0.1, 1),  # TransmissionProb
                    np.random.uniform(0.1, 1),  # Contact baseline
                    np.random.uniform(0.1, 1),  # Contact minimum
                    np.random.uniform(0.1, 1),
                ]
            )  # Contact damping

            result = Oseir.simulate(initial_condition=initial_condition, parameters=parameters)
        end = time.perf_counter()

    def test_call_simulation_with_invalid_parameters(self):
        initial_condition = np.array(
            [
                10000,  # Susceptible
                9000,  # Exposed
                8000,  # Infected
                7000,  # Recovered
            ]
        )
        parameters = np.array(
            [
                -1,  # TimeExposed
                1,  # TimeInfected
                1,  # TransmissionProb
                1,  # Contact baseline
                1,  # Contact minimum
                1,
            ]
        )  # Contact dampling

        result = Oseir.simulate(initial_condition=initial_condition, parameters=parameters)
        print(result)
