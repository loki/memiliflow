import time

import numpy as np
import pandas as pd
import pytest
from memiliflow.memilio.secirvvs import Secirvvs
from memilio.simulation.osecirvvs import InfectionState


class TestSecirvvs:
    def test_call_simulation_1e3_times(self):
        duration = 100
        names = [
            "SusceptibleNaive",
            "SusceptiblePartialImmunity",
            "SusceptibleImprovedImmunity",
            "ExposedNaive",
            "ExposedPartialImmunity",
            "ExposedImprovedImmunity",
            "InfectedNoSymptomsNaive",
            "InfectedNoSymptomsPartialImmunity",
            "InfectedNoSymptomsImprovedImmunity",
            "InfectedNoSymptomsNaiveConfirmed",
            "InfectedNoSymptomsPartialImmunityConfirmed",
            "InfectedNoSymptomsImprovedImmunityConfirmed",
            "InfectedSymptomsNaive",
            "InfectedSymptomsPartialImmunity",
            "InfectedSymptomsImprovedImmunity",
            "InfectedSymptomsNaiveConfirmed",
            "InfectedSymptomsPartialImmunityConfirmed",
            "InfectedSymptomsImprovedImmunityConfirmed",
            "InfectedSevereNaive",
            "InfectedSeverePartialImmunity",
            "InfectedSevereImprovedImmunity",
            "InfectedCriticalNaive",
            "InfectedCriticalPartialImmunity",
            "InfectedCriticalImprovedImmunity",
            "DeadNaive",
            "DeadPartialImmunity",
            "DeadImprovedImmunity",
        ]
        initial_condition = np.array(
            [
                1,  # SusceptibleNaive
                2,  # SusceptiblePartialImmunity
                3,  # SusceptibleImprovedImmunity
                4,  # ExposedNaive
                5,  # ExposedPartialImmunity
                6,  # ExposedImprovedImmunity
                7,  # InfectedNoSymptomsNaive
                8,  # InfectedNoSymptomsPartialImmunity
                9,  # InfectedNoSymptomsImprovedImmunity
                10,  # InfectedNoSymptomsNaiveConfirmed
                11,  # InfectedNoSymptomsPartialImmunityConfirmed
                12,  # InfectedNoSymptomsImprovedImmunityConfirmed
                13,  # InfectedSymptomsNaive
                14,  # InfectedSymptomsPartialImmunity
                15,  # InfectedSymptomsImprovedImmunity
                16,  # InfectedSymptomsNaiveConfirmed
                17,  # InfectedSymptomsPartialImmunityConfirmed
                18,  # InfectedSymptomsImprovedImmunityConfirmed
                19,  # InfectedSevereNaive
                20,  # InfectedSeverePartialImmunity
                21,  # InfectedSevereImprovedImmunity
                22,  # InfectedCriticalNaive
                23,  # InfectedCriticalPartialImmunity
                24,  # InfectedCriticalImprovedImmunity
                25,  # DeadNaive
                26,  # DeadPartialImmunity
                27,  # DeadImprovedImmunity
            ]
        )

        start = time.perf_counter()
        for i in range(1000):
            parameters = np.array(
                [
                    np.random.uniform(4.3, 8),  # Time Exposed
                    np.random.uniform(7.0, 10.0),  # time infected no symptoms
                    np.random.uniform(7.0, 10.0),  # time infected symptoms
                    np.random.uniform(10, 20),  # tim infected severe
                    np.random.uniform(20, 30),  # time infected critical
                    np.random.uniform(0.05, 0.25),  # transmission prob
                    np.random.uniform(0.5, 0.8),  # relative transmission no symptoms
                    np.random.uniform(0.3, 0.8),  # risk of infection symptomatic
                    np.random.uniform(0.5, 0.8),  # recovered per infected no symptoms
                    np.random.uniform(0.05, 0.1),  # severe per infected symptoms
                    np.random.uniform(0.05, 0.1),  # critical per severe
                    np.random.uniform(0.05, 0.1),  # deaths per critical
                    np.random.uniform(0.05, 0.1),  # reduc exposed partial immunity
                    np.random.uniform(0.05, 0.1),  # reduc exposed improved immunity
                    1,  # reducTimeInfectedMild (fixed to either 1 or 0.5
                    0.2,  # seasonality
                    np.random.uniform(0.05, 0.1),  # reduc infected partial immunity
                    np.random.uniform(0.05, 0.1),  # reduc infected improved immunity
                    np.random.uniform(0.05, 0.1),  # reduc critical severe dead partial immunity
                    np.random.uniform(0.05, 0.1),  # reduc critical  severe dead mproved immunity
                    3,  # contacts
                ]
            )
        result = Secirvvs.simulate(
            initial_condition=initial_condition, parameters=parameters, duration=duration
        )

        # for i, c in enumerate(result.columns):
        #     if i > 0:
        #         column_index = names.index(c) + 1  # +1 because time is not in names
        end = time.perf_counter()
        print("time", end - start)
