import time

import numpy as np
import pytest
from memiliflow.memilio.secir import Secir
from memilio.simulation import AgeGroup


class TestSecir:
    def test_call_simulation_1e3_times(self):
        duration = 10
        initial_condition = np.array(
            [
                1_000_000,  # SUSCEPTIBLE
                9_000,  # EXPOSED
                8_000,  # INFECTED_NO_SYMPTOMS
                7_000,  # INFECTED_NO_SYMPTOMS_CONFIRMED
                6_000,  # INFECTED_SYMPTOMS
                5_000,  # INFECTED_SYMPTOMS_CONFIRMED
                4_000,  # INFECTED_SEVERE
                3_000,  # INFECTED_CRITICAL
                2_000,  # RECOVERED
                1_000,  # DEAD
            ]
        )
        start = time.perf_counter()
        for i in range(1000):
            parameters = np.array(
                [
                    np.random.uniform(4.3, 8),  # time exposed
                    np.random.uniform(7.0, 10.0),  # time infected no symptoms
                    np.random.uniform(7.0, 10.0),  # time infected symptoms
                    np.random.uniform(10, 20),  # tim infected severe
                    np.random.uniform(20, 30),  # time infected critical
                    np.random.uniform(0.05, 0.25),  # relative transmissions no symptoms
                    np.random.uniform(0.05, 0.25),  # transmission prob
                    np.random.uniform(0.5, 0.8),  # recovered per infected no symptoms
                    np.random.uniform(0.3, 0.8),  # risk of infection symptomatic
                    np.random.uniform(0.05, 0.1),  # severe per infected symptoms
                    np.random.uniform(0.05, 0.1),  # critical per severe
                    np.random.uniform(0.05, 0.1),  # deaths per critical
                    1,  # max risk of infection from symptomatic
                    3,  # contacts
                ]
            )
            result = Secir.simulate(
                initial_condition=initial_condition, parameters=parameters, duration=duration
            )
        end = time.perf_counter()
        print("time", end - start)

    @pytest.mark.skip(reason="takes forever")
    def test_call_simulation_with_invalid_parameters(self):
        initial_condition = np.array(
            [
                10000,  # SUSCEPTIBLE
                9000,  # EXPOSED
                8000,  # INFECTED_NO_SYMPTOMS
                7000,  # INFECTED_NO_SYMPTOMS_CONFIRMED
                6000,  # INFECTED_SYMPTOMS
                5000,  # INFECTED_SYMPTOMS_CONFIRMED
                4000,  # INFECTED_SEVERE
                3000,  # INFECTED_CRITICAL
                2000,  # RECOVERED
                1000,  # DEAD
            ]
        )
        start = time.perf_counter()
        parameters = np.array(
            [
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
                -1,
            ]
        )

        result = Secir.simulate(initial_condition=initial_condition, parameters=parameters)
