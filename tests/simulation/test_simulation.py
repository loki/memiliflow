import numpy as np
import pytest
from memiliflow.simulation.simulation import (
    SimulatorForPyMC,
    create_oseir_confirmed_cases_simulator,
    create_oseir_new_cases_simulator,
    create_secir_confirmed_cases_simulator,
    create_secir_new_cases_simulator,
    wrap_simulator_for_sbi,
)


class TestSimulation:
    def test_Oseir_confirmed_cases_simulator(self):
        days_to_sim = 200
        total_population = 10_000_000
        sim = create_oseir_confirmed_cases_simulator(
            days_to_simulate=days_to_sim, total_population=total_population
        )

        params = [
            1000,  # initial exposed
            100,  # initial infected
            10,  # initial recovered
            0.4,  # rate E->I
            0.3,  # rate I->R
            0.05,  # transmission probability on contact
            10,  # contact rate
        ]

        result = sim(parameters=params)
        # len(result) == days_to_sim + 1 because day 0 is included in results
        assert len(result) == days_to_sim + 1

        days_to_sim = 200
        total_population = 10_000_000
        sim = create_oseir_confirmed_cases_simulator(
            days_to_simulate=days_to_sim, total_population=total_population
        )

        params = [
            1000,  # initial exposed
            100,  # initial infected
            10,  # initial recovered
            0.4,  # rate E->I
            0.3,  # rate I->R
            0.05,  # transmission probability on contact
            10,  # contact rate
        ]

        result = sim(parameters=params)
        # len(result) == days_to_sim + 1 because day 0 is included in results
        assert len(result) == days_to_sim + 1

    def test_Oseir_new_cases_simulator(self):
        days_to_sim = 200
        total_population = 10_000_000
        sim = create_oseir_confirmed_cases_simulator(
            days_to_simulate=days_to_sim, total_population=total_population
        )

        params = [
            1000,  # initial exposed
            100,  # initial infected
            10,  # initial recovered
            0.4,  # rate E->I
            0.3,  # rate I->R
            0.05,  # transmission probability on contact
            10,  # contact rate
        ]

        result = sim(parameters=params)
        # len(result) == days_to_sim + 1 because day 0 is included in results
        assert len(result) == days_to_sim + 1

        days_to_sim = 200
        total_population = 10_000_000
        sim = create_oseir_new_cases_simulator(
            days_to_simulate=days_to_sim, total_population=total_population
        )

        params = [
            1000,  # initial exposed
            100,  # initial infected
            10,  # initial recovered
            0.4,  # rate E->I
            0.3,  # rate I->R
            0.05,  # transmission probability on contact
            10,  # contact rate
        ]

        result = sim(parameters=params)
        # len(result) == days_to_sim, because one day goes missing (the first) when computing new cases
        assert len(result) == days_to_sim

    def test_Secir_confirmed_cases_simulator(self):
        days_to_sim = 20
        total_population = 10_000_000
        sim = create_secir_confirmed_cases_simulator(
            days_to_simulate=days_to_sim, total_population=total_population
        )

        params = [
            10000,  # parameter[0] -> initial exposed
            10000,  # parameter[1] -> initial infected no symptoms
            10000,  # parameter[2] -> initial infected no symptoms confirmed
            10000,  # parameter[3] -> initial infected symptoms
            10000,  # parameter[4] -> initial infected symptoms confirmed
            10000,  # parameter[5] -> initial infected severe
            10000,  # parameter[6] -> initial infected critical
            10000,  # parameter[7] -> initial recovered
            10000,  # parameter[8] -> initial dead
            5.2,  # parameter[9] -> incubation time
            5.2,  # parameter[10] -> time infected symptoms
            5.2,  # parameter[11] -> time infected severe
            5.2,  # parameter[12] -> time infected critical
            0.05,  # parameter[13] -> transmission probability on contact
            0.1,  # parameter[14] -> recovered per infected no symptoms
            0.1,  # parameter[15] -> risk of infection from symptomatic
            0.1,  # parameter[16] -> severe per infected symptoms
            0.1,  # parameter[17] -> critical per severe
            0.1,  # parameter[18] -> deaths per critical
            1,  # parameter[19] -> max risk infection
            0.1,  # parameter[20] -> contact rate
        ]

        result = sim(parameters=params)
        # len(result) == days_to_sim + 1 because day 0 is included in results
        assert len(result) == days_to_sim + 1

    def test_Secir_new_cases_simulator(self):
        days_to_sim = 200
        total_population = 10_000_000
        sim = create_secir_new_cases_simulator(
            days_to_simulate=days_to_sim, total_population=total_population
        )

        params = [
            10000,  # parameter[0] -> initial exposed
            10000,  # parameter[1] -> initial infected no symptoms
            10000,  # parameter[2] -> initial infected no symptoms confirmed
            10000,  # parameter[3] -> initial infected symptoms
            10000,  # parameter[4] -> initial infected symptoms confirmed
            10000,  # parameter[5] -> initial infected severe
            10000,  # parameter[6] -> initial infected critical
            10000,  # parameter[7] -> initial recovered
            10000,  # parameter[8] -> initial dead
            5.2,  # parameter[9] -> incubation time
            5.2,  # parameter[10] -> time infected symptoms
            5.2,  # parameter[11] -> time infected severe
            5.2,  # parameter[12] -> time infected critical
            0.05,  # parameter[13] -> transmission probability on contact
            0.1,  # parameter[14] -> recovered per infected no symptoms
            0.1,  # parameter[15] -> risk of infection from symptomatic
            0.1,  # parameter[16] -> severe per infected symptoms
            0.1,  # parameter[17] -> critical per severe
            0.1,  # parameter[18] -> deaths per critical
            1,  # parameter[19] -> max risk infection
            0.1,  # parameter[20] -> contact rate
        ]

        result = sim(parameters=params)
        # len(result) == days_to_sim because day 0 is dropped when making diff for new cases
        assert len(result) == days_to_sim
