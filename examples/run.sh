#!/bin/bash -x
#SBATCH --account=atmlaml
#SBATCH --nodes=6
#SBATCH --ntasks=6
#SBATCH --cpus-per-task=96
#SBATCH --output=slurms/gpu-out.%j
#SBATCH --error=slurms/gpu-err.%j
#SBATCH --time=00:20:00
#SBATCH --partition=booster

source /p/project/atmlaml/bazarova1/memiliflow/sc_venv_template1/activate.sh

module load OpenMPI

export SRUN_CPUS_PER_TASK="$SLURM_CPUS_PER_TASK"

srun python run_sbi_joblib.py
