import logging
import os
import sys
import time
from multiprocessing import Pool

import numpy as np
from matplotlib import pyplot as plt
from memiliflow.data.simulation import simulate_oseir_stochastic_trajectory
from memiliflow.likelihood.likelihood_op import (
    MemilioOseirLikelihoodOpForHopsy,
    MemilioPyOseirLikelihoodOpForHopsy,
    PyrossOseirLikelihoodOpForHopsy,
)
from memiliflow.memilio.oseir import Oseir
from memiliflow.memilio.oseir_builder import OseirBuilder
from memilio import simulation

_log = logging.getLogger(__file__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

true_time_exposed = 5.2
true_time_infected = 6.0
true_transmission = 0.95

population = 8600 * 1e3
initial_exposed = 300 * 1e3
initial_infected = 200 * 1e3
initial_recovered = 100 * 1e3

priors = {
    Oseir.Parameter.TIME_EXPOSED: {
        "mean": np.array(true_time_exposed),
        "sigma": np.array(1e6),
        "lower": np.array(2),
        "upper": np.array(14),
    },
    Oseir.Parameter.TIME_INFECTED: {
        "mean": np.array(true_time_infected),
        "sigma": np.array(1e6),
        "lower": np.array(2),
        "upper": np.array(15),
    },
    Oseir.Parameter.TRANSMISSION_PROBABILITY: {
        "mean": np.array(true_transmission),
        "sigma": np.array(0.5),
        "lower": np.array(0.5),
        "upper": np.array(0.999),
    },
}


def single_f(vals):
    model = vals[4]
    row = vals[2]
    col = vals[3]
    point = np.array(
        [
            vals[0][row, col],
            vals[1][row, col],
            priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["mean"],
        ]
    )
    return model.compute_negative_log_likelihood(point)


def f(_x, _y, result, model):
    args = []
    for row in range(result.shape[0]):
        for col in range(result.shape[1]):
            args.append([_x, _y, row, col, model])
    with Pool(32) as p:
        results = p.map(single_f, args)
    for row in range(result.shape[0]):
        for col in range(result.shape[1]):
            result[row, col] = results[row + col * result.shape[0]]

    return result


def plot_likelihood_levels(mmodel, Tf, observations, priors, dir_name, method, n_grid):
    if method not in ["memilio", "memiliflow", "pyross"]:
        raise Exception("wrong parameter")
    dir_name = dir_name + "/" + method + "_n=" + str(n_grid)
    os.mkdir(dir_name)

    x = np.linspace(
        [priors[Oseir.Parameter.TIME_EXPOSED]["lower"]],
        [priors[Oseir.Parameter.TIME_EXPOSED]["upper"]],
        n_grid,
    )

    y = np.linspace(
        [priors[Oseir.Parameter.TIME_INFECTED]["lower"]],
        [priors[Oseir.Parameter.TIME_INFECTED]["upper"]],
        n_grid,
    )

    X, Y = np.meshgrid(x, y)

    result = np.zeros(shape=X.shape)

    print("computing likelihoods...")
    true_params = np.array(
        [
            priors[Oseir.Parameter.TIME_INFECTED]["mean"],
            priors[Oseir.Parameter.TIME_EXPOSED]["mean"],
            priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["mean"],
        ]
    )
    model = None
    if method == "memilio":
        memilio_model = MemilioOseirLikelihoodOpForHopsy(priors, mmodel, observations)
        model = memilio_model
    if method == "memiliflow":
        py_model = MemilioPyOseirLikelihoodOpForHopsy(priors, mmodel, observations)
        model = py_model
    if method == "pyross":
        pyross_model = PyrossOseirLikelihoodOpForHopsy(priors, Tf, observations)
        model = pyross_model

    start = time.time()
    Z = f(X, Y, result, model)
    end = time.time()

    runtime = np.array([end - start])

    np.savetxt(dir_name + "/runtime.txt", runtime, fmt="%d")
    np.savetxt(dir_name + "/X.txt", X, fmt="%d")
    np.savetxt(dir_name + "/Y.txt", Y, fmt="%d")
    np.savetxt(dir_name + "/Z.txt", Z, fmt="%d")

    cp = plt.contourf(X, Y, Z)
    plt.axvline(priors[Oseir.Parameter.TIME_EXPOSED]["mean"])
    plt.axhline(priors[Oseir.Parameter.TIME_INFECTED]["mean"])
    plt.colorbar(cp)
    plt.title(method + " (n=" + str(n_grid) + ")")
    plt.xlabel("time exposed")
    plt.ylabel("time infected")
    plt.tight_layout()
    plt.savefig(dir_name + "/likelihood_countour.png", format="png")
    plt.show(block=False)


def oseir_likelihood(simulation_duration, plot_trajectories=True):
    # Setting log level might be broken, see https://github.com/DLR-SC/memilio/issues/335
    simulation.set_log_level(simulation.LogLevel.Error)

    print("simulating ground truth")
    dir_name = "likelihood_investigation"
    os.mkdir(dir_name)
    ground_truth = simulate_oseir_stochastic_trajectory(
        population=population,
        num_age_groups=1,
        beta=true_transmission,
        gE=1.0 / true_time_exposed,
        gI=1.0 / true_time_infected,
        initial_exposed=initial_exposed,
        initial_infected=initial_infected,
        initial_recovered=initial_recovered,
        simulation_duration=simulation_duration,
        plot_trajectories=plot_trajectories,
    )
    np.savetxt(dir_name + "/ground_truth.txt", ground_truth, fmt="%d")
    print("done")

    oseir_builder = OseirBuilder(
        time_exposed=true_time_exposed,
        time_infected=true_time_infected,
        transmission=true_transmission,
        population=population,
        initial_exposed=initial_exposed,
        initial_infected=initial_infected,
        initial_recovered=initial_recovered,
    )

    model = Oseir(
        oseir_builder,
        default_t0=0,
        default_simulation_step=0.1,
        default_simulation_duration=simulation_duration,
    )
    model.set_parameter(Oseir.Parameter.CONTACT_MATRIX, np.array([[1]]))

    deterministic_simulation = model.simulate(simulation_duration=simulation_duration)
    if plot_trajectories:
        time = deterministic_simulation[0, :]
        S = deterministic_simulation[1, :]
        E = deterministic_simulation[2, :]
        I = deterministic_simulation[3, :]
        R = deterministic_simulation[4, :]
        plt.plot(time, S, "x", label="simulated S", lw=2)
        plt.plot(time, E, "x", label="simulated E", lw=2)
        plt.plot(time, I, "x", label="simulated I", lw=2)
        plt.plot(time, R, "x", label="simulated R", lw=2)
        plt.legend()
        plt.savefig(dir_name + "/grund_trouth.png", format="png")
        plt.show(block=False)

    for n_grid in [5, 10, 50, 100]:
        plot_likelihood_levels(
            mmodel=model,
            Tf=simulation_duration,
            observations=ground_truth,
            priors=priors,
            dir_name=dir_name,
            method="memilio",
            n_grid=n_grid,
        )
        plot_likelihood_levels(
            mmodel=model,
            Tf=simulation_duration,
            observations=ground_truth,
            priors=priors,
            dir_name=dir_name,
            method="pyross",
            n_grid=n_grid,
        )
        plot_likelihood_levels(
            mmodel=model,
            Tf=simulation_duration,
            observations=ground_truth,
            priors=priors,
            dir_name=dir_name,
            method="memiliflow",
            n_grid=n_grid,
        )


if __name__ == "__main__":
    oseir_likelihood(50)
