import matplotlib.pyplot as plt
from memiliflow.data.case_data import CaseData


def plot_real_cases():
    case_data = CaseData.read_json_from_dir(".", keys=["cases_all_germany"])
    cases = case_data.data["cases_all_germany"].set_index("Date")
    print(cases)
    cols = ["Confirmed"]
    cases[cols].plot()
    plt.title("cases")
    plt.show()
    # cases['cases_all_germany'].plot()
    new_cases = cases - cases.shift(1)
    # new_cases = (cases[['Deaths', ]] - cases[['Deaths', ]].shift(1))
    print(new_cases)

    new_cases.plot()
    plt.title("new cases")
    plt.show()

    pass


if __name__ == "__main__":
    plot_real_cases()
