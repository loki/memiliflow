import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from memiliflow.data.simulation import simulate_reporting
from memiliflow.memilio.oseir import Oseir


def simulate_delays():
    compartments = {
        Oseir.Compartment.SUSCEPTIBLE: 1e8,
        Oseir.Compartment.EXPOSED: 1e1,
        Oseir.Compartment.INFECTED: 1e6,
        Oseir.Compartment.RECOVERED: 1e3,
    }

    parameters = {
        Oseir.Parameter.TIME_EXPOSED: 5.2,
        Oseir.Parameter.TIME_INFECTED: 10.0,
        Oseir.Parameter.TRANSMISSION_PROBABILITY: 0.1,
    }

    contact_patterns = {Oseir.Conctact.BASELINE: np.array([[0.90]])}

    model = Oseir.setup_model(
        parameters=parameters, compartments=compartments, contact_patterns=contact_patterns
    )

    simulated = Oseir.simulate(model, duration=180, interpolation_target=np.arange(180))
    reported = simulate_reporting(
        observations=simulated,
        index_to_delay=3,
        detection_delay=7,
        detection_delay_std=3,
        report_matrix=None,
    )
    simulated = model.simulate(simulation_duration=simulation_duration)
    I, detected, reported = simulate_delay(
        simulated, index_to_delay=3, detection_delay=7, detection_delay_std=4, report_matrix=None
    )

    fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor="w", edgecolor="k")
    t = simulated[0, :]

    # plt.plot(t, simulated[1, :], "-", label="S", lw=2)
    plt.plot(t, simulated[2, :], "-", label="E", lw=2)
    plt.plot(t, simulated[3, :], "-", label="I", lw=2)
    plt.plot(t, reported, "-", label="reported I", lw=2)
    # plt.plot(t, simulated[4, :], "-", label="R", lw=2)

    plt.legend(fontsize=26)
    plt.grid()
    plt.xlabel(r"time")
    plt.autoscale(enable=True, axis="x", tight=True)
    plt.show(block=False)

    return


if __name__ == "__main__":
    simulate_delays()
