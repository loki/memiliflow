import time

import numpy as np
import pyross
import sbi.utils as utils
import torch
from matplotlib import pyplot as plt
from sbi.analysis.plot import pairplot
from sbi.inference.base import infer


def simulate_stochastic_trajectory():
    M = 1  # the population has one age groups
    N = 1e6  # and this is the total population
    Ni = N * np.ones(M)

    # parameters for generating synthetic trajectory
    beta = 0.95  # infection rate
    gE = 1.0 / 5.2  # recovery rate of asymptomatic infectives
    gI = 1.0 / 6  # recovery rate of asymptomatic infectives

    # set up initial condition
    E0 = 100
    I0 = 50
    R0 = 10
    S0 = N - (E0 + I0 + R0)
    x0 = {"S": [S0], "E": [E0], "I": [I0], "R": [R0]}

    Tf = 100  # reduce time span if sim is too slow
    Nf = Tf + 1

    # set the contact structure
    C = np.array([[1.0]])

    # [[18., 9.],
    #           [3., 12.]])
    # C_ij = number of people group from group i that an individual from group j meets per day
    def contactMatrix(t):
        return C

    parameters = {"beta": beta, "gE": gE, "gI": gI}
    true_parameters = {"beta": beta, "gE": gE, "gI": gI}

    model_spec = {
        "classes": ["S", "E", "I", "R"],
        "S": {
            "infection": [["I", "S", "-beta"]],
        },
        "E": {"linear": [["E", "-gE"]], "infection": [["I", "S", "beta"]]},
        "I": {
            "linear": [
                ["E", "gE"],
                ["I", "-gI"],
            ],
        },
        "R": {
            "linear": [["I", "gI"]],
        },
    }

    # use pyross stochastic to generate traj and save
    sto_model = pyross.stochastic.Model(model_spec, true_parameters, M, Ni)
    data = sto_model.simulate(x0, contactMatrix, Tf, Nf, method="tau-leaping")
    data_array = data["X"]
    np.save("SIR_sto_traj.npy", data_array)

    fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor="w", edgecolor="k")
    plt.rcParams.update({"font.size": 22})
    t = data["t"]

    plt.fill_between(t, 0, np.sum(data_array[:, :M], axis=1), alpha=0.3)
    plt.plot(t, np.sum(data_array[:, :M], axis=1), "-", label="S", lw=2)

    plt.fill_between(t, 0, np.sum(data_array[:, M : 2 * M], axis=1), alpha=0.3)
    plt.plot(t, np.sum(data_array[:, M : 2 * M], axis=1), "-", label="E", lw=2)

    plt.fill_between(t, 0, np.sum(data_array[:, 2 * M : 3 * M], axis=1), alpha=0.3)
    plt.plot(t, np.sum(data_array[:, 2 * M : 3 * M], axis=1), "-", label="I", lw=2)

    plt.fill_between(t, 0, np.sum(data_array[:, 3 * M : 4 * M], axis=1), alpha=0.3)
    plt.plot(t, np.sum(data_array[:, 3 * M : 4 * M], axis=1), "-", label="R", lw=2)

    plt.legend(fontsize=26)
    plt.grid()
    plt.xlabel(r"time")
    plt.autoscale(enable=True, axis="x", tight=True)
    plt.show()

    return data_array, M, Ni, true_parameters


def run_sbi(data_array, M, Ni, true_parameters, Tf_inference=20):
    # load the data and rescale to intensive variables
    Tf_inference = 30  # truncate to only getting the first few datapoints
    Nf_inference = Tf_inference + 1
    observed_data = (data_array[:Nf_inference]).astype("float")

    inference_parameters = {"beta": 100, "gE": 100, "gI": 100}

    model_spec = {
        "classes": ["S", "E", "I", "R"],
        "S": {
            "infection": [["I", "S", "-beta"]],
        },
        "E": {"linear": [["E", "-gE"]], "infection": [["I", "S", "beta"]]},
        "I": {
            "linear": [
                ["E", "gE"],
                ["I", "-gI"],
            ],
        },
        "R": {
            "linear": [["I", "gI"]],
        },
    }

    # set the contact structure
    C = np.array([[1.0]])

    estimator = pyross.inference.Model(model_spec, inference_parameters, 1, Ni)

    # [[18., 9.],
    #           [3., 12.]])
    # C_ij = number of people group from group i that an individual from group j meets per day
    # TODO: check how contact matrix should be set in simulator
    def contactMatrix(t):
        return C

    def simulator(parameters: np.array):
        # compute -log_p for the initial guess
        guessed_parameters = {
            "beta": parameters[0],
            "gE": parameters[1],
            "gI": parameters[2],
        }
        estimator.set_det_model(guessed_parameters)
        starting_time = 0
        return estimator.integrate(observed_data[0], starting_time, Tf, Nf)

    # Define the prior (log normal prior around guess of parameter with defined std. deviation)
    beta_g = 0.95
    beta_std = 0.1
    gE_g = 1.0 / 5.2
    gE_std = 0.1
    gI_g = 1.0 / 6.0
    gI_std = 0.1

    prior_sbi = utils.BoxUniform(
        low=torch.tensor([-0.1, -0.1, -0.1]), high=torch.tensor([0.1, 0.1, 0.1])
    )

    num_sim = 10

    method = "SNRE"

    posterior = infer(
        simulator,
        prior,
        # See glossary for explanation of methods.
        #    SNRE newer than SNLE newer than SNPE.
        method=method,
        num_workers=-1,
        num_simulations=num_sim,
    )

    print("success!")
    # TODO SBI can fit in here

    # Nf = 101
    # Tf = Nf - 1

    # estimator.set_det_model(best_estimates)
    # x_det = estimator.integrate(observed_data[0], 0, Tf, Nf)
    # plt.rcParams.update({"font.size": 12})
    # M = 1
    # inds = np.random.randint(len(flat_samples), size=1000)
    # param_names = ["beta", "gE", "gI"]
    # for i, ind in enumerate(inds):
    #    params = {}
    #    for ii, jj in enumerate(flat_samples[ind]):
    #        params[param_names[ii]] = jj

    #    estimator.set_det_model(params)
    #    pred = estimator.integrate(observed_data[0], 0, Tf, Nf)
    #    plt.plot(np.sum(pred[:, :M], axis=1), c="C0", alpha=0.01)
    #    plt.plot(np.sum(pred[:, 2 * M : 3 * M], axis=1), c="C2", alpha=0.01)
    #    plt.plot(np.sum(pred[:, M : 2 * M], axis=1), c="C1", alpha=0.01)

    # observed_data = data_array
    # plt.plot(np.sum(x_det[:, :M], axis=1), label="Inferred S", c="C0", linestyle="--")
    # plt.plot(np.sum(observed_data[:, :M], axis=1), label="True S", c="C3")
    # plt.plot(np.sum(x_det[:, M : 2 * M], axis=1), label="Inferred E", c="C1", linestyle="--")
    # plt.plot(np.sum(observed_data[:, M : 2 * M], axis=1), label="True E", c="C4")
    # plt.plot(np.sum(x_det[:, 2 * M : 3 * M], axis=1), label="Inferred I", c="C2", linestyle="--")
    # plt.plot(np.sum(observed_data[:, 2 * M : 3 * M], axis=1), label="True I", c="C5")
    # plt.axvspan(0, Nf_inference, label="Used for inference", alpha=0.3, color="dodgerblue")
    # plt.legend()
    # plt.savefig("demo.pdf", format="pdf")
    # plt.show()


if __name__ == "__main__":
    # start = time.time()
    data_array, M, Ni, true_parameters = simulate_stochastic_trajectory()
    print(data_array.shape)
    print(data_array)
    run_sbi(data_array, M, Ni, true_parameters)
    # end = time.time()
    # print("inference time:", end - start, " seconds")
