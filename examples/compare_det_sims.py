# depracted: update to new Oseir instead of OseirInterface
# import time
#
# import numpy as np
# import pyross
# from matplotlib import pyplot as plt
# from memiliflow.memilio.oseir_builder import OseirBuilder
# from memiliflow.memilio.oseir_interface import OseirInterface
#
#
# def plot_det():
#     M = 1  # the population has one age groups
#     N = 1e6  # and this is the total population
#
#     # parameters for generating synthetic trajectory
#     beta = 0.95  # infection rate
#     gE = 1.0 / 5.2  # recovery rate of asymptomatic infectives
#     gI = 1.0 / 6  # recovery rate of asymptomatic infectives
#
#     Ni = N * np.ones(M)
#
#     # set up initial condition
#     E0 = 100
#     I0 = 50
#     R0 = 10
#     S0 = N - (E0 + I0 + R0)
#     x0 = {"S": [S0], "E": [E0], "I": [I0], "R": [R0]}
#
#     Tf = 100
#     Nf = Tf + 1
#
#     # set the contact structure
#     C = np.array([[0.75]])
#
#     def contactMatrix(t):
#         return C
#
#     true_parameters = {"beta": beta, "gE": gE, "gI": gI}
#
#     model_spec = {
#         "classes": ["S", "E", "I", "R"],
#         "S": {
#             "infection": [["I", "S", "-beta"]],
#         },
#         "E": {"linear": [["E", "-gE"]], "infection": [["I", "S", "beta"]]},
#         "I": {
#             "linear": [
#                 ["E", "gE"],
#                 ["I", "-gI"],
#             ],
#         },
#         "R": {
#             "linear": [["I", "gI"]],
#         },
#     }
#
#     model = pyross.deterministic.Model(model_spec, true_parameters, 1, Ni)
#     print("x0", x0, "( array:", np.array(list(x0.values())).flatten(), ")")
#     xsim = model.simulate(x0, contactMatrix, Tf, Nf)
#
#     oseir_builder = OseirBuilder(
#         time_exposed=1.0 / gE,
#         time_infected=1.0 / gI,
#         transmission=beta,
#         contact_matrix_baseline=C,
#         population=N,
#         initial_exposed=E0,
#         initial_infected=I0,
#         initial_recovered=R0,
#     )
#
#     mem_model = OseirInterface(
#         oseir_builder,
#         default_t0=0,
#         default_simulation_step=0.05,
#         default_simulation_duration=Tf,
#     )
#
#     res = mem_model.simulate(
#         parameters=[1.0 / gE, 1.0 / gI, beta],
#         parameter_names=["TimeExposed", "TimeInfected", "TransmissionProbabilityOnContact"],
#     )
#
#     S_mem = res[1, :]
#     E_mem = res[2, :]
#     I_mem = res[3, :]
#     R_mem = res[4, :]
#
#     t = xsim["t"]
#     S = np.sum(model.model_class_data("S", xsim), axis=1)
#     E = np.sum(model.model_class_data("E", xsim), axis=1)
#     I = np.sum(model.model_class_data("I", xsim), axis=1)
#     R = np.sum(model.model_class_data("R", xsim), axis=1)
#
#     plt.plot(t, S, "-", label="S_pyross", lw=4, alpha=0.3)
#     plt.plot(t, E, "-", label="E_pyross", lw=4, alpha=0.3)
#     plt.plot(t, I, "-", label="I_pyross", lw=4, alpha=0.3)
#     plt.plot(t, R, "-", label="R_pyross", lw=4, alpha=0.3)
#
#     t = res[0, :]
#     plt.plot(t, S_mem, "--", label="S_mem", lw=2, c="C0", alpha=0.7)
#     plt.plot(t, E_mem, "--", label="E_mem", lw=2, c="C1", alpha=0.7)
#     plt.plot(t, I_mem, "--", label="I_mem", lw=2, c="C2", alpha=0.7)
#     plt.plot(t, R_mem, "--", label="R_mem", lw=2, c="C3", alpha=0.7)
#
#     plt.legend(fontsize=12)
#     plt.grid()
#     plt.xlabel(r"time")
#     plt.autoscale(enable=True, axis="x", tight=True)
#     plt.show()
#
#
# if __name__ == "__main__":
#     plot_det()
