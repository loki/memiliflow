import arviz as az
import matplotlib
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from memiliflow.data.case_studies import get_RKI_case_data_2020_ma7
from memiliflow.simulation import simulation

if __name__ == "__main__":
    matplotlib.use("TkAgg")
    days = 14
    models = ["oseir", "secir"]
    for model in models:
        samples = az.from_netcdf(f"run_new_cases_mcmc_{model}_{days}.nc")
        summary = az.summary(samples, hdi_prob=0.5)
        with pd.option_context("display.max_rows", None, "display.max_columns", None):
            print(summary)

        summary = summary[["pred" in s for s in summary.index]]
        percentile_25 = summary[["hdi_25%"]].values
        percentile_50 = summary[["mean"]].values
        percentile_75 = summary[["hdi_75%"]].values

        days = len(summary.index) + 1
        population = 83e6
        data = get_RKI_case_data_2020_ma7(days)
        data["Daily new cases"] = data["Confirmed"] - data["Confirmed"].shift(1)
        data = data[data["Daily new cases"].notna()]

        plt.figure(figsize=(2, 2))
        plt.title(f"{model}")
        plt.plot(range(days - 1), data["Daily new cases"], label="observation")
        plt.plot(range(days - 1), percentile_25, c="C1")
        plt.plot(range(days - 1), percentile_50, c="C1")
        plt.plot(range(days - 1), percentile_75, c="C1")
        plt.fill_between(
            range(days - 1),
            percentile_75.flatten(),
            percentile_25.flatten(),
            color="C1",
            alpha=0.25,
        )

        plt.legend()
        plt.tight_layout()
        plt.savefig(f"{model}_posterior_predictive_days_{days}.png")
        plt.show(block=False)
        # var_names = [

        # az.plot_trace(trace, var_names=var_names)
        # plt.tight_layout()
        # plt.savefig(f"examples/mcmc/run_new_cases_mcmc_secir_{days}.png")

        # az.plot_forest(samples, var_names=["pred"], combined=True)
        # plt.show()
        #
        # az.plot_ppc(samples, random_seed=4, var_names=['pred'])
        #     "infection*contacts",
        #     "initial E",
        #     "initial I no Sym",
        #     "recovered per infected no symptoms",
        # ]

        # az.plot_pair(samples, var_names=var_names, show=True)

        # az.plot_posterior(samples, show=True)# , var_names=var_names)
        # az.plot_ppc(samples, show=True)# , var_names=var_names)
