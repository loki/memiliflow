from datetime import date
from time import perf_counter

import arviz as az
import matplotlib
import memilio.simulation as mio
import pandas as pd
import pymc as pm
from matplotlib import pyplot as plt
from memiliflow.data.case_studies import get_divi_data_2020_ma7
from memiliflow.inference.pymc_models import create_pymc_icu_secir_model

if __name__ == "__main__":
    days = 100
    data = get_divi_data_2020_ma7(days)
    pmodel = create_pymc_icu_secir_model(data)

    gv = pm.model_to_graphviz(pmodel)
    gv.render(filename="run_new_icu_secir.pdf", format="pdf")
    # matplotlib.use("TkAgg")
    #
    with pmodel:
        mio.set_log_level(mio.LogLevel.Off)
        start = perf_counter()
        trace = pm.sample(10000, tune=2_000, step=pm.DEMetropolisZ())
        end = perf_counter()
        print("sampling took", end - start, "seconds")
        with pd.option_context("display.max_rows", None, "display.max_columns", None):
            print(az.summary(trace))

        # trace.to_netcdf(f"examples/mcmc/run_new_icu_mcmc_secir_{days}.nc")
        trace.to_netcdf(f"run_new_icu_mcmc_secir_{days}.nc")
