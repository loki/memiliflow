import time
from datetime import date

import arviz as az
import matplotlib
import pandas as pd
import pymc as pm
from matplotlib import pyplot as plt
from memiliflow.data.case_studies import get_RKI_case_data_2020_ma7
from memiliflow.inference.pymc_models import create_pymc_secir_model

if __name__ == "__main__":
    days = 14
    data = get_RKI_case_data_2020_ma7(days)
    pmodel = create_pymc_secir_model(data)

    gv = pm.model_to_graphviz(pmodel)
    gv.render(filename="run_new_cases_secir.pdf", format="pdf")
    matplotlib.use("TkAgg")

    with pmodel:
        trace = pm.sample(200_000, tune=10_000, step=pm.DEMetropolisZ()).copy()
        with pd.option_context("display.max_rows", None, "display.max_columns", None):
            print(az.summary(trace))

        trace.to_netcdf(f"examples/mcmc/run_new_cases_mcmc_secir_{days}.nc")
