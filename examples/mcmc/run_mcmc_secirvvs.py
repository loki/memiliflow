import datetime
import json
from datetime import date

import arviz as az
import h5py
import matplotlib
import pandas as pd
import pymc as pm
from memiliflow.data.case_studies import get_RKI_case_data_2020_ma7
from memiliflow.inference.pymc_models import create_pymc_secir_model

if __name__ == "__main__":
    days = 14
    meta_data_path = "data/metadata.json"
    data_path = "data/Results_sum.h5"

    with open(meta_data_path, "r") as f:
        meta_data = json.load(f)

    start_day = meta_data["startDay"]
    compartmentOrder = meta_data["compartmentOrder"]
    print(compartmentOrder)

    h5file = h5py.File("data/Results_sum.h5", "r")

    data = h5file["0"]["Total"][:]
    print(data)
    print(data.shape)
    data_date = str(
        datetime.datetime.fromisoformat(start_day) + datetime.timedelta(days=data.shape[0])
    )
    print(data_date)

    # required: how to initialize model

    #
    #
    # pmodel = create_pymc_secir_model(data)
    #
    # gv = pm.model_to_graphviz(pmodel)
    # gv.render(filename="run_new_cases_secir.pdf", format="pdf")
    # matplotlib.use("TkAgg")
    #
    # with pmodel:
    #     trace = pm.sample(200_000, tune=10_000, step=pm.DEMetropolisZ()).copy()
    #     with pd.option_context("display.max_rows", None, "display.max_columns", None):
    #         print(az.summary(trace))
    #
    #     trace.to_netcdf(f"examples/mcmc/run_new_cases_mcmc_secir_{days}.nc")
