import time

import numpy as np
import pyross
from matplotlib import pyplot as plt
from memiliflow.data.simulation import simulate_oseir_stochastic_trajectory


def run_inference(data_array, num_age_groups, Ni, true_parameters, Tf_inference=10):
    # load the data and rescale to intensive variables
    Nf_inference = Tf_inference + 1
    x = (data_array[:Nf_inference]).astype("float")

    inference_parameters = {"beta": 1, "gE": 1, "gI": 1}

    model_spec = {
        "classes": ["S", "E", "I", "R"],
        "S": {
            "infection": [["I", "S", "-beta"]],
        },
        "E": {"linear": [["E", "-gE"]], "infection": [["I", "S", "beta"]]},
        "I": {
            "linear": [
                ["E", "gE"],
                ["I", "-gI"],
            ],
        },
        "R": {
            "linear": [["I", "gI"]],
        },
    }

    # initialise the estimator
    estimator = pyross.inference.Model(model_spec, inference_parameters, num_age_groups, Ni)

    # set the contact structure
    C = np.array([[1.0]])

    def contactMatrix(t):
        return C

    # Define the prior (log normal prior around guess of parameter with defined std. deviation)
    beta_g = 0.1
    beta_std = 0.01

    gE_g = 1.0 / 5.2
    gE_std = 0.01

    gI_g = 1.0 / 6.0
    gI_std = 0.01

    # compute -log_p for the initial guess
    guessed_parameters = {
        "beta": beta_g,
        "gE": gE_g,
        "gI": gI_g,
    }

    logp = estimator.obtain_minus_log_p(guessed_parameters, x, Tf_inference, contactMatrix)
    print("logp guessed", logp)
    print("x", x)
    print("tf_inference", Tf_inference)
    logp = estimator.obtain_minus_log_p(true_parameters, x, Tf_inference, contactMatrix)
    print("logp true", logp)

    # cov = estimator.obtain_full_mean_cov(np.array(x[0]), 0, 1)
    # print('cov', cov)
    # print('cov', cov.shape)

    ## inference
    ftol = 1e-6  # Stopping criterion for minimisation (realtive change in function value)
    keys = ["beta", "gE", "gI"]
    eps = 1e-4
    param_priors = {
        "beta": {"mean": beta_g, "std": beta_std, "bounds": [eps, 1]},
        "gE": {"mean": gE_g, "std": gE_std, "bounds": [eps, 0.6]},
        "gI": {"mean": gI_g, "std": gI_std, "bounds": [eps, 0.6]},
    }

    sampler = estimator.infer_mcmc(
        x,
        Tf_inference,
        param_priors,
        contactMatrix=contactMatrix,  # init_priors=init_priors,
        tangent=False,
        verbose=True,
        nprocesses=4,
        nsamples=2000,
    )

    fig, axes = plt.subplots(3, figsize=(12, 10), sharex=True)
    samples = sampler.get_chain()
    print(samples.shape)
    for i in range(3):
        ax = axes[i]
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
    axes[-1].set_xlabel("step number")
    plt.show()

    result = estimator.mcmc_inference_process_result(sampler, param_priors, discard=100)
    post_mean = pyross.utils.posterior_mean(result)
    flat_samples = sampler.get_chain(discard=5000, thin=10, flat=True)

    print("True parameters:")
    print(true_parameters)
    #
    print("\nInferred parameters:")
    best_estimates = post_mean["map_dict"]
    print(best_estimates)

    Nf = 101
    Tf = Nf - 1

    estimator.set_det_model(best_estimates)
    x_det = estimator.integrate(x[0], 0, Tf, Nf)
    plt.rcParams.update({"font.size": 12})
    M = 1
    inds = np.random.randint(len(flat_samples), size=1000)
    param_names = ["beta", "gE", "gI"]
    for i, ind in enumerate(inds):
        params = {}
        for ii, jj in enumerate(flat_samples[ind]):
            params[param_names[ii]] = jj

        estimator.set_det_model(params)
        pred = estimator.integrate(x[0], 0, Tf, Nf)
        plt.plot(np.sum(pred[:, :M], axis=1), c="C0", alpha=0.01)
        plt.plot(np.sum(pred[:, 2 * M : 3 * M], axis=1), c="C2", alpha=0.01)
        plt.plot(np.sum(pred[:, M : 2 * M], axis=1), c="C1", alpha=0.01)

    x = data_array
    plt.plot(np.sum(x_det[:, :M], axis=1), label="Inferred S", c="C0", linestyle="--")
    plt.plot(np.sum(x[:, :M], axis=1), label="True S", c="C3")
    plt.plot(np.sum(x_det[:, M : 2 * M], axis=1), label="Inferred E", c="C1", linestyle="--")
    plt.plot(np.sum(x[:, M : 2 * M], axis=1), label="True E", c="C4")
    plt.plot(np.sum(x_det[:, 2 * M : 3 * M], axis=1), label="Inferred I", c="C2", linestyle="--")
    plt.plot(np.sum(x[:, 2 * M : 3 * M], axis=1), label="True I", c="C5")
    plt.axvspan(0, Nf_inference, label="Used for inference", alpha=0.3, color="dodgerblue")
    plt.legend()
    plt.savefig("demo.pdf", format="pdf")
    plt.show()


if __name__ == "__main__":
    start = time.time()
    M = 1  # num age groups
    N = 8600  # total population
    Ni = N * np.ones(M)

    # parameters for generating synthetic trajectory
    beta = 1  # infection rate
    gE = 1.0 / 5.2  # recovery rate of asymptomatic infectives
    gI = 1.0 / 6  # recovery rate of asymptomatic infectives

    # set up initial condition
    E0 = 300
    I0 = 200
    R0 = 100
    S0 = N - (E0 + I0 + R0)
    x0 = {"S": [S0], "E": [E0], "I": [I0], "R": [R0]}

    Tf = 51

    true_parameters = {"beta": beta, "gE": gE, "gI": gI}
    data_and_timestamp_array = simulate_oseir_stochastic_trajectory(
        population=N,
        num_age_groups=M,
        beta=beta,
        gE=gE,
        gI=gI,
        initial_exposed=E0,
        initial_infected=I0,
        initial_recovered=R0,
        simulation_duration=Tf,
    )
    data_array = data_and_timestamp_array[1:, :].transpose()
    run_inference(data_array, M, Ni, true_parameters, Tf_inference=10)
    end = time.time()
    print("inference time:", end - start, " seconds")
