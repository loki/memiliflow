import pickle
from pathlib import Path

import numpy as np
import pyross


# set the contact structure
def contact_matrix(t):
    C = np.array([[1.0]])
    return C


if __name__ == "__main__":
    model_spec = {
        "classes": ["S", "E", "I", "R"],
        "S": {
            "infection": [["I", "S", "-beta"]],
        },
        "E": {"linear": [["E", "-gE"]], "infection": [["I", "S", "beta"]]},
        "I": {
            "linear": [
                ["E", "gE"],
                ["I", "-gI"],
            ],
        },
        "R": {
            "linear": [["I", "gI"]],
        },
    }

    x0 = np.array([8 * 1e8, 1e5, 1e5, 1e3])
    N = np.sum(x0)
    M = 1  # the population has one age groups
    Ni = N * np.ones(M)
    Tf = 10
    Nf = Tf + 1
    parameters = {"beta": 0.25, "gE": 1.0 / 5.2, "gI": 1.0 / 6.0}

    if not Path("test_sto.pickle").exists():
        sto_model = pyross.stochastic.Model(model_spec, parameters, M, Ni)
        data = sto_model.simulate(x0, contact_matrix, Tf, Nf, method="tau-leaping")
        with open("test_sto.pickle", "wb") as handle:
            pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open("test_sto.pickle", "rb") as handle:
        data = pickle.load(handle)

    # initialise the estimator
    # steps = 4
    # rtol_det = 1e-15
    # det_method = "LSODA"
    estimator = pyross.inference.Model(
        model_spec,
        parameters,
        1,
        Ni,
        # steps=steps,
        # rtol_det=rtol_det,
        # det_method=det_method
    )
    estimator.set_contact_matrix(contact_matrix)
    data_array = data["X"]
    x = data_array.astype("float")
    logp = estimator.obtain_minus_log_p(parameters, x, Tf, contact_matrix)

    # t = 0
    # print('x at t=0', x[0,:])
    # estimator.c_b_and_J(x[0,:], t, jacobian=True)
    # Jre = estimator.J.reshape(4,4)
    # J = estimator.J_mat
    # print(J-Jre)
    # B = estimator.B.reshape(4,4)
    # print('drift at ', t)
    # print(J)
    # for i in range(J.shape[0]):
    #     for j in range(J.shape[1]):
    #         print(J[i,j],',', end="")
    #     print('')
    # print(';')
    # print('noise_correlation at ', t)
    # print(B)
    # for i in range(B.shape[0]):
    #     for j in range(B.shape[1]):
    #         print(B[i,j],',', end="")
    #     print('')
    # print(';' )

    # t = 5
    # print('x at t5')
    # print(x[t,:])
    # estimator.c_b_and_J(x[t,:], t, jacobian=True)
    # J = estimator.J_mat
    # B = estimator.B.reshape(4,4)
    # print('drift at ', t)
    # print(J)
    # for i in range(J.shape[0]):
    #     for j in range(J.shape[1]):
    #         print(J[i,j],',', end="")
    #     print('' )
    # print(';' )
    # print('noise_correlation at ', t)
    # print(B)
    # for i in range(B.shape[0]):
    #     for j in range(B.shape[1]):
    #         print(B[i,j],',', end="" )
    #     print('' )
    # print(';' )

    obs = np.vstack((data["t"], data["X"].transpose()))
    print("obs")
    print(obs)

    for i in range(data["t"].shape[0]):
        out = ",".join([str(m) for m in list(data["X"][i, :])])
        print(out, ";")

    print("logp at true", logp)
    print(steps)
    print(rtol_det)
