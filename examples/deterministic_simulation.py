import time
from datetime import date

import matplotlib.pyplot as plt
import numpy as np
from memiliflow.memilio.oseir import Oseir

# Reproduces ex01a-SIR from pyross with memilio under the hood
if __name__ == "__main__":

    true_time_exposed = 5.2
    true_time_infected = 6
    true_transmission = 0.95

    compartments = {
        Oseir.Compartment.SUSCEPTIBLE: 8300.0,
        Oseir.Compartment.EXPOSED: 500.0,
        Oseir.Compartment.INFECTED: 500.0,
        Oseir.Compartment.RECOVERED: 50.0,
    }

    parameters = {
        Oseir.Parameter.TIME_EXPOSED: true_time_exposed,
        Oseir.Parameter.TIME_INFECTED: true_time_infected,
        Oseir.Parameter.TRANSMISSION_PROBABILITY: true_transmission,
    }
    contact_patterns = {Oseir.Conctact.BASELINE: np.array([[1.0]])}

    model = Oseir.setup_model(
        parameters=parameters, compartments=compartments, contact_patterns=contact_patterns
    )

    start = time.time()
    data = None
    for i in range(100):
        data = Oseir.simulate(model, start_date=date.today(), duration=100)
    end = time.time()
    print("benchmark took", end - start, "seconds")

    # plot the data and obtain the epidemic curve
    t = data["Date"]
    S = data[Oseir.Compartment.SUSCEPTIBLE.value]
    E = data[Oseir.Compartment.EXPOSED.value]
    I = data[Oseir.Compartment.INFECTED.value]
    R = data[Oseir.Compartment.RECOVERED.value]

    fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor="w", edgecolor="k")
    plt.rcParams.update({"font.size": 22})

    N = 1
    plt.fill_between(t, 0, S / N, color="#348ABD", alpha=0.3)
    plt.plot(t, S / N, "-", color="#348ABD", label="$S$", lw=4)

    plt.fill_between(t, 0, E / N, color="orange", alpha=0.3)
    plt.plot(t, E / N, "-", color="orange", label="$E$", lw=4)

    plt.fill_between(t, 0, I / N, color="#A60628", alpha=0.3)
    plt.plot(t, I / N, "-", color="#A60628", label="$I$", lw=4)

    plt.fill_between(t, 0, R / N, color="dimgrey", alpha=0.3)
    plt.plot(t, R / N, "-", color="dimgrey", label="$R$", lw=4)

    plt.legend(fontsize=26)
    plt.grid()
    plt.autoscale(enable=True, axis="x", tight=True)
    plt.ylabel("Fraction of compartment value")
    plt.xlabel("Date")
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()
