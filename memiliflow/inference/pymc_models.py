import arviz as az
import numpy as np
import pandas as pd
import pymc as pm
import pytensor.tensor as pt
from memiliflow.memilio.graph_secirvvs import GraphSecirvvs
from memiliflow.simulation import simulation


def create_pymc_oseir_model(data: pd.DataFrame, prior=None):
    nu = 4
    offset_sigma = 1
    # https://www.biorxiv.org/content/10.1101/2024.01.30.577909v1.full.pdf infers first changepoint at 21
    population = 83e6
    data_at_start = data.iloc[0]

    mean_gamma_E = 0.25
    mean_gamma_I = 1 / 14

    R0 = data_at_start["Recovered"] + data_at_start["Deaths"]
    data["Daily new cases"] = data["Confirmed"] - data["Confirmed"].shift(1)
    data = data[data["Daily new cases"].notna()]

    sim_op = simulation.SimulatorForPyMC(
        simulator=simulation.create_oseir_new_cases_simulator(
            days_to_simulate=len(data.index), total_population=population
        )
    )
    with pm.Model() as pmodel:
        # priors
        initial_E = pm.HalfCauchy("initial E", beta=1000)
        initial_I = pm.HalfCauchy("initial I", beta=1000)
        initial_R = pm.Data("initial R", value=R0)
        gamma_E = pm.LogNormal("gamma_E", mu=np.log(mean_gamma_E), sigma=1)
        gamma_I = pm.math.constant(mean_gamma_I, "gamma_I")
        beta = pm.LogNormal("infection*contacts", mu=np.log(1), sigma=2)

        # memilio OSEIR model
        param_list = [initial_E, initial_I, initial_R, gamma_E, gamma_I, beta]
        theta = pt.as_tensor(param_list)
        sim_cases = pm.Deterministic("pred", sim_op(theta))
        # normality of 4 from Priesemann Paper on 2020 championship on spread of COVID-19, see https://www.nature.com/articles/s41467-022-35512-x
        sigma_obs = pm.HalfCauchy("prior scale", beta=30, shape=len(data.index))
        sigma = pt.abs(sim_cases + offset_sigma) ** 0.5 * sigma_obs
        daily_new_cases = pm.Data(
            "Daily new cases (moving average 7 days)", value=data["Daily new cases"].to_numpy()
        )
        pm.StudentT("likelihood", nu=nu, mu=sim_cases, sigma=sigma, observed=daily_new_cases)

    return pmodel


def create_pymc_secir_model(data: pd.DataFrame, prior=None):
    nu = 4
    offset_sigma = 1
    # https://www.biorxiv.org/content/10.1101/2024.01.30.577909v1.full.pdf infers first changepoint at 21
    population = 83e6
    data_at_start = data.iloc[0]

    R0 = data_at_start["Recovered"]
    D0 = data_at_start["Deaths"]
    data["Daily new cases"] = data["Confirmed"] - data["Confirmed"].shift(1)
    data = data[data["Daily new cases"].notna()]

    sim_op = simulation.SimulatorForPyMC(
        simulator=simulation.create_secir_new_cases_simulator(
            days_to_simulate=len(data.index), total_population=population
        )
    )
    with pm.Model() as pmodel:
        # priors
        initial_E = pm.HalfCauchy(name="initial E", beta=1000)
        initial_I_no_Sym = pm.HalfCauchy(name="initial I no Sym", beta=1000)
        initial_I_Sym = pm.HalfCauchy(name="initial I Sym", beta=1000)
        initial_I_Sev = pm.HalfCauchy(name="initial I Sev", beta=1000)
        initial_I_Crit = pm.HalfCauchy(name="initial I Crit", beta=1000)
        initial_R = pm.Data(name="initial R", value=R0)
        initial_D = pm.Data(name="initial D", value=D0)
        # time_exposed = pm.Uniform(lower=2.67, upper=4, name="time exposed")
        time_exposed = pm.Uniform(lower=2, upper=5, name="time exposed")

        time_infected_no_symptoms = pm.bounpm.LogNormal(
            mu=np.log(3), sigma=1, name="time infected no sym"
        )
        time_infected_symptoms = pm.LogNormal(mu=np.log(6), sigma=1, name="time infected sym")
        time_infected_severe = pm.LogNormal(mu=np.log(8), sigma=1, name="time infected sev")
        time_infected_critical = pm.LogNormal(mu=np.log(10), sigma=1, name="time infected critical")
        relative_transmission_no_symptoms = pm.Data(
            value=0.58, name="relative transmission no symptoms"
        )
        trans_mission_prob = pm.Data(value=1, name="aux")
        recovered_per_inf = pm.Data(value=0.17, name="recovered per infected no symptoms")

        risk_of_infection_from_sym = pm.Uniform(
            lower=0.01, upper=0.4, name="risk of infection from symptomatic"
        )
        severe_per_infected_symptoms = pm.Data(value=0.18, name="severe per infected symptoms")

        critical_per_severe_symptoms = pm.Data(value=0.14, name="critical per severe symptoms")

        deaths_per_critical_symptoms = pm.Data(value=0.47, name="deaths per critical symptoms")
        max_risk_infection_no_symptoms = pm.Data(value=1, name="max risk infection no symptoms")

        beta = pm.LogNormal(name="infection*contacts", mu=np.log(0.5), sigma=3)

        param_list = [
            initial_E,
            initial_I_no_Sym,
            initial_I_Sym,
            initial_I_Sev,
            initial_I_Crit,
            initial_R,
            initial_D,
            time_exposed,
            time_infected_no_symptoms,
            time_infected_symptoms,
            time_infected_severe,
            time_infected_critical,
            relative_transmission_no_symptoms,
            trans_mission_prob,
            recovered_per_inf,
            risk_of_infection_from_sym,
            severe_per_infected_symptoms,
            critical_per_severe_symptoms,
            deaths_per_critical_symptoms,
            max_risk_infection_no_symptoms,
            beta,
        ]
        theta = pt.as_tensor(param_list)

        sim_cases = pm.Deterministic("pred", sim_op(theta))
        # normality of 4 from Priesemann Paper on 2020 championship on spread of COVID-19, see https://www.nature.com/articles/s41467-022-35512-x
        sigma_obs = pm.HalfCauchy("prior scale", beta=30, shape=len(data.index))
        sigma = pt.abs(sim_cases + offset_sigma) ** 0.5 * sigma_obs
        daily_new_cases = pm.Data(
            "Daily new cases (moving average 7 days)", value=data["Daily new cases"].to_numpy()
        )
        pm.StudentT("likelihood", nu=nu, mu=sim_cases, sigma=sigma, observed=daily_new_cases)

    return pmodel


def create_pymc_secirvvs_model(data: pd.DataFrame, prior=None):
    nu = 4
    offset_sigma = 1
    # https://www.biorxiv.org/content/10.1101/2024.01.30.577909v1.full.pdf infers first changepoint at 21
    population = 83e6
    data_at_start = data.iloc[0]

    R0 = data_at_start["Recovered"]
    D0 = data_at_start["Deaths"]
    data["Daily new cases"] = data["Confirmed"] - data["Confirmed"].shift(1)
    data = data[data["Daily new cases"].notna()]

    sim_op = simulation.SimulatorForPyMC(
        simulator=simulation.create_secir_new_cases_simulator(
            days_to_simulate=len(data.index), total_population=population
        )
    )
    with pm.Model() as pmodel:
        # priors
        initial_E = pm.HalfCauchy(name="initial E", beta=1000)
        initial_I_no_Sym = pm.HalfCauchy(name="initial I no Sym", beta=1000)
        initial_I_Sym = pm.HalfCauchy(name="initial I Sym", beta=1000)
        initial_I_Sev = pm.HalfCauchy(name="initial I Sev", beta=1000)
        initial_I_Crit = pm.HalfCauchy(name="initial I Crit", beta=1000)
        initial_R = pm.Data(name="initial R", value=R0)
        initial_D = pm.Data(name="initial D", value=D0)
        # time_exposed = pm.Uniform(lower=2.67, upper=4, name="time exposed")
        time_exposed = pm.Uniform(lower=2, upper=5, name="time exposed")

        time_infected_no_symptoms = pm.bounpm.LogNormal(
            mu=np.log(3), sigma=1, name="time infected no sym"
        )
        time_infected_symptoms = pm.LogNormal(mu=np.log(6), sigma=1, name="time infected sym")
        time_infected_severe = pm.LogNormal(mu=np.log(8), sigma=1, name="time infected sev")
        time_infected_critical = pm.LogNormal(mu=np.log(10), sigma=1, name="time infected critical")
        relative_transmission_no_symptoms = pm.Data(
            value=0.58, name="relative transmission no symptoms"
        )
        trans_mission_prob = pm.Data(value=1, name="aux")
        recovered_per_inf = pm.Data(value=0.17, name="recovered per infected no symptoms")

        risk_of_infection_from_sym = pm.Uniform(
            lower=0.01, upper=0.4, name="risk of infection from symptomatic"
        )
        severe_per_infected_symptoms = pm.Data(value=0.18, name="severe per infected symptoms")

        critical_per_severe_symptoms = pm.Data(value=0.14, name="critical per severe symptoms")

        deaths_per_critical_symptoms = pm.Data(value=0.47, name="deaths per critical symptoms")
        max_risk_infection_no_symptoms = pm.Data(value=1, name="max risk infection no symptoms")

        beta = pm.LogNormal(name="infection*contacts", mu=np.log(0.5), sigma=3)

        param_list = [
            initial_E,
            initial_I_no_Sym,
            initial_I_Sym,
            initial_I_Sev,
            initial_I_Crit,
            initial_R,
            initial_D,
            time_exposed,
            time_infected_no_symptoms,
            time_infected_symptoms,
            time_infected_severe,
            time_infected_critical,
            relative_transmission_no_symptoms,
            trans_mission_prob,
            recovered_per_inf,
            risk_of_infection_from_sym,
            severe_per_infected_symptoms,
            critical_per_severe_symptoms,
            deaths_per_critical_symptoms,
            max_risk_infection_no_symptoms,
            beta,
        ]
        theta = pt.as_tensor(param_list)

        sim_cases = pm.Deterministic("pred", sim_op(theta))
        # normality of 4 from Priesemann Paper on 2020 championship on spread of COVID-19, see https://www.nature.com/articles/s41467-022-35512-x
        sigma_obs = pm.HalfCauchy("prior scale", beta=30, shape=len(data.index))
        sigma = pt.abs(sim_cases + offset_sigma) ** 0.5 * sigma_obs
        daily_new_cases = pm.Data(
            "Daily new cases (moving average 7 days)", value=data["Daily new cases"].to_numpy()
        )
        pm.StudentT("likelihood", nu=nu, mu=sim_cases, sigma=sigma, observed=daily_new_cases)

    return pmodel


# Pymc graph model is likely impossible with MCMC
# def create_pymc_graph_model(days_to_simulate=14, data_dir):
#     """Creates pymc model for inferring parameters of graph model of germany (SECIRVVS)"""
#     nu = 4
#     offset_sigma = 1
#
#     sim_op = simulation.SimulatorForPyMC(
#         simulator=GraphSecirvvs(
#             days_to_simulate=14,
#             total_population=data_dir
#         )
#     )
#
#     with pm.Model() as pmodel:
#         # priors
#         beta = pm.LogNormal(name="infection*contacts", mu=np.log(0.5), sigma=3)
#         param_list = [ beta ]
#         theta = pt.as_tensor(param_list)
#
#     #     sim_cases = pm.Deterministic("pred", sim_op(theta))
#     #     # normality of 4 from Priesemann Paper on 2020 championship on spread of COVID-19, see https://www.nature.com/articles/s41467-022-35512-x
#     #     sigma_obs = pm.HalfCauchy("prior scale", beta=30, shape=len(data.index))
#     #     sigma = pt.abs(sim_cases + offset_sigma) ** 0.5 * sigma_obs
#     #     daily_new_cases = pm.Data(
#     #         "Daily new cases (moving average 7 days)", value=data["Daily new cases"].to_numpy()
#     #     )
#     #     pm.StudentT("likelihood", nu=nu, mu=sim_cases, sigma=sigma, observed=daily_new_cases)
#     #
#     # return pmodel
