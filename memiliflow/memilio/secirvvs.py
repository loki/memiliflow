import logging
from datetime import date, timedelta
from enum import Enum
from typing import Mapping, Union

import numpy as np
import pandas as pd
from memilio.simulation import (
    AgeGroup,
    ContactMatrix,
    Damping,
    SimulationDay,
    osecirvvs,
)
from memilio.simulation.osecirvvs import (
    InfectionState,
    Model,
    Simulation,
    simulate,
    simulate_flows,
)

_log = logging.getLogger(__file__)


class Secirvvs:
    class Parameter(str, Enum):
        TIME_EXPOSED = "TimeExposed"
        TIME_INFECTED_NO_SYMPTOMS = "TimeInfectedNoSymptoms"
        TIME_INFECTED_SYMPTOMS = "TimeInfectedSymptoms"
        TIME_INFECTED_SEVERE = "TimeInfectedSevere"
        TIME_INFECTED_CRITICAL = "TimeInfectedCritical"
        TRANSMISSION_PROBABILITY = "TransmissionProbabilityOnContact"
        RELATIVE_TRANSMISSION_NO_SYMPTOMS = "RelativeTransmissionNoSymptoms"
        RISK_OF_INFECTION_FROM_SYMPTOMATIC = "RiskOfInfectionFromSymptomatic"
        RECOVERED_PER_INFECTED_NO_SYMPTOMS = "RecoveredPerInfectedNoSymptoms"
        SEVERE_PER_INFECTED_SYMPTOMS = "SeverePerInfectedSymptoms"
        CRITICAL_PER_SEVERE = "CriticalPerSevere"
        DEATHS_PER_CRITICAL = "DeathsPerCritical"
        REDUC_EXPOSED_PARTIAL_IMMUNITY = "ReducExposedPartialImmunity"
        REDUC_EXPOSED_IMPROVED_IMMUNITY = "ReducExposedImprovedImmunity"
        REDUC_TIME_INFECTED_MILD = "ReducTimeInfectedMild"
        SEASONALITY = "Seasonality"
        REDUC_INFECTED_SYMPTOMS_PARTIAL_IMMUNITY = "ReducInfectedSymptomsPartialImmunity"
        REDUC_INFECTED_SYMPTOMS_IMPROVED_IMMUNITY = "ReducInfectedSymptomsImprovedImmunity"
        REDUC_INFECTED_SEVERE_CRITICAL_DEAD_PARTIAL_IMMUNITY = (
            "ReducInfectedSevereCriticalDeadPartialImmunity"
        )
        REDUC_INFECTED_SEVERE_CRITICAL_DEAD_IMPROVED_IMMUNITY = (
            "ReducInfectedSevereCriticalDeadImprovedImmunity"
        )
        CONTACT_MATRIX = "ContactMatrix"

    class Compartment(str, Enum):
        SUSCEPTIBLE_NAIVE = "SusceptibleNaive"
        # [ < InfectionState.SusceptibleNaive: 0,
        SUSCEPTIBLE_PARTIAL_IMMUNITY = "SusceptiblePartialImmunity"
        # < InfectionState.SusceptiblePartialImmunity: 1 >,
        SUSCEPTIBLE_IMPROVED_IMMUNITY = "SusceptibleImprovedImmunity"
        # < InfectionState.SusceptibleImprovedImmunity: 23 >,
        EXPOSED_NAIVE = "ExposedNaive"
        # < InfectionState.ExposedNaive: 2 >,
        EXPOSED_PARTIAL_IMMUNITY = "ExposedPartialImmunity"
        # < InfectionState.ExposedPartialImmunity: 3 >,
        EXPOSED_IMPROVED_IMMUNITY = "ExposedImprovedImmunity"
        # < InfectionState.ExposedImprovedImmunity: 4 >,
        INFECTED_NO_SYMPTOMS_NAIVE = "InfectedNoSymptomsNaive"
        # < InfectionState.InfectedNoSymptomsNaive: 5 >,
        INFECTED_NO_SYMPTOMS_PARTIAL_IMMUNITY = "InfectedNoSymptomsPartialImmunity"
        # < InfectionState.InfectedNoSymptomsPartialImmunity: 6 >,
        INFECTED_NO_SYMPTOMS_IMPROVED_IMMUNITY = "InfectedNoSymptomsImprovedImmunity"
        # < InfectionState.InfectedNoSymptomsImprovedImmunity: 7 >,
        INFECTED_NO_SYMPTOMS_NAIVE_CONFIRMED = "InfectedNoSymptomsNaiveConfirmed"
        # < InfectionState.InfectedNoSymptomsNaiveConfirmed: 8 >,
        INFECTED_NO_SYMPTOMS_PARTIAL_IMMUNITY_CONFIRMED = (
            "InfectedNoSymptomsPartialImmunityConfirmed"
        )
        # < InfectionState.InfectedNoSymptomsPartialImmunityConfirmed: 9 >,
        INFECTED_NO_SYMPTOMS_IMPROVED_IMMUNITY_CONFIRMED = (
            "InfectedNoSymptomsImprovedImmunityConfirmed"
        )
        # < InfectionState.InfectedNoSymptomsImprovedImmunityConfirmed: 10 >,
        INFECTED_SYMPTOMS_NAIVE = "InfectedSymptomsNaive"
        # < InfectionState.InfectedSymptomsNaive: 11 >,
        INFECTED_SYMPTOMS_PARTIAL_IMMUNITY = "InfectedSymptomsPartialImmunity"
        # < InfectionState.InfectedSymptomsPartialImmunity: 12 >,
        INFECTED_SYMPTOMS_IMPROVED_IMMUNITY = "InfectedSymptomsImprovedImmunity"
        # < InfectionState.InfectedSymptomsImprovedImmunity: 13 >,
        INFECTED_SYMPTOMS_NAIVE_CONFIRMED = "InfectedSymptomsNaiveConfirmed"
        # < InfectionState.InfectedSymptomsNaiveConfirmed: 14 >,
        INFECTED_SYMPTOMS_PARTIAL_IMMUNITY_CONFIRMED = "InfectedSymptomsPartialImmunityConfirmed"
        # < InfectionState.InfectedSymptomsPartialImmunityConfirmed: 15 >,
        INFECTED_SYMPTOMS_IMPROVED_IMMUNITY_CONFIRMED = "InfectedSymptomsImprovedImmunityConfirmed"
        # < InfectionState.InfectedSymptomsImprovedImmunityConfirmed: 16 >,
        INFECTED_SEVERE_NAIVE = "InfectedSevereNaive"
        # < InfectionState.InfectedSevereNaive: 17 >,
        INFECTED_SEVERE_PARTIAL_IMMUNITY = "InfectedSeverePartialImmunity"
        # < InfectionState.InfectedSeverePartialImmunity: 18 >,
        INFECTED_SEVERE_IMPROVED_IMMUNITY = "InfectedSevereImprovedImmunity"
        # < InfectionState.InfectedSevereImprovedImmunity: 19 >,
        INFECTED_CRITICAL_NAIVE = "InfectedCriticalNaive"
        # < InfectionState.InfectedCriticalNaive: 20 >,
        INFECTED_CRITICAL_PARTIAL_IMMUNITY = "InfectedCriticalPartialImmunity"
        # < InfectionState.InfectedCriticalPartialImmunity: 21 >,
        INFECTED_CRITICAL_IMPROVED_IMMUNITY = "InfectedCriticalImprovedImmunity"
        # < InfectionState.InfectedCriticalImprovedImmunity: 22 >,
        DEAD_NAIVE = "DeadNaive"
        # < InfectionState.DeadNaive: 24 >,
        DEAD_PARTIAL_IMMUNITY = "DeadPartialImmunity"
        # < InfectionState.DeadPartialImmunity: 25 >,
        DEAD_IMPROVED_IMMUNITY = "DeadImprovedImmunity"
        # < InfectionState.DeadImprovedImmunity: 26 >]

    @staticmethod
    def numpy_to_dict(values: np.array, enum_class, age_group):
        """Takes numpy array and sorts it into dictionary according to enum"""
        result = {}
        assert len(enum_class) == values.shape[0]
        for index, p in enumerate(list(enum_class)):
            result[p] = values[index]
        return {age_group: result}

    @staticmethod
    def _set_compartments(
        model: osecirvvs.Model, compartments: Mapping[Compartment, float], age_group
    ):
        for compartment, value in dict(compartments).items():
            model.populations[age_group, getattr(InfectionState, compartment.value)] = value
        return model

    @staticmethod
    def _set_parameters(model, parameters, age_group):
        for parameter, value in dict(parameters).items():
            match parameter:
                case Secirvvs.Parameter.CONTACT_MATRIX:
                    # TODO update for more age groups
                    model.parameters.ContactPatterns.cont_freq_mat[0] = ContactMatrix(np.r_[value])
                case Secirvvs.Parameter.SEASONALITY:
                    getattr(model.parameters, str(parameter.value)).value = value
                case _:
                    getattr(model.parameters, str(parameter.value))[age_group].value = value

        return model

    @staticmethod
    def simulate(
        initial_condition: np.array,
        parameters: np.array,
        start_date=None,
        duration: float = 10,
        step: float = 0.1,
        interpolation_target: np.ndarray = None,
        age_groups={0: AgeGroup(0)},
    ):
        if len(age_groups) != 1:
            raise NotImplementedError("More than 1 age group not yet supported")

        age_group = list(age_groups.keys())[0]
        initial_condition_dict = Secirvvs.numpy_to_dict(
            initial_condition, Secirvvs.Compartment, age_group=age_group
        )
        parameters_dict = Secirvvs.numpy_to_dict(
            parameters, Secirvvs.Parameter, age_group=age_group
        )
        return Secirvvs.simulate_dicts(
            initial_condition_dict,
            parameters_dict,
            start_date=start_date,
            duration=duration,
            step=step,
            interpolation_target=interpolation_target,
            age_groups=age_groups,
        )

    @staticmethod
    def simulate_dicts(
        initial_condition: Mapping[AgeGroup, Mapping[osecirvvs.InfectionState, float]],
        parameters: Mapping[Parameter, Union[float, np.ndarray, None]],
        start_date=None,
        duration: float = 10,
        step: float = 0.1,
        interpolation_target: np.ndarray = None,
        age_groups={0: AgeGroup(0)},
    ):
        if len(age_groups) != 1:
            raise NotImplementedError("More than 1 age group not yet supported")

        if interpolation_target is None:
            interpolation_target = np.array([i for i in range(int(duration) + 1)])
        model = osecirvvs.Model(num_agegroups=len(age_groups))
        for i, age_group in age_groups.items():
            model = Secirvvs._set_compartments(
                model=model, compartments=initial_condition[i], age_group=age_group
            )
            model = Secirvvs._set_parameters(model, parameters=parameters[i], age_group=age_group)

        model.parameters.DailyFirstVaccination.resize_SimulationDay(
            SimulationDay(int(duration) + 1)
        )
        model.parameters.DailyFirstVaccination[:, :] = 0
        model.parameters.DailyFullVaccination.resize_SimulationDay(SimulationDay(int(duration) + 1))
        model.parameters.DailyFullVaccination[:, :] = 0

        t0 = 0
        # if check constraints throws a warning, the simulation is bad! Try to select priors so that it does not happen
        model.check_constraints()
        # right now osecirvvs already interpolates
        simulation_result = (
            osecirvvs.simulate(t0, duration, model=model, dt=step).as_ndarray().copy()
        )
        interpolated = simulation_result

        simulated_data = pd.DataFrame(
            data=interpolated.transpose(),
            columns=[
                "Date",
                Secirvvs.Compartment.SUSCEPTIBLE_NAIVE.value,
                Secirvvs.Compartment.SUSCEPTIBLE_PARTIAL_IMMUNITY.value,
                Secirvvs.Compartment.EXPOSED_NAIVE.value,
                Secirvvs.Compartment.EXPOSED_PARTIAL_IMMUNITY.value,
                Secirvvs.Compartment.EXPOSED_IMPROVED_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_NO_SYMPTOMS_NAIVE.value,
                Secirvvs.Compartment.INFECTED_NO_SYMPTOMS_PARTIAL_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_NO_SYMPTOMS_IMPROVED_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_NO_SYMPTOMS_NAIVE_CONFIRMED.value,
                Secirvvs.Compartment.INFECTED_NO_SYMPTOMS_PARTIAL_IMMUNITY_CONFIRMED.value,
                Secirvvs.Compartment.INFECTED_NO_SYMPTOMS_IMPROVED_IMMUNITY_CONFIRMED.value,
                Secirvvs.Compartment.INFECTED_SYMPTOMS_NAIVE.value,
                Secirvvs.Compartment.INFECTED_SYMPTOMS_PARTIAL_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_SYMPTOMS_IMPROVED_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_SYMPTOMS_NAIVE_CONFIRMED.value,
                Secirvvs.Compartment.INFECTED_SYMPTOMS_PARTIAL_IMMUNITY_CONFIRMED.value,
                Secirvvs.Compartment.INFECTED_SYMPTOMS_IMPROVED_IMMUNITY_CONFIRMED.value,
                Secirvvs.Compartment.INFECTED_SEVERE_NAIVE.value,
                Secirvvs.Compartment.INFECTED_SEVERE_PARTIAL_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_SEVERE_IMPROVED_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_CRITICAL_NAIVE.value,
                Secirvvs.Compartment.INFECTED_CRITICAL_PARTIAL_IMMUNITY.value,
                Secirvvs.Compartment.INFECTED_CRITICAL_IMPROVED_IMMUNITY.value,
                Secirvvs.Compartment.SUSCEPTIBLE_IMPROVED_IMMUNITY.value,
                Secirvvs.Compartment.DEAD_NAIVE.value,
                Secirvvs.Compartment.DEAD_PARTIAL_IMMUNITY.value,
                Secirvvs.Compartment.DEAD_IMPROVED_IMMUNITY.value,
            ],
        )

        # interpolation hack for now
        for index, row in simulated_data.iterrows():
            if not row["Date"].is_integer():
                simulated_data.drop(index, inplace=True)
        simulated_data.reset_index(inplace=True, drop=True)

        if start_date is not None:
            simulated_data["Date"] = simulated_data["Date"].apply(
                lambda x: start_date + timedelta(days=x)
            )
        else:
            simulated_data["Date"] = simulated_data["Date"].apply(lambda x: timedelta(days=x))

        return simulated_data
