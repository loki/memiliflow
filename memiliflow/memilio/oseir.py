import logging
from datetime import date, timedelta
from enum import Enum
from typing import Mapping, Union

import numpy as np
import pandas as pd
from memilio.simulation import AgeGroup, Damping, oseir

_log = logging.getLogger(__file__)


class Oseir:
    A0 = AgeGroup(0)

    class Parameter(str, Enum):
        TIME_EXPOSED = "TimeExposed"
        TIME_INFECTED = "TimeInfected"
        TRANSMISSION_PROBABILITY = "TransmissionProbabilityOnContact"
        CONTACT_BASELINE = "ContactBaseline"
        CONTACT_MINIMUM = "ContactMinimum"
        CONTACT_DAMPING = "ContactDamping"

    class Compartment(str, Enum):
        SUSCEPTIBLE = "Susceptible"
        EXPOSED = "Exposed"
        INFECTED = "Infected"
        RECOVERED = "Recovered"

    @staticmethod
    def numpy_to_dict(params: np.array, enumClass):
        """Takes numpy array and sorts it into dictionary according to enum"""
        result = {}
        assert len(enumClass) == params.shape[0]
        for index, p in enumerate(enumClass):
            result[p] = params[index]
        return result

    @staticmethod
    def _set_compartments(model: oseir.Model, compartments: Mapping[Compartment, float] = {}):
        for compartment, value in dict(compartments).items():
            match compartment:
                case Oseir.Compartment.SUSCEPTIBLE:
                    model.populations[
                        Oseir.A0, oseir.Index_InfectionState(oseir.InfectionState.Susceptible)
                    ] = value
                case Oseir.Compartment.EXPOSED:
                    model.populations[
                        Oseir.A0, oseir.Index_InfectionState(oseir.InfectionState.Exposed)
                    ] = value
                case Oseir.Compartment.INFECTED:
                    model.populations[
                        Oseir.A0, oseir.Index_InfectionState(oseir.InfectionState.Infected)
                    ] = value
                case Oseir.Compartment.RECOVERED:
                    model.populations[
                        Oseir.A0, oseir.Index_InfectionState(oseir.InfectionState.Recovered)
                    ] = value
        return model

    @staticmethod
    def _set_parameters(model, parameters):
        for parameter, value in dict(parameters).items():
            match parameter:
                case Oseir.Parameter.CONTACT_DAMPING:
                    model.parameters.ContactPatterns.cont_freq_mat.add_damping(
                        Damping(coeffs=np.array([[value]]), t=0)
                    )
                case Oseir.Parameter.CONTACT_BASELINE:
                    model.parameters.ContactPatterns.cont_freq_mat[0].baseline = np.array([[value]])
                case Oseir.Parameter.CONTACT_MINIMUM:
                    model.parameters.ContactPatterns.cont_freq_mat[0].minimum = np.array([[value]])
                case _:
                    getattr(model.parameters, str(parameter.value))[Oseir.A0].value = value

        return model

    @staticmethod
    def simulate(
        initial_condition: np.array,
        parameters: np.array,
        start_date=None,
        duration: float = 10,
        step: float = 0.1,
        interpolation_target: np.ndarray = None,
    ):
        initial_condition_dict = Oseir.numpy_to_dict(initial_condition, Oseir.Compartment)
        parameters_dict = Oseir.numpy_to_dict(parameters, Oseir.Parameter)
        return Oseir.simulate_dicts(
            initial_condition_dict,
            parameters_dict,
            start_date=start_date,
            duration=duration,
            step=step,
            interpolation_target=interpolation_target,
        )

    @staticmethod
    def simulate_dicts(
        initial_condition: Mapping[Compartment, float],
        parameters: Mapping[Parameter, Union[float, np.ndarray, None]],
        start_date=None,
        duration: float = 10,
        step: float = 0.1,
        interpolation_target: np.ndarray = None,
    ):

        if interpolation_target is None:
            interpolation_target = np.array([i for i in range(int(duration) + 1)])
        model = oseir.Model(1)
        model = Oseir._set_compartments(model=model, compartments=initial_condition)
        model = Oseir._set_parameters(model, parameters=parameters)

        t0 = 0
        simulation_results = oseir.simulate(t0, duration, step, model)

        if interpolation_target is not None:
            interpolated = oseir.interpolate_simulation_result(
                simulation_results, interpolation_target
            )
        else:
            interpolated = simulation_results

        simulated_data = pd.DataFrame(
            data=interpolated.as_ndarray().copy().transpose(),
            columns=[
                "Date",
                Oseir.Compartment.SUSCEPTIBLE.value,
                Oseir.Compartment.EXPOSED.value,
                Oseir.Compartment.INFECTED.value,
                Oseir.Compartment.RECOVERED.value,
            ],
        )
        if start_date is not None:
            simulated_data["Date"] = simulated_data["Date"].apply(
                lambda x: start_date + timedelta(days=x)
            )
        else:
            simulated_data["Date"] = simulated_data["Date"].apply(lambda x: timedelta(days=x))

        return simulated_data
