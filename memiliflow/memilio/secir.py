import logging
from datetime import date, timedelta
from enum import Enum
from typing import Mapping, Union

import numpy as np
import pandas as pd
from memilio.simulation import AgeGroup, ContactMatrix, secir

_log = logging.getLogger(__file__)


class Secir:
    class Parameter(str, Enum):
        TIME_EXPOSED = "TimeExposed"
        TIME_INFECTED_NO_SYMPTOMS = "TimeInfectedNoSymptoms"
        TIME_INFECTED_SYMPTOMS = "TimeInfectedSymptoms"
        TIME_INFECTED_SEVERE = "TimeInfectedSevere"
        TIME_INFECTED_CRITICAL = "TimeInfectedCritical"
        RELATIVE_TRANSMISSION_NO_SYMPTOMS = "RelativeTransmissionNoSymptoms"
        TRANSMISSION_PROBABILITY = "TransmissionProbabilityOnContact"
        RECOVERED_PER_INFECTED_NO_SYMPTOMS = "RecoveredPerInfectedNoSymptoms"
        RISK_OF_INFECTION_FROM_SYMPTOMATIC = "RiskOfInfectionFromSymptomatic"
        SEVERE_PER_INFECTED_SYMPTOMS = "SeverePerInfectedSymptoms"
        CRITICAL_PER_SEVERE = "CriticalPerSevere"
        DEATHS_PER_CRITICAL = "DeathsPerCritical"
        MAX_RISK_OF_INFECTION_FROM_SYMPTOMATIC = "MaxRiskOfInfectionFromSymptomatic"
        CONTACT_MATRIX = "ContactMatrix"

    class Compartment(str, Enum):
        SUSCEPTIBLE = "Susceptible"
        EXPOSED = "Exposed"
        INFECTED_NO_SYMPTOMS = "InfectedNoSymptoms"
        INFECTED_NO_SYMPTOMS_CONFIRMED = "InfectedNoSymptomsConfirmed"
        INFECTED_SYMPTOMS = "InfectedSymptoms"
        INFECTED_SYMPTOMS_CONFIRMED = "InfectedSymptomsConfirmed"
        INFECTED_SEVERE = "InfectedSevere"
        INFECTED_CRITICAL = "InfectedCritical"
        RECOVERED = "Recovered"
        DEAD = "Dead"

    @staticmethod
    def numpy_to_dict(params: np.array, enum_class, age_group):
        """Takes numpy array and sorts it into dictionary according to enum"""
        result = {}
        assert len(enum_class) == params.shape[0]
        for index, p in enumerate(enum_class):
            result[p] = params[index]
        return {age_group: result}

    @staticmethod
    def _set_compartments(model: secir.Model, compartments: Mapping[Compartment, float], age_group):
        for compartment, value in dict(compartments).items():
            match compartment:
                case Secir.Compartment.SUSCEPTIBLE:
                    model.populations[age_group, secir.InfectionState.Susceptible] = value
                case Secir.Compartment.EXPOSED:
                    model.populations[age_group, secir.InfectionState.Exposed] = value
                case Secir.Compartment.INFECTED_NO_SYMPTOMS:
                    model.populations[age_group, secir.InfectionState.InfectedNoSymptoms] = value
                case Secir.Compartment.INFECTED_NO_SYMPTOMS_CONFIRMED:
                    model.populations[
                        age_group, secir.InfectionState.InfectedNoSymptomsConfirmed
                    ] = value
                case Secir.Compartment.INFECTED_SYMPTOMS:
                    model.populations[age_group, secir.InfectionState.InfectedSymptoms] = value
                case Secir.Compartment.INFECTED_SYMPTOMS_CONFIRMED:
                    model.populations[
                        age_group, secir.InfectionState.InfectedSymptomsConfirmed
                    ] = value
                case Secir.Compartment.INFECTED_SEVERE:
                    model.populations[age_group, secir.InfectionState.InfectedSevere] = value
                case Secir.Compartment.INFECTED_CRITICAL:
                    model.populations[age_group, secir.InfectionState.InfectedCritical] = value
                case Secir.Compartment.RECOVERED:
                    model.populations[age_group, secir.InfectionState.Recovered] = value
                case Secir.Compartment.DEAD:
                    model.populations[age_group, secir.InfectionState.Dead] = value
        return model

    @staticmethod
    def _set_parameters(model, parameters, age_group):
        for parameter, value in dict(parameters).items():
            match parameter:
                case Secir.Parameter.CONTACT_MATRIX:
                    # TODO update for more age groups
                    model.parameters.ContactPatterns.cont_freq_mat[0] = ContactMatrix(np.r_[value])
                case _:
                    getattr(model.parameters, str(parameter.value))[age_group].value = value

        return model

    @staticmethod
    def simulate(
        initial_condition: np.array,
        parameters: np.array,
        start_date=None,
        duration: float = 10,
        step: float = 0.1,
        interpolation_target: np.ndarray = None,
        age_groups={0: AgeGroup(0)},
    ):
        if len(age_groups) != 1:
            raise NotImplementedError("More than 1 age group not yet supported")

        age_group = list(age_groups.keys())[0]
        initial_condition_dict = Secir.numpy_to_dict(
            initial_condition, Secir.Compartment, age_group=age_group
        )
        parameters_dict = Secir.numpy_to_dict(parameters, Secir.Parameter, age_group=age_group)
        return Secir.simulate_dicts(
            initial_condition_dict,
            parameters_dict,
            start_date=start_date,
            duration=duration,
            step=step,
            interpolation_target=interpolation_target,
            age_groups=age_groups,
        )

    @staticmethod
    def simulate_dicts(
        initial_condition: Mapping[AgeGroup, Mapping[Compartment, float]],
        parameters: Mapping[Parameter, Union[float, np.ndarray, None]],
        start_date=None,
        duration: float = 10,
        step: float = 0.1,
        interpolation_target: np.ndarray = None,
        age_groups={0: AgeGroup(0)},
    ):
        if len(age_groups) != 1:
            raise NotImplementedError("More than 1 age group not yet supported")

        if interpolation_target is None:
            interpolation_target = np.array([i for i in range(int(duration) + 1)])
        model = secir.Model(num_agegroups=len(age_groups))
        for i, age_group in age_groups.items():
            model = Secir._set_compartments(
                model=model, compartments=initial_condition[i], age_group=age_group
            )
            model = Secir._set_parameters(model, parameters=parameters[i], age_group=age_group)

        t0 = 0
        # if check constraints throws a warning, the simulation is bad! Try to select priors so that it does not happen
        check = model.check_constraints()
        simulation_results = secir.simulate(t0, duration, model=model, dt=step)

        if interpolation_target is not None:
            interpolated = secir.interpolate_simulation_result(
                simulation_results, interpolation_target
            )
        else:
            interpolated = simulation_results

        simulated_data = pd.DataFrame(
            data=interpolated.as_ndarray().copy().transpose(),
            columns=[
                "Date",
                Secir.Compartment.SUSCEPTIBLE.value,
                Secir.Compartment.EXPOSED.value,
                Secir.Compartment.INFECTED_NO_SYMPTOMS.value,
                Secir.Compartment.INFECTED_NO_SYMPTOMS_CONFIRMED.value,
                Secir.Compartment.INFECTED_SYMPTOMS.value,
                Secir.Compartment.INFECTED_SYMPTOMS_CONFIRMED.value,
                Secir.Compartment.INFECTED_SEVERE.value,
                Secir.Compartment.INFECTED_CRITICAL.value,
                Secir.Compartment.RECOVERED.value,
                Secir.Compartment.DEAD.value,
            ],
        )
        if start_date is not None:
            simulated_data["Date"] = simulated_data["Date"].apply(
                lambda x: start_date + timedelta(days=x)
            )
        else:
            simulated_data["Date"] = simulated_data["Date"].apply(lambda x: timedelta(days=x))

        return simulated_data
