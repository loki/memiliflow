import os
from datetime import date
from enum import Enum
from time import perf_counter

import memilio.simulation as mio
import memilio.simulation.osecirvvs as osecirvvs
import numpy as np


class Location(Enum):
    Home = 0
    School = 1
    Work = 2
    Other = 3


class GraphSecirvvs:
    def __init__(self, start_date: date, data_dir: str, num_days_sim=14):
        self.num_groups = 6
        self.data_dir = data_dir
        self.num_days_sim = num_days_sim

        year = start_date.year
        month = start_date.month
        day = start_date.day
        self.start_date = mio.Date(year=year, month=month, day=day)
        self.end_date = self.start_date + num_days_sim

        self.graph = self.build_graph(end_date=self.end_date)

    def build_graph(self, end_date):
        self.model = osecirvvs.Model(self.num_groups)
        self.set_covid_parameters(self.model)
        self.set_contact_matrices(self.model)
        # self.set_npis(model.parameters, end_date)

        graph = osecirvvs.ModelGraph()

        scaling_factor_infected = np.ones(self.num_groups)
        scaling_factor_icu = 1.0
        tnt_capacity_factor = 1.43 / 100000.0

        path_population_data = os.path.join(
            self.data_dir, "pydata", "Germany", "county_current_population.json"
        )

        osecirvvs.set_nodes(
            self.model.parameters,
            self.start_date,
            end_date,
            self.data_dir,
            path_population_data,
            True,
            graph,
            scaling_factor_infected,
            scaling_factor_icu,
            tnt_capacity_factor,
            end_date - self.start_date,
            False,
        )

        osecirvvs.set_edges(self.data_dir, graph, len(Location))

        return graph

    def set_covid_parameters(self, model):
        def array_assign_uniform_distribution(param, min, max, num_groups=6, epsilon=0):
            if isinstance(min, (int, float)) and isinstance(max, (int, float)):
                min = min * np.ones(num_groups)
                max = max * np.ones(num_groups)
            elif not (isinstance(min, (list)) and isinstance(max, (list))):
                raise TypeError(
                    "Invalid type for parameter 'min' or 'max. \
                            Expected a scalar or a list. Must be the same for both."
                )
            for i in range(num_groups):
                value = 0.5 * (max[i] + min[i])
                param[mio.AgeGroup(i)] = mio.UncertainValue(value)
                param[mio.AgeGroup(i)].set_distribution(
                    mio.ParameterDistributionUniform(value - epsilon, value + epsilon)
                )

        # times
        timeExposedMin = 2.67
        timeExposedMax = 4.0
        timeInfectedNoSymptomsMin = 1.2
        timeInfectedNoSymptomsMax = 2.53
        timeInfectedSymptomsMin = [5.6255, 5.6255, 5.6646, 5.5631, 5.501, 5.465]
        timeInfectedSymptomsMax = [8.427, 8.427, 8.4684, 8.3139, 8.169, 8.085]
        timeInfectedSevereMin = [3.925, 3.925, 4.85, 6.4, 7.2, 9.0]
        timeInfectedSevereMax = [6.075, 6.075, 7.0, 8.7, 9.8, 13.0]
        timeInfectedCriticalMin = [4.95, 4.95, 4.86, 14.14, 14.4, 10.0]
        timeInfectedCriticalMax = [8.95, 8.95, 8.86, 20.58, 19.8, 13.2]

        array_assign_uniform_distribution(
            model.parameters.TimeExposed, timeExposedMin, timeExposedMax
        )

        array_assign_uniform_distribution(
            model.parameters.TimeInfectedNoSymptoms,
            timeInfectedNoSymptomsMin,
            timeInfectedNoSymptomsMax,
        )

        array_assign_uniform_distribution(
            model.parameters.TimeInfectedSymptoms, timeInfectedSymptomsMin, timeInfectedSymptomsMax
        )

        array_assign_uniform_distribution(
            model.parameters.TimeInfectedSevere, timeInfectedSevereMin, timeInfectedSevereMax
        )

        array_assign_uniform_distribution(
            model.parameters.TimeInfectedCritical, timeInfectedCriticalMin, timeInfectedCriticalMax
        )

        # probabilities
        variantFactor = 1.4
        transmissionProbabilityOnContactMin = [
            0.02 * variantFactor,
            0.05 * variantFactor,
            0.05 * variantFactor,
            0.05 * variantFactor,
            0.08 * variantFactor,
            0.1 * variantFactor,
        ]
        transmissionProbabilityOnContactMax = [
            0.04 * variantFactor,
            0.07 * variantFactor,
            0.07 * variantFactor,
            0.07 * variantFactor,
            0.10 * variantFactor,
            0.15 * variantFactor,
        ]
        relativeTransmissionNoSymptomsMin = 0.5
        relativeTransmissionNoSymptomsMax = 0.5

        # The precise value between Risk* (situation under control) and MaxRisk* (situation not under control)
        # depends on incidence and test and trace capacity
        riskOfInfectionFromSymptomaticMin = 0.0
        riskOfInfectionFromSymptomaticMax = 0.2
        maxRiskOfInfectionFromSymptomaticMin = 0.4
        maxRiskOfInfectionFromSymptomaticMax = 0.5
        recoveredPerInfectedNoSymptomsMin = [0.2, 0.2, 0.15, 0.15, 0.15, 0.15]
        recoveredPerInfectedNoSymptomsMax = [0.3, 0.3, 0.25, 0.25, 0.25, 0.25]
        severePerInfectedSymptomsMin = [0.006, 0.006, 0.015, 0.049, 0.15, 0.20]
        severePerInfectedSymptomsMax = [0.009, 0.009, 0.023, 0.074, 0.18, 0.25]
        criticalPerSevereMin = [0.05, 0.05, 0.05, 0.10, 0.25, 0.35]
        criticalPerSevereMax = [0.10, 0.10, 0.10, 0.20, 0.35, 0.45]
        deathsPerCriticalMin = [0.00, 0.00, 0.10, 0.10, 0.30, 0.5]
        deathsPerCriticalMax = [0.10, 0.10, 0.18, 0.18, 0.50, 0.7]

        reducExposedPartialImmunityMin = 0.75
        reducExposedPartialImmunityMax = 0.85
        reducExposedImprovedImmunityMin = 0.281
        reducExposedImprovedImmunityMax = 0.381
        reducInfectedSymptomsPartialImmunityMin = 0.6
        reducInfectedSymptomsPartialImmunityMax = 0.7
        reducInfectedSymptomsImprovedImmunityMin = 0.193
        reducInfectedSymptomsImprovedImmunityMax = 0.293
        reducInfectedSevereCriticalDeadPartialImmunityMin = 0.05
        reducInfectedSevereCriticalDeadPartialImmunityMax = 0.15
        reducInfectedSevereCriticalDeadImprovedImmunityMin = 0.041
        reducInfectedSevereCriticalDeadImprovedImmunityMax = 0.141

        temp_reducTimeInfectedMild = 0.5
        reducTimeInfectedMild = temp_reducTimeInfectedMild

        array_assign_uniform_distribution(
            model.parameters.TransmissionProbabilityOnContact,
            transmissionProbabilityOnContactMin,
            transmissionProbabilityOnContactMax,
        )

        array_assign_uniform_distribution(
            model.parameters.RelativeTransmissionNoSymptoms,
            relativeTransmissionNoSymptomsMin,
            relativeTransmissionNoSymptomsMax,
        )

        array_assign_uniform_distribution(
            model.parameters.RiskOfInfectionFromSymptomatic,
            riskOfInfectionFromSymptomaticMin,
            riskOfInfectionFromSymptomaticMax,
        )

        array_assign_uniform_distribution(
            model.parameters.MaxRiskOfInfectionFromSymptomatic,
            maxRiskOfInfectionFromSymptomaticMin,
            maxRiskOfInfectionFromSymptomaticMax,
        )

        array_assign_uniform_distribution(
            model.parameters.RecoveredPerInfectedNoSymptoms,
            recoveredPerInfectedNoSymptomsMin,
            recoveredPerInfectedNoSymptomsMax,
        )

        array_assign_uniform_distribution(
            model.parameters.SeverePerInfectedSymptoms,
            severePerInfectedSymptomsMin,
            severePerInfectedSymptomsMax,
        )

        array_assign_uniform_distribution(
            model.parameters.CriticalPerSevere, criticalPerSevereMin, criticalPerSevereMax
        )

        array_assign_uniform_distribution(
            model.parameters.DeathsPerCritical, deathsPerCriticalMin, deathsPerCriticalMax
        )

        array_assign_uniform_distribution(
            model.parameters.ReducExposedPartialImmunity,
            reducExposedPartialImmunityMin,
            reducExposedPartialImmunityMax,
        )

        array_assign_uniform_distribution(
            model.parameters.ReducExposedImprovedImmunity,
            reducExposedImprovedImmunityMin,
            reducExposedImprovedImmunityMax,
        )

        array_assign_uniform_distribution(
            model.parameters.ReducInfectedSymptomsPartialImmunity,
            reducInfectedSymptomsPartialImmunityMin,
            reducInfectedSymptomsPartialImmunityMax,
        )

        array_assign_uniform_distribution(
            model.parameters.ReducInfectedSymptomsImprovedImmunity,
            reducInfectedSymptomsImprovedImmunityMin,
            reducInfectedSymptomsImprovedImmunityMax,
        )

        array_assign_uniform_distribution(
            model.parameters.ReducInfectedSevereCriticalDeadPartialImmunity,
            reducInfectedSevereCriticalDeadPartialImmunityMin,
            reducInfectedSevereCriticalDeadPartialImmunityMax,
        )

        array_assign_uniform_distribution(
            model.parameters.ReducInfectedSevereCriticalDeadImprovedImmunity,
            reducInfectedSevereCriticalDeadImprovedImmunityMin,
            reducInfectedSevereCriticalDeadImprovedImmunityMax,
        )

        array_assign_uniform_distribution(
            model.parameters.ReducTimeInfectedMild, reducTimeInfectedMild, reducTimeInfectedMild
        )

        # start day is set to the n-th day of the year
        model.parameters.StartDay = self.start_date.day_in_year

        model.parameters.Seasonality.value = 0.2

        seasonality_min = 0.1
        seasonality_max = 0.3
        model.parameters.Seasonality = mio.UncertainValue(0.5 * (seasonality_max + seasonality_min))
        model.parameters.Seasonality.set_distribution(
            mio.ParameterDistributionUniform(seasonality_min, seasonality_max)
        )
        # model.parameters.StartDayNewVariant = mio.Date(2021, 6, 6).day_in_year

    def set_contact_matrices(self, model):
        contact_matrices = mio.ContactMatrixGroup(len(list(Location)), self.num_groups)
        locations = ["home", "school_pf_eig", "work", "other"]

        for i, location in enumerate(locations):
            baseline_file = os.path.join(self.data_dir, "contacts", "baseline_" + location + ".txt")
            contact_matrices[i] = mio.ContactMatrix(
                mio.read_mobility_plain(baseline_file), np.zeros((self.num_groups, self.num_groups))
            )
        model.parameters.ContactPatterns.cont_freq_mat = contact_matrices

    def aggregate_results(self, results, county_ids, num_days, num_groups):
        return

    def __call__(self, parameters):
        # TODO
        # self.set_parameters(parameters)
        start = perf_counter()
        result = osecirvvs.simulate(t0=0, tmax=14, dt=0.1, model=self.model)  # .as_ndarray().copy()
        print("sim dur without graph was ", perf_counter() - start, " seconds")
        print("result is\n", result)
        pass
        # start = perf_counter()
        # study = osecirvvs.ParameterStudy(
        #     self.graph, t0=0., tmax=self.num_days_sim, dt=0.1, num_runs=1)
        # print('creating parameter study with 1 run for germany takes', perf_counter()-start)
        # high = False
        # start = perf_counter()
        # ensemble = study.run(high)
        # print(f'one {self.num_days_sim} day simulation for all of germany takes', perf_counter()-start)
        #
        #
        # study = osecirvvs.ParameterStudy(
        #     self.graph, t0=0., tmax=self.num_days_sim, dt=0.1, num_runs=10)
        # print('creating parameter study with 10 runs for germany takes', perf_counter()-start)
        # high = False
        # start = perf_counter()
        # ensemble = study.run(high)
        # print(f'ten {self.num_days_sim} day simulation for all of germany takes', perf_counter()-start)

        timeseries = np.array([0])
        self.aggregate_results(None, None, self.num_days_sim, self.num_groups)
        return timeseries


if __name__ == "__main__":
    mio.set_log_level(mio.LogLevel.Off)
    stat_date = date.fromisoformat("2024-05-22")
    data_dir = "/tmp/testdata"
    model = GraphSecirvvs(start_date=stat_date, data_dir=data_dir)
    model([])
