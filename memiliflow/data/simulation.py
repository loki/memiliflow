from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyross


def simulate_oseir_stochastic_trajectory(
    population,
    num_age_groups,
    beta,
    gE,
    gI,
    initial_exposed,
    initial_infected,
    initial_recovered,
    simulation_duration,
    plot_trajectories=False,
):
    M = num_age_groups  # the population has one age groups
    N = population  # and this is the total population
    Ni = N * np.ones(M)

    # set up initial condition
    E0 = initial_exposed
    I0 = initial_infected
    R0 = initial_recovered
    S0 = N - (E0 + I0 + R0)
    x0 = {"S": [S0], "E": [E0], "I": [I0], "R": [R0]}

    Tf = simulation_duration
    Nf = Tf + 1

    # C_ij = number of people group from group i that an individual from group j meets per day
    C = np.array([[1.0]])

    def contactMatrix(t):
        return C

    true_parameters = {"beta": beta, "gE": gE, "gI": gI}

    model_spec = {
        "classes": ["S", "E", "I", "R"],
        "S": {
            "recovered": [["I", "S", "-beta"]],
        },
        "E": {"linear": [["E", "-gE"]], "recovered": [["I", "S", "beta"]]},
        "I": {
            "linear": [
                ["E", "gE"],
                ["I", "-gI"],
            ],
        },
        "R": {
            "linear": [["I", "gI"]],
        },
    }

    # use pyross stochastic to generate traj and save
    sto_model = pyross.stochastic.Model(model_spec, true_parameters, M, Ni)
    data = sto_model.simulate(x0, contactMatrix, Tf, Nf, method="tau-leaping")
    data_array = data["X"]

    if plot_trajectories:
        fig = plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor="w", edgecolor="k")
        plt.rcParams.update({"font.size": 22})
        t = data["t"]

        plt.fill_between(t, 0, np.sum(data_array[:, :M], axis=1), alpha=0.3)
        plt.plot(t, np.sum(data_array[:, :M], axis=1), "-", label="S", lw=2)

        plt.fill_between(t, 0, np.sum(data_array[:, M : 2 * M], axis=1), alpha=0.3)
        plt.plot(t, np.sum(data_array[:, M : 2 * M], axis=1), "-", label="E", lw=2)

        plt.fill_between(t, 0, np.sum(data_array[:, 2 * M : 3 * M], axis=1), alpha=0.3)
        plt.plot(t, np.sum(data_array[:, 2 * M : 3 * M], axis=1), "-", label="I", lw=2)

        plt.fill_between(t, 0, np.sum(data_array[:, 3 * M : 4 * M], axis=1), alpha=0.3)
        plt.plot(t, np.sum(data_array[:, 3 * M : 4 * M], axis=1), "-", label="R", lw=2)

        plt.legend(fontsize=26)
        plt.grid()
        plt.xlabel(r"time")
        plt.autoscale(enable=True, axis="x", tight=True)

    return np.hstack((data["t"].reshape((data["t"].shape[0], 1)), data["X"])).transpose()


def simulate_detection_delay(
    observations: pd.DataFrame,
    detection_delay,
    detection_delay_std,
    column="Infected",
    max_detection_delay=14,
):
    compartment = observations[column]
    detected_compartment = np.zeros(compartment.shape)
    # Wie viel Tage später wird ein case reported?
    # Mo-So
    if report_matrix is None:
        report_matrix = np.array(
            [  # Wann wird ein Fall vom Tag x reportet?
                [0, 0.3, 0.4, 0.3, 0, 0, 0, 0],  # Montag
                [0, 0.3, 0.4, 0.3, 0, 0, 0, 0],  # Dienstag
                [0, 0.3, 0.4, 0.1, 0.1, 0.1, 0, 0],  # Mittwoch
                [0, 0.3, 0.1, 0.1, 0.4, 0.1, 0, 0],  # Donnerstag
                [0, 0.1, 0.1, 0.4, 0.4, 0.1, 0, 0],  # Freitag
                [0, 0.1, 0.4, 0.3, 0.2, 0, 0, 0],  # Samstag
                [0.0, 0.3, 0.4, 0.3, 0, 0, 0, 0],  # Sonntag
            ]
        )
        report_matrix = np.array(
            [  # Wann wird ein Fall vom Tag x reportet?
                [0, 0.4, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Montag
                [0, 0.4, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Dienstag
                [0, 0.4, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Mittwoch
                [0, 0.4, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Donnerstag
                [0, 0.4, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Freitag
                [0, 0.0, 0.4, 0.2, 0.1, 0.1, 0.1, 0.1],  # Samstag
                [0, 0.4, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Sonntag
                # [0, 0.3, 0.4, 0.3, 0, 0, 0, 0],  # Dienstag
                # [0, 0.3, 0.4, 0.1, 0.1, 0.1, 0, 0],  # Mittwoch
                # [0, 0.3, 0.1, 0.1, 0.4, 0.1, 0, 0],  # Donnerstag
                # [0, 0.1, 0.1, 0.4, 0.4, 0.1, 0, 0],  # Freitag
                # [0, 0.1, 0.4, 0.3, 0.2, 0, 0, 0],  # Samstag
                # [0., 0.3, 0.4, 0.3, 0, 0, 0, 0],  # Sonntag
            ]
        )

    detection_delay_dt = np.arange(max_detection_delay)
    detection_delay_prob = np.exp(
        -((detection_delay_dt - detection_delay) ** 2) / detection_delay_std**2
    )
    detection_delay_prob /= np.sum(detection_delay_prob)
    ## detected_delay describes the (normalized) probability, that a case is detected k
    ## days after infection
    for i in range(compartment.shape[-1]):
        for k in range(detection_delay_dt.size):
            delayed_day = max(i - k, 0)
            # print(delayed_day)
            detected_compartment[i] += compartment[delayed_day] * detection_delay_prob[k]

    observations[column] = detected_compartment
    return observations


# def simulate_reporting_delay(
#         observations: pd.DataFrame,
#         columns: List,
#         holidays=None,
#         max_reporting_delay=10.
# ):
#
#     detection_delay_dt = np.arange(max_detection_delay)
#     detection_delay_prob = np.exp(-(detection_delay_dt - detection_delay) ** 2 / detection_delay_std ** 2)
#     detection_delay_prob /= np.sum(detection_delay_prob)
#     # Wie viel Tage später wird ein case reported?
#     # Mo-So
#     if report_matrix is None:
#         report_matrix = np.array([  # Wann wird ein Fall vom Tag x reportet?
#             [0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.0, 0.2],  # Montag
#             [0, 0.2, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Dienstag
#             [0, 0.2, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Mittwoch
#             [0, 0.2, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Donnerstag
#             [0, 0.2, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Freitag
#             [0, 0.0, 0.1, 0.2, 0.1, 0.1, 0.1, 0.1],  # Samstag
#             [0, 0.2, 0.2, 0.1, 0.1, 0.1, 0.1, 0],  # Sonntag
#         ])
#
#     compartment = observations[index_to_delay, :].flatten()
#     reported_compartment = np.zeros(compartment.shape)  # R
#
#     ## delay reporting
#     for i in range(detected_compartment.shape[-1]):
#         day_of_week = i % 7
#         for k in range(report_matrix.shape[0]):
#             delayed_day = max(i - k, 0)
#             D_prev = detected_compartment[delayed_day]
#             reported_compartment[i] += D_prev * report_matrix[day_of_week - k, k]
#
#     return reported_compartment
