import logging
import time
from datetime import date, timedelta
from enum import Enum
from pathlib import Path
from typing import List, Mapping

import numpy as np
import pandas as pd
from memilio.epidata import (
    getCaseData,
    getDIVIData,
    getPopulationData,
    getVaccinationData,
)

_log = logging.getLogger(__file__)


class CaseData:
    class Label(str, Enum):
        DATE = "Date"
        CONFIRMED = "Confirmed"
        RECOVERED = "Recovered"
        DEATHS = "Deaths"

    def __init__(self, data: pd.DataFrame):
        self.data = data

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.data == other.data).all().all()
        else:
            return False

    def to_oseir_compartments(self, time_exposed, time_infected) -> pd.DataFrame:
        case_data = self.data.copy()

        # round time_exposed for now
        case_data["COMPARTMENT_EXPOSED"] = -case_data[self.Label.CONFIRMED.value].diff(
            periods=-int(time_exposed)
        )
        case_data["COMPARTMENT_INFECTED"] = case_data[self.Label.CONFIRMED.value].diff(
            periods=int(time_infected)
        )
        # Deaths are counted as recovered, because oseir is a test-model without deaths compartment.
        case_data["COMPARTMENT_RECOVERED"] = (
            case_data[self.Label.RECOVERED.value] + case_data[self.Label.DEATHS.value]
        )

        # num_susceptible is not set here, because memilio-models have a function for that

        case_data.drop(
            columns=[
                c
                for c in case_data.columns
                if c
                not in [
                    self.Label.DATE.value,
                    "COMPARTMENT_EXPOSED",
                    "COMPARTMENT_INFECTED",
                    "COMPARTMENT_RECOVERED",
                ]
            ],
            inplace=True,
        )
        return case_data

    def to_pyross_oseir_array(self, total_population, time_exposed, time_infected) -> np.array:
        """This function computes compartments based on case data. It will produce some nans at the beginning and end
        of array because some compartments can not be estimated"""
        dataframe = self.to_oseir_compartments(time_exposed, time_infected)
        print(dataframe.columns)
        dataframe["COMPARTMENT_SUSCEPTIBLE"] = (
            total_population
            - dataframe["COMPARTMENT_EXPOSED"]
            - dataframe["COMPARTMENT_INFECTED"]
            - dataframe["COMPARTMENT_RECOVERED"]
        )
        dataframe = dataframe[
            [
                "COMPARTMENT_SUSCEPTIBLE",
                "COMPARTMENT_EXPOSED",
                "COMPARTMENT_INFECTED",
                "COMPARTMENT_RECOVERED",
            ]
        ]
        return dataframe.to_numpy()

    @staticmethod
    def read_json_from_dir(data_dir: Path, keys: List[str] = None) -> "CaseData":
        case_data = {}
        if keys is None:
            _keys = [file for file in Path.iterdir(data_dir) if file.suffix == ".json"]
        else:
            _keys = [data_dir / Path(key + ".json") for key in keys]

        for key in _keys:
            case_data[Path(key).stem] = CaseData(pd.read_json(key))

        return case_data

    @staticmethod
    def download_to_filesystem(
        data_dir,
        start_date: date,
        end_date: date,
        moving_average: int = 7,
        file_format: str = "json_timeasstring",
        raw_data: bool = False,
        files=["all_germany"],
    ):
        """
        Downloads case data using memilio python bindings
        :param data_dir:
        :param moving_average:
        :param start_date:
        :param end_date:
        :param file_format: see memilio-epidata
        :return:CaseData
        """
        if file_format != "json" and file_format != "json_timeasstring" and file_format != "hdf5":
            raise ValueError(f"'{file_format}' is not supported as file_format.")

        data_dir = Path(data_dir)
        if not data_dir.exists():
            raise FileNotFoundError(f"Data_dir {data_dir} does not exist.")

        _log.info("starting download")
        start = time.time()

        arg_dict_all = {
            "file_format": file_format,
            "out_folder": data_dir,
            "no_raw": (not raw_data),
        }
        _log.info(f"moving average is {moving_average}")
        arg_dict_data_download = {
            "start_date": start_date,
            "end_date": end_date,
            "moving_average": moving_average,
            "files": files,
        }
        arg_dict_cases = {**arg_dict_all, **arg_dict_data_download}

        getCaseData.get_case_data(**arg_dict_cases)
        getPopulationData.get_population_data(**arg_dict_all)
        getDIVIData.get_divi_data(**arg_dict_cases)
        getVaccinationData.get_vaccination_data(**arg_dict_cases)

        end = time.time()
        _log.info("finished download after %.1f seconds", end - start)
