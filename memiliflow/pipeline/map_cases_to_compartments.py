import logging
import shutil
import tempfile
from pathlib import Path

import boto3
import click
from memiliflow.data.case_data import CaseData
from s3_label import S3Label

_log = logging.getLogger(__file__)


@click.command()
@click.option("--s3_access_key_id", default="axEhL4FmWR0U54rV", help="Acess key to S3")
@click.option(
    "--s3_access_key_secret", default="ahzh10eFY0P90vIsVbpba6HzK7PPEka9", help="Access secret to S3"
)
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="airflow-memiliflow-test", help="S3 bucket to upload to")
@click.option("--country", default="Germany", required=True, type=str)
@click.option(
    "--case_data_key",
    help="examples: cases_all_germany_ma7 or cases_all_germany. ma7 stands for moving average with 7 days.",
    default="cases_all_germany",
    required=True,
    type=str,
)
def map_cases_to_compartments(
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    country,
    case_data_key,
):
    session = boto3.session.Session()
    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=s3_access_key_id,
        aws_secret_access_key=s3_access_key_secret,
        endpoint_url=s3_endpoint,
    )
    with tempfile.TemporaryDirectory() as tmp_dir:
        rki_data_path = Path(tmp_dir) / Path(S3Label.RKI_DATA.value)
        with open(rki_data_path, "wb") as f:
            s3_client.download_fileobj(bucket, S3Label.RKI_DATA.value, f)
        shutil.unpack_archive(rki_data_path, extract_dir=tmp_dir)

        case_data = CaseData.read_json_from_dir(data_dir=tmp_dir, keys=[case_data_key])[
            case_data_key
        ]
        compartments = case_data.to_oseir_compartments(time_infected=6, time_exposed=5.2)
        compartments.to_json(Path(tmp_dir) / Path(S3Label.COMPARTMENT_DATA.value))
        s3_client.upload_file(
            str(Path(tmp_dir) / Path(S3Label.COMPARTMENT_DATA.value)),
            bucket,
            S3Label.COMPARTMENT_DATA.value,
        )


if __name__ == "__main__":
    map_cases_to_compartments()
