# import logging
# import tempfile
# import time
# from pathlib import Path
#
# import arviz as az
# import boto3
# import click
# import numpy as np
# import pandas as pd
# from memiliflow.data.case_data import CaseData
# from memiliflow.inference.oseir_inference import pymc_infer_oseir
# from memiliflow.likelihood.oseir_likelihood import make_memilio_oseir_likelihood
# from memiliflow.memilio.oseir import Oseir
# from memiliflow.memilio.oseir_builder import OseirBuilder
# from s3_label import S3Label
#
# _log = logging.getLogger(__file__)
#
#
# @click.command()
# @click.option("--fetch_data_from_s3", default=True, help="whether to fetch case_data from S3")
# @click.option("--s3_access_key_id", default="axEhL4FmWR0U54rV", help="Acess key to S3")
# @click.option(
#     "--s3_access_key_secret", default="ahzh10eFY0P90vIsVbpba6HzK7PPEka9", help="Acess secret to S3"
# )
# @click.option(
#     "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
# )
# @click.option("--bucket", default="airflow-memiliflow-test", help="S3 bucket to uplaod to")
# @click.option("--steps", default=1000, help="number of mcmc steps for each chain")
# @click.option("--tune", default=1000, help="number of mcmc tuning steps")
# @click.option("--chains", default=4, help="number of mcmc chains to simulate")
# @click.option("--cores", default=4, help="number of cores")
# # TODO FJ add options for sampling, like which model to use (infer)
# def pymc_inference(
#     fetch_data_from_s3,
#     s3_access_key_id,
#     s3_access_key_secret,
#     s3_endpoint,
#     bucket,
#     steps,
#     tune,
#     cores,
#     chains,
# ):
#     session = boto3.session.Session()
#     s3_client = session.client(
#         service_name="s3",
#         aws_access_key_id=s3_access_key_id,
#         aws_secret_access_key=s3_access_key_secret,
#         endpoint_url=s3_endpoint,
#     )
#
#     log = tempfile.NamedTemporaryFile()
#     # Open the file for writing.
#     with open(log.name, "w") as f:
#         f.write(str("starting mcmc\n"))
#     s3_client.upload_file(log.name, bucket, "log_pymc_inference.txt")
#
#     with tempfile.TemporaryDirectory() as tmp_dir:
#         compartment_data_file = str(S3Label.COMPARTMENT_DATA.value)
#         compartment_data_path = str(Path(tmp_dir) / Path(compartment_data_file))
#         if fetch_data_from_s3:
#             with open(compartment_data_path, "wb") as f:
#                 s3_client.download_fileobj(bucket, compartment_data_file, f)
#         compartment_data = pd.read_json(compartment_data_path).dropna()
#
#     with open(log.name, "a") as f:
#         f.write(str("fetched data\n"))
#     s3_client.upload_file(log.name, bucket, "log_pymc_inference.txt")
#
#     # assumes we have one data point for each day! Will break if this is not the case
#     # TODO FJ: map_cases should also store simulation length
#     simulation_duration = len(compartment_data.index)
#
#     # TODO FJ: replace next block fetch prior data from S3
#     # Setting log level might be broken, see https://github.com/DLR-SC/memilio/issues/335
#
#     # TODO FJ: fetch priors from S3, see https://codebase.helmholtz.cloud/loki/memiliflow/-/issues/31
#     # TODO FJ use switch cases here to select model and inference framework.
#     # FOr now default to pymc and memilio
#     true_time_exposed = 5.2
#     true_time_infected = 6.0
#     true_transmission = 0.95
#     priors = {
#         Oseir.Parameter.TIME_EXPOSED: {
#             "mean": np.array(true_time_exposed),
#             "sigma": np.array(2.5),
#             "lower": np.array(2.0),
#             "upper": np.array(14),
#         },
#         Oseir.Parameter.TIME_INFECTED: {
#             "mean": np.array(true_time_infected),
#             "sigma": np.array(2.5),
#             "lower": np.array(2.0),
#             "upper": np.array(14),
#         },
#         Oseir.Parameter.TRANSMISSION_PROBABILITY: {
#             "mean": np.array(true_transmission),
#             "sigma": np.array(0.5),
#             "lower": np.array(0.5),
#             "upper": np.array(0.999),
#         },
#     }
#
#     #  inference expects certain shape
#     observations = compartment_data.values.transpose()
#
#     oseir_builder = OseirBuilder(
#         time_exposed=true_time_exposed,
#         time_infected=true_time_infected,
#         transmission=true_transmission,
#     )
#
#     model = Oseir(
#         oseir_builder,
#         default_t0=0,
#         default_simulation_step=0.1,
#         default_simulation_duration=simulation_duration,
#     )
#     model.set_parameter(Oseir.Parameter.CONTACT_MATRIX, np.array([[1]]))
#     model.set_parameter(Oseir.Parameter.TIME_EXPOSED, true_time_exposed)
#     model.set_parameter(Oseir.Parameter.TIME_INFECTED, true_time_infected)
#     model.set_parameter(Oseir.Parameter.TRANSMISSION_PROBABILITY, true_transmission)
#     likelihood_op = make_memilio_oseir_likelihood(model, observations=observations)
#     print("obs")
#     print(observations)
#     print("test-likelihood")
#     print(likelihood_op(np.array([8, 8, 0.74])))
#     test_val = likelihood_op(np.array([true_time_exposed, true_time_infected, true_transmission]))
#     with open(log.name, "a") as f:
#         f.write(str("test likelihood:") + str(test_val))
#     s3_client.upload_file(log.name, bucket, "log_pymc_inference.txt")
#
#     start = time.time()
#     np.seterr(over="ignore")
#     trace = pymc_infer_oseir(
#         likelihood_function=likelihood_op,
#         priors=priors,
#         chains=int(chains),
#         cores=int(cores),
#         steps=int(steps),
#         tune=int(tune),
#     )
#     end = time.time()
#     print("inference time:", end - start, " seconds")
#     print("trace")
#     print(trace)
#     summary = az.summary(trace)
#     print(summary)
#     with tempfile.TemporaryDirectory() as tmp_dir:
#         # uploads trace
#         trace_path = str(Path(tmp_dir) / Path("trace.csv"))
#         trace.to_dataframe().to_csv(trace_path)
#         s3_client.upload_file(trace_path, bucket, Path(trace_path).name)
#
#         # uploads trace summary
#         summary_path = str(Path(tmp_dir) / Path("mcmc_summary.csv"))
#         summary.to_csv(summary_path)
#         s3_client.upload_file(summary_path, bucket, Path(summary_path).name)
#
#         # uploads measured runtime
#         mcmc_time_path = str(Path(tmp_dir) / Path("mcmc_time.txt"))
#         with open(mcmc_time_path, "w") as f:
#             f.write(str(end - start) + "seconds")
#         s3_client.upload_file(mcmc_time_path, bucket, Path(mcmc_time_path).name)
#
#     pass
#
#
# if __name__ == "__main__":
#     pymc_inference()
