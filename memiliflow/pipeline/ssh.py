import logging
import subprocess
import tempfile

_log = logging.getLogger(__file__)


def run_ssh_command(cmd, user, host, private_key, logger=None):
    subprocess.run(
        f'ssh -i {private_key} -o IdentitiesOnly=yes -o ServerAliveInterval=60 -o StrictHostKeyChecking=accept-new {user}@{host} "{cmd}"',
        shell=True,
        capture_output=True,
        check=True,
    )


def run_scp_command(file, target_dir, user, host, private_key, logger=None):
    subprocess.run(
        f"scp -i {private_key} -o IdentitiesOnly=yes -o StrictHostKeyChecking=accept-new {file} {user}@{host}:{target_dir}",
        shell=True,
        check=True,
        capture_output=True,
    )
