import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)
import logging
import os
import tempfile
import time
from pathlib import Path

import boto3
import click
import s3
from memilio.epidata import getPopulationData as gpd
from memilio.epidata import progress_indicator

_log = logging.getLogger(__file__)


@click.command()
@click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option(
    "--regionalstatistik_username",
    help="username for downloading population data from regionalstatistik.de",
    required=True,
    type=str,
)
@click.option(
    "--regionalstatistik_password",
    help="password for downloading population data from regionalstatistik.de",
    required=True,
    type=str,
)
@click.option(
    "--tmpdir",
    default=None,
    help="where to place tmpdir. If none /dependencies is used per default",
)
def ESID_get_population_data(
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    regionalstatistik_username,
    regionalstatistik_password,
    tmpdir,
):
    logging.basicConfig()
    _log.setLevel(logging.INFO)

    session = boto3.session.Session()
    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=s3_access_key_id,
        aws_secret_access_key=s3_access_key_secret,
        endpoint_url=s3_endpoint,
    )

    # _log.info(f"start download with start_date {start_date}")
    start = time.time()
    # download data until yesterday
    _log.info(f"start pop-data download")
    with tempfile.TemporaryDirectory(dir="/dependencies" if tmpdir is None else tmpdir) as tmp_dir:
        _log.info(f"downloading pop data to {tmp_dir}/Germany")
        os.mkdir(Path(tmp_dir) / Path("Germany"))
        arg_dict_pop = {
            "out_folder": "{}".format(tmp_dir),
            "username": regionalstatistik_username,
            "password": regionalstatistik_password,
        }
        progress_indicator.ProgressIndicator.disable_indicators(True)
        gpd.get_population_data(**arg_dict_pop)
        end = time.time()
        download_time = end - start
        _log.info("finished download after %.1f seconds", download_time)

        s3_client.upload_file(
            str(Path(tmp_dir) / Path("Germany") / Path("county_current_population.json")),
            bucket,
            "cached/county_current_population.json",
        )
        end = time.time()
        upload_time = end - start
        _log.info("uploaded pop data after %.1f seconds", upload_time)
        _log.info("done")
    _log.info("finished download after %.1f seconds", download_time)


if __name__ == "__main__":
    ESID_get_population_data()
