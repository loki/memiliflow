import logging
import warnings
from datetime import date

import click
import requests

warnings.simplefilter(action="ignore", category=FutureWarning)
_log = logging.getLogger(__file__)


@click.command()
@click.option("-a", "api-root-endpoint", required=True, type=str, help="URL to the APi endpoint")
@click.option(
    "-o",
    "--out-folder",
    required=True,
    default="temp_out_folder",
    help="Name of the folder where output is written temporarily",
    type="str",
)
@click.option(
    "-s",
    "--db-schema",
    required=True,
    help="Name of the database schema under which tables should be persisted",
    type="str",
)
@click.option("--moving-avg", help="The moving average window", required=False, type=int)
@click.option("--start-date", help="Date of first data point in dataframe", required=False)
@click.option("--end-date", help="Date of last data point in dataframe", required=False)
@click.option(
    "-u", "api-user-name", required=True, type=str, help="Your username to authenticate against API"
)
@click.option(
    "-p",
    "api-user-password",
    required=True,
    type=str,
    help="Your password to authenticate against API",
)
def trigger_epidata_upload_to_lokidb(
    api_root_endpoint: str,
    api_user_name: str,
    api_user_password: str,
    out_folder: str,
    db_schema: str,
    moving_avg: int | None,
    start_date: date | None,
    end_date: date | None,
) -> None:

    logging.basicConfig()
    _log.setLevel(logging.INFO)

    payload = {
        "out_folder": out_folder,
        "db_schema": db_schema,
        "moving_avg": moving_avg,
        "start_date": start_date,
        "end_date": end_date,
    }

    r = requests.post(
        f"{api_root_endpoint}/token",
        data={"username": api_user_name, "password": api_user_password},
        headers={"content-type": "application/x-www-form-urlencoded"},
    )

    token = r.json()["access_token"]

    res = requests.post(
        f"{api_root_endpoint}/upload_public_epidata_to_DB",
        headers={"content-type": "application/json", "Authorization": f"Bearer {token}"},
        params=payload,
    )

    _log.info(f"The request response status code: {res.status_code}")


if __name__ == "__main__":
    trigger_epidata_upload_to_lokidb()
