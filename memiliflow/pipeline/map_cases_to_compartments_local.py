import logging
from pathlib import Path

import click
from memiliflow.data.case_data import CaseData

_log = logging.getLogger(__file__)


@click.command()
@click.option(
    "--input_dir",
    help="where to read case data from",
    required=True,
    type=str,
)
@click.option(
    "--output_dir",
    help="directory where mapped data will be written to",
    required=True,
    type=str,
)
@click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
)
@click.option("--country", default="Germany", required=True, type=str)
@click.option(
    "--case_data_key",
    help="examples: cases_all_germany_ma7 or cases_all_germany. ma7 stands for moving average with 7 days.",
    default="cases_all_germany_ma7",
    required=True,
    type=str,
)
def map_cases_to_compartments(input_dir, output_dir, execution_date, country, case_data_key):
    input_dir = Path(input_dir)
    input_dir = input_dir / Path(execution_date) / Path(country)
    if not input_dir.exists():
        raise FileNotFoundError(f"Data_dir {input_dir} does not exist.")

    case_data = CaseData.read_json_from_dir(data_dir=input_dir, keys=[case_data_key])[case_data_key]
    compartments = case_data.to_oseir_compartments(time_infected=6, time_exposed=5)
    compartments.to_json(Path(output_dir) / Path("mapped_compartments.json"))


if __name__ == "__main__":
    map_cases_to_compartments()
