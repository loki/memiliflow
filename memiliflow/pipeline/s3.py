import os

import boto3


def create_s3_client(s3_access_key_id, s3_access_key_secret, s3_endpoint):
    session = boto3.session.Session()

    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=s3_access_key_id,
        aws_secret_access_key=s3_access_key_secret,
        endpoint_url=s3_endpoint,
    )

    return s3_client


def download(s3_client, bucket, remote_name, local_name, logger=None, mode=None):
    f"""Downloading {remote_name} from {bucket} to a file called {local_name}"""
    with open(local_name, "wb") as f:
        if logger:
            logger.info(f"downloading {remote_name} from {bucket} to {local_name}")
        s3_client.download_fileobj(bucket, remote_name, f)
    if mode is not None:
        os.chmod(local_name, mode)
