import logging
import tempfile
from pathlib import Path

import boto3
import click
from memiliflow.data.case_data import CaseData
from memiliflow.data.simulation import simulate_oseir_stochastic_trajectory
from s3_label import S3Label

_log = logging.getLogger(__file__)


@click.command()
@click.option("--s3_access_key_id", default="axEhL4FmWR0U54rV", help="Acess key to S3")
@click.option(
    "--s3_access_key_secret", default="ahzh10eFY0P90vIsVbpba6HzK7PPEka9", help="Access secret to S3"
)
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="airflow-memiliflow-test", help="S3 bucket to upload to")
@click.option("--population", default=1e5)
@click.option("--num_age_groups", default=1)
@click.option("--beta", default=0.95)
@click.option("--time_exposed", default=5.2)
@click.option("--time_infected", default=6.0)
@click.option("--initial_exposed", default=5e4)
@click.option("--initial_infected", default=1e4)
@click.option("--initial_recovered", default=1e1)
@click.option("--simulation_duration", default=8)
def simulate_data(
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    population,
    num_age_groups,
    beta,
    time_exposed,
    time_infected,
    initial_exposed,
    initial_infected,
    initial_recovered,
    simulation_duration,
):
    session = boto3.session.Session()
    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=s3_access_key_id,
        aws_secret_access_key=s3_access_key_secret,
        endpoint_url=s3_endpoint,
    )
    log = tempfile.NamedTemporaryFile()
    # Open the file for writing.
    with open(log.name, "w") as f:
        f.write(str("simulate_compartments has s3 connection"))
    s3_client.upload_file(log.name, bucket, "log_simulate_compartments.txt")
    try:
        # TODO FJ also store random seed used for stochastic simulation
        simulated_data = simulate_oseir_stochastic_trajectory(
            population=population,
            num_age_groups=num_age_groups,
            beta=beta,
            gE=1.0 / time_exposed,
            gI=1.0 / time_infected,
            initial_exposed=initial_exposed,
            initial_infected=initial_infected,
            initial_recovered=initial_recovered,
            simulation_duration=simulation_duration,
        )

        simulated_dataframe = CaseData.numpy_to_oseir_compartments(simulated_data)

        with tempfile.TemporaryDirectory() as tmp_dir:
            simulated_data_path = str(Path(tmp_dir) / Path(S3Label.COMPARTMENT_DATA.value))
            simulated_dataframe.to_json(simulated_data_path)

            s3_client.upload_file(simulated_data_path, bucket, str(Path(simulated_data_path).name))
    except Exception as e:
        tmp = tempfile.NamedTemporaryFile()
        # Open the file for writing.
        with open(tmp.name, "w") as f:
            f.write(str(e))
        s3_client.upload_file(tmp.name, bucket, "exception_sim")


if __name__ == "__main__":
    simulate_data()
