import logging
import os

import click
import s3

_log = logging.getLogger(__file__)


@click.command()
@click.option("--file", required=True, help="file to upload to S3")
@click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option("--execution_date", help="current date. E.g. 2020-07-15", required=True, type=str)
def upload(file, s3_access_key_id, s3_access_key_secret, s3_endpoint, bucket, execution_date):
    logging.basicConfig()
    _log.setLevel(logging.INFO)

    _log.info(f"uploading {file}")
    if os.path.isfile(file):
        s3_client = s3.create_s3_client(s3_access_key_id, s3_access_key_secret, s3_endpoint)
        s3_client.upload_file(file, bucket, file)
    else:
        _log.error("file does not exist")
        raise RuntimeError(f"Can not upload {file}. It does not exist.")


if __name__ == "__main__":
    upload()
