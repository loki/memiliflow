import os
import sys

sys.path.append(os.getcwd())

import logging
import os
import tempfile
import warnings
from pathlib import Path

import click
import s3
import ssh

warnings.simplefilter(action="ignore", category=FutureWarning)


_log = logging.getLogger(__file__)


@click.command()
@click.option(
    "--scenario_name",
    required=True,
    default="baseline",
    type=str,
    help="Name of the scenario to compute",
)
@click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
)
@click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option("--host", required=True, help="ESID host")
@click.option("--user", required=True, help="user to use for upload on ESID host")
@click.option("--ssh_key", required=True, help="ssh_key to ssh into ESID host")
@click.option(
    "--esid_path", default="/home/temp/ESID/", help="path to ESID (where docker-compose.yaml is)."
)
@click.option(
    "--django_path", default="/mnt/docker-volumes/django-data/", help="path to django-data."
)
@click.option(
    "--tmpdir",
    default=None,
    help="where to place tmpdir. If none /dependencies is used per default",
)
def upload_simulation_to_ESID(
    scenario_name,
    execution_date,
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    host,
    user,
    ssh_key,
    esid_path,
    django_path,
    tmpdir,
):
    s3_client = s3.create_s3_client(s3_access_key_id, s3_access_key_secret, s3_endpoint)
    logging.basicConfig()
    _log.setLevel(logging.INFO)
    with tempfile.TemporaryDirectory(dir="/dependencies" if tmpdir is None else tmpdir) as tmp_dir:
        _log.info(f"writing temporary data to {tmp_dir}")
        local_key_file = Path(tmp_dir) / Path("ssh_key_file")
        with open(local_key_file, "w+") as fh:
            fh.write(ssh_key)
        os.chmod(local_key_file, 0o600)

        remote_file = f"computed/simulation_{scenario_name}@".replace(
            "@", "_" + str(execution_date) + ".zip"
        )
        local_file = f"{scenario_name}_simulation.zip"
        target = f"{django_path}/{local_file}"

        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name=remote_file,
            local_name=os.path.join(tmp_dir, local_file),
            logger=_log,
        )

        # file to upload needs to be in the same dir as the docker-compose, therefore it is copied over
        ssh.run_scp_command(
            file=os.path.join(tmp_dir, local_file),
            target_dir=target,
            user=user,
            host=host,
            private_key=local_key_file,
            logger=_log,
        )

        cmds = [
            # upload
            f"cd {esid_path} && USER_ID={user} GROUP_ID={user} docker-compose -f docker-compose.yaml run --rm backend python manage.py import_simulation /localdata/{local_file} --action 1",
        ]

        for cmd in cmds:
            _log.info(f"running: {cmd}")
            ssh.run_ssh_command(
                cmd=cmd, user=user, host=host, private_key=local_key_file, logger=_log
            )
        _log.info("done")


if __name__ == "__main__":
    upload_simulation_to_ESID()
