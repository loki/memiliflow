import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)
import logging
import os
import shutil
import tempfile
import time
from datetime import date, timedelta
from pathlib import Path

import boto3
import click
import s3
from memilio.epidata import getVaccinationData as gvd
from memilio.epidata import progress_indicator

_log = logging.getLogger(__file__)


@click.command()
@click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option("--execution_date", help="current date. E.g. 2020-07-15", required=True, type=str)
@click.option(
    "--tmpdir",
    default=None,
    help="where to place tmpdir. If none /dependencies is used per default",
)
def ESID_get_vaccination_data(
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    execution_date,
    bucket,
    tmpdir,
):
    logging.basicConfig()
    _log.setLevel(logging.INFO)

    session = boto3.session.Session()
    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=s3_access_key_id,
        aws_secret_access_key=s3_access_key_secret,
        endpoint_url=s3_endpoint,
    )

    start = time.time()
    # download data until yesterday
    end_date = date.today() - timedelta(days=1)
    _log.info(f"start vaccination data download with end_date {end_date}")
    with tempfile.TemporaryDirectory(dir="/dependencies" if tmpdir is None else tmpdir) as tmp_dir:
        arg_dict = {
            "out_folder": "{}/pydata".format(tmp_dir),
            "end_date": end_date,
            "moving_average": 7,
        }
        progress_indicator.ProgressIndicator.disable_indicators(False)
        os.makedirs(os.path.join(arg_dict["out_folder"], "Germany"))
        _log.info(f"downloading pop data to {tmp_dir}")
        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name="cached/county_current_population.json",
            local_name=os.path.join(
                arg_dict["out_folder"], "Germany", "county_current_population.json"
            ),
            logger=_log,
        )
        _log.info(f"downloading vaccination data to {tmp_dir}")
        gvd.get_vaccination_data(**arg_dict)
        end = time.time()
        download_time = end - start
        _log.info("finished download after %.1f seconds", download_time)

        shutil.make_archive(
            str(Path(tmp_dir) / Path("pydata")), "gztar", root_dir=tmp_dir, base_dir="pydata"
        )
        s3_client.upload_file(
            str(Path(tmp_dir) / Path("pydata.tar.gz")),
            bucket,
            "daily-data/vaccination_data_" + str(execution_date + ".tar.gz"),
        )

        end = time.time()
        upload_time = end - start
        _log.info("uploaded vaccination data after %.1f seconds", upload_time)
        _log.info("done")
    _log.info("finished download after %.1f seconds", download_time)


if __name__ == "__main__":
    ESID_get_vaccination_data()
