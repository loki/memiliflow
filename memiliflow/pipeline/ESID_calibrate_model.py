import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)
import logging
import shutil
import tempfile
from datetime import date
from pathlib import Path

import click
import s3

_log = logging.getLogger(__file__)


@click.command()
@click.option(
    "--start_date", help="start date for which case_data should be fetched", required=True, type=str
)
@ click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
) @ click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option("--num_days_sim", default=14, help="Number of days to simulate ")
@click.option(
    "--num_samples",
    default=1_000,
    help="Number of simulation runs (parameters are sampled from valid ranges)",
)
@click.option(
    "--tmpdir",
    default=None,
    help="where to place tmpdir. If none /dependencies is used per default",
)
def ESID_calibrate_model(
    start_date,
    execution_date,
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    num_days_sim,
    num_samples,
    tmpdir,
):
    logging.basicConfig()
    _log.setLevel(logging.INFO)
    _log.info(
        f"Called {__file__} with arguments --start_date {start_date} --execution_date {execution_date} --bucket {bucket} --num_days_sim {num_days_sim}"
    )
    s3_client = s3.create_s3_client(s3_access_key_id, s3_access_key_secret, s3_endpoint)
    with tempfile.TemporaryDirectory(dir="/dependencies" if tmpdir is None else tmpdir) as tmp_dir:
        _log.info(f"writing sim data to {tmp_dir}")

        extrapolated_real_data_path = Path(tmp_dir) / Path("data.zip")
        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name=f"computed/ESID_input_{execution_date}.zip",
            local_name=str(extrapolated_real_data_path),
            logger=_log,
        )
        shutil.unpack_archive(str(extrapolated_real_data_path), extract_dir=tmp_dir)

        _log.info(f"Calibrating model using data in {tmp_dir}")
        # load data

        # memiliflow.calibrate_model(tmp_dir)


if __name__ == "__main__":
    ESID_calibrate_model()
