import json
import os
import sys
import warnings

import numpy as np

sys.path.append(os.getcwd())

warnings.simplefilter(action="ignore", category=FutureWarning)
import logging
import shutil
import tempfile
from datetime import date

# from memilio_src.automatic import (
#     compute_compartments_from_input_data,
#     prepare_case_data_for_backend,
# )
from enum import Enum
from pathlib import Path

import click
import s3
from config import SCENARIO_DEFINITIONS

_log = logging.getLogger(__file__)


class ContactLocations(Enum):
    HOME = "home"
    SCHOOL = "school_pf_eig"
    WORK = "work"
    OTHER = "other"


contact_scalings = {
    "baseline": {
        ContactLocations.HOME: 1.0,
        ContactLocations.SCHOOL: 1.0,
        ContactLocations.WORK: 1.0,
        ContactLocations.OTHER: 1.0,
    },
    "10p_reduced_contacts": {
        ContactLocations.HOME: 0.9,
        ContactLocations.SCHOOL: 0.9,
        ContactLocations.WORK: 0.9,
        ContactLocations.OTHER: 0.9,
    },
    "closed_schools": {
        ContactLocations.HOME: 1,
        ContactLocations.SCHOOL: 0,
        ContactLocations.WORK: 1,
        ContactLocations.OTHER: 1,
    },
    "remote_work": {
        ContactLocations.HOME: 1,
        ContactLocations.SCHOOL: 1,
        ContactLocations.WORK: 0.5,
        ContactLocations.OTHER: 1,
    },
}


def get_remote_and_local_filename(file_name, tmp_dir):
    """
    returns:
        remote name: the file in s3 that is pulled
        local name: where the file is stored locally
    """
    remote_name = f"cached/{file_name}"
    local_name = str(Path(tmp_dir) / Path(file_name))
    return remote_name, local_name


def create_scenario(
    execution_date, scenario_name, contact_scalings, s3_client, bucket, tmp_dir, logger=None
):
    set_mobility(
        execution_date=execution_date,
        scenario_name=scenario_name,
        s3_client=s3_client,
        bucket=bucket,
        tmp_dir=tmp_dir,
        logger=logger,
    )
    effective_contact_scaling = set_contacts(
        execution_date=execution_date,
        scenario_name=scenario_name,
        contact_scalings=contact_scalings,
        s3_client=s3_client,
        bucket=bucket,
        tmp_dir=tmp_dir,
        logger=logger,
    )
    set_scenario_definition(
        execution_date=execution_date,
        contact_scaling=effective_contact_scaling,
        scenario_name=scenario_name,
        s3_client=s3_client,
        bucket=bucket,
        tmp_dir=tmp_dir,
        logger=logger,
    )


def set_mobility(execution_date, scenario_name, s3_client, bucket, tmp_dir, logger):
    """
    For now we do not change mobility. However, if we at any point want to add changes in mobility to scenarios,
    this is the place to do so.
    """
    remote_name, local_name = get_remote_and_local_filename("mobility.zip", tmp_dir=tmp_dir)
    s3.download(
        s3_client=s3_client,
        bucket=bucket,
        remote_name=remote_name,
        local_name=local_name,
        logger=logger,
    )
    s3_client.upload_file(
        local_name, bucket, f"{SCENARIO_DEFINITIONS}/mobility_{scenario_name}_{execution_date}.zip"
    )


def set_scenario_definition(
    execution_date, contact_scaling, scenario_name, s3_client, bucket, tmp_dir, logger
):
    remote_name, local_name = get_remote_and_local_filename("baseline.json", tmp_dir=tmp_dir)
    s3.download(
        s3_client=s3_client,
        bucket=bucket,
        remote_name=remote_name,
        local_name=local_name,
        logger=logger,
    )

    with open(local_name, "r") as jsonFile:
        scenario_definition = json.load(jsonFile)
    scenario_definition["key"] = scenario_name
    scenario_definition["name"] = scenario_name
    scenario_definition["description"] = scenario_name
    scenario_definition["parameters"]["ContactScaling"][0]["value"] = [
        contact_scaling,
        contact_scaling,
    ]
    with open(local_name, "w") as jsonFile:
        json.dump(scenario_definition, jsonFile, indent=4)

    s3_client.upload_file(
        local_name,
        bucket,
        f"{SCENARIO_DEFINITIONS}/scenario_{scenario_name}_{str(execution_date)}.json",
    )


def set_contacts(
    execution_date, scenario_name, contact_scalings, s3_client, bucket, tmp_dir, logger
):
    remote_name, local_name = get_remote_and_local_filename("contacts.zip", tmp_dir=tmp_dir)
    s3.download(
        s3_client=s3_client,
        bucket=bucket,
        remote_name=remote_name,
        local_name=local_name,
        logger=logger,
    )
    _log.info(f"Unpacking contact data for {scenario_name}")
    shutil.unpack_archive(local_name, extract_dir=tmp_dir)

    for location in ContactLocations:
        contact_matrix_file = str(
            Path(tmp_dir) / Path("contacts") / Path(f"baseline_{location.value}.txt")
        )
        contacts = contact_scalings[location] * np.genfromtxt(contact_matrix_file)
        np.savetxt(contact_matrix_file, contacts, delimiter=" ")

    s3_client.upload_file(
        local_name, bucket, f"{SCENARIO_DEFINITIONS}/contacts_{scenario_name}_{execution_date}.zip"
    )

    effective_contact_scaling = np.mean(list(contact_scalings.values()))
    return effective_contact_scaling


@click.command()
@click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
)
@click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option(
    "--tmpdir",
    default=None,
    help="where to place tmpdir. If none /dependencies is used per default",
)
def ESID_define_scenarios(
    execution_date,
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    tmpdir,
):
    """
    Only parallelizable when each running instance has a different tmpdir
    """
    logging.basicConfig()
    _log.setLevel(logging.INFO)
    _log.info(
        f"Called {__file__} with arguments --execution_date {execution_date} --bucket {bucket}"
    )
    s3_client = s3.create_s3_client(s3_access_key_id, s3_access_key_secret, s3_endpoint)
    with tempfile.TemporaryDirectory(dir="/dependencies" if tmpdir is None else tmpdir) as tmp_dir:
        _log.info(f"writing temporary data to {tmp_dir}")

        for case, scalings in contact_scalings.items():
            scenario_name = case
            create_scenario(
                execution_date=execution_date,
                scenario_name=scenario_name,
                contact_scalings=contact_scalings[case],
                s3_client=s3_client,
                bucket=bucket,
                tmp_dir=tmp_dir,
                logger=_log,
            )


if __name__ == "__main__":
    ESID_define_scenarios()
