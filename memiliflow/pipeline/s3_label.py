from enum import Enum


class S3Label(str, Enum):
    RKI_DATA = "rki_case_data.zip"
    COMPARTMENT_DATA = "compartment_data.json"
