import logging
import os
import shutil
import tempfile
import time
from datetime import date
from pathlib import Path

import boto3
import click
from memiliflow.data.case_data import CaseData
from s3_label import S3Label

_log = logging.getLogger(__file__)


@click.command()
@click.option("--moving_average", default=7)
@click.option("--s3_access_key_id", default="axEhL4FmWR0U54rV", help="Acess key to S3")
@click.option(
    "--s3_access_key_secret", default="ahzh10eFY0P90vIsVbpba6HzK7PPEka9", help="Access secret to S3"
)
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="airflow-memiliflow-test", help="S3 bucket to upload to")
@click.option(
    "--start_date", help="start date for which case_data should be fetched", required=True, type=str
)
@click.option(
    "--end_date", help="end date for which case_data should be fetched", required=True, type=str
)
@click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
)
@click.option(
    "--file_format",
    default="json_timeasstring",
    required=True,
    type=str,
)
@click.option(
    "--country",
    default="Germany",
    required=True,
    type=str,
)
@click.option(
    "--raw_data",
    default=False,
    help="whether to fetch the raw data (a couple of GB aswell)",
    type=bool,
)
@click.option(
    "--files",
    default="All",
    help="With files to get",
    type=str,
)
def download(
    moving_average,
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    start_date,
    end_date,
    execution_date,
    file_format,
    country,
    raw_data,
    files,
):
    logging.basicConfig()
    _log.setLevel(logging.INFO)

    session = boto3.session.Session()
    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=s3_access_key_id,
        aws_secret_access_key=s3_access_key_secret,
        endpoint_url=s3_endpoint,
    )

    _log.info("start download")
    start = time.time()
    with tempfile.TemporaryDirectory() as tmp_dir:
        CaseData.download_to_filesystem(
            data_dir=tmp_dir,
            moving_average=moving_average,
            start_date=date.fromisoformat(start_date),
            end_date=date.fromisoformat(end_date),
            file_format=file_format,
            raw_data=raw_data,
            files=files,
        )
        shutil.make_archive(
            Path(tmp_dir) / Path(S3Label.RKI_DATA.value).stem,
            "zip",
            root_dir=Path(tmp_dir) / Path(country),
        )
        s3_client.upload_file(
            str(Path(tmp_dir) / Path(S3Label.RKI_DATA.value)),
            bucket,
            S3Label.RKI_DATA.value.replace(".", "_" + str(execution_date + ".")),
        )
        print("done")
    end = time.time()
    download_time = end - start
    _log.info("finished download after %.1f seconds", download_time)


if __name__ == "__main__":
    download()
