import logging

import boto3
import click

_log = logging.getLogger(__file__)


@click.command()
@click.option(
    "--name",
    help="name of bucket to create",
    required=True,
    type=str,
)
@click.option("--s3_access_key_id", default="axEhL4FmWR0U54rV", help="Acess key to S3")
@click.option(
    "--s3_access_key_secret", default="ahzh10eFY0P90vIsVbpba6HzK7PPEka9", help="Acess secret to S3"
)
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
def create_s3_bucket(name, s3_access_key_id, s3_access_key_secret, s3_endpoint):
    session = boto3.session.Session()
    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=s3_access_key_id,
        aws_secret_access_key=s3_access_key_secret,
        endpoint_url=s3_endpoint,
    )

    s3_client.create_bucket(Bucket=name)


if __name__ == "__main__":
    create_s3_bucket()
