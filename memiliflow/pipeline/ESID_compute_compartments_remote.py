import os
import sys

sys.path.append(os.getcwd())

import logging
import os
import tempfile
import warnings
from pathlib import Path

import click
import s3
import ssh

warnings.simplefilter(action="ignore", category=FutureWarning)

_log = logging.getLogger(__file__)


@click.command()
@click.option(
    "--start_date", help="start date for which case_data should be fetched", required=True, type=str
)
@click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
)
@click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option("--host", required=True, help="ESID host")
@click.option("--user", required=True, help="user to use for upload on ESID host")
@click.option("--ssh_key_file", required=True, help="ssh_keyfile to ssh into ESID host")
@click.option("--num_days_sim", default=10, help="Number of days to take into account")
@click.option(
    "--image",
    required=True,
    help="which docker image to use to execute ESID_compute_comartments within",
)
def compute_compartments_remote(
    start_date,
    execution_date,
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    host,
    user,
    ssh_key_file,
    num_days_sim,
    image,
):
    s3_client = s3.create_s3_client(s3_access_key_id, s3_access_key_secret, s3_endpoint)
    logging.basicConfig()
    _log.setLevel(logging.INFO)
    with tempfile.TemporaryDirectory(dir="/dependencies") as tmp_dir:
        _log.info(f"writing temporary data to {tmp_dir}")
        remote_ssh_keyfile = ssh_key_file
        local_key_file = Path(tmp_dir) / Path(ssh_key_file)

        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name=remote_ssh_keyfile,
            local_name=local_key_file,
            logger=_log,
            mode=0o600,
        )

        cmd = (
            f"docker pull {image} "
            f"&& "
            f"docker run --rm -t {image} "
            f"python -u memiliflow/pipeline/ESID_compute_compartments.py --start_date {start_date} "
            f"--execution_date {execution_date} "
            f"--num_days_sim {num_days_sim} "
            f"--bucket {bucket} "
            f"--s3_access_key_id {s3_access_key_id} "
            f"--s3_access_key_secret {s3_access_key_secret} "
            f"--s3_endpoint {s3_endpoint}"
        )

        _log.info(f"running: {cmd}")
        ssh.run_ssh_command(cmd=cmd, user=user, host=host, private_key=local_key_file, logger=_log)
    _log.info("done")


if __name__ == "__main__":
    compute_compartments_remote()
