import os
import sys
import warnings

sys.path.append(os.getcwd())

warnings.simplefilter(action="ignore", category=FutureWarning)
import logging
import shutil
import tempfile
from datetime import date
from pathlib import Path

import boto3
import click
import s3
from memilio_src.automatic import (
    prepare_simulation_data_for_backend,
    run_baseline_simulation,
)

_log = logging.getLogger(__file__)


@click.command()
@click.option(
    "--start_date", help="start date for which case_data should be fetched", required=True, type=str
)
@click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
)
@click.option("--s3_access_key_id", required=True, help="Access key to S3")
@click.option("--s3_access_key_secret", required=True, help="Access secret to S3")
@click.option(
    "--s3_endpoint", default="https://s3-loki.jsc.fz-juelich.de", help="url of S3 endpoint"
)
@click.option("--bucket", default="input-database", help="S3 bucket to upload to")
@click.option(
    "--num_days_sim_baseline", default=10, help="Number of days to simulate the baseline for"
)
@click.option(
    "--num_runs",
    default=10_000,
    help="Number of simulation runs (parameters are sampled from valid ranges)",
)
@click.option(
    "--tmpdir",
    default=None,
    help="where to place tmpdir. If none /dependencies is used per default",
)
def ESID_compute_baseline_scenario(
    start_date,
    execution_date,
    s3_access_key_id,
    s3_access_key_secret,
    s3_endpoint,
    bucket,
    num_days_sim_baseline,
    num_runs,
    tmpdir,
):

    logging.basicConfig()
    _log.setLevel(logging.INFO)
    _log.info(
        f"Called {__file__} with arguments --start_date {start_date} --execution_date {execution_date} --bucket {bucket} --num_days_sim {num_days_sim_baseline}"
    )
    s3_client = s3.create_s3_client(s3_access_key_id, s3_access_key_secret, s3_endpoint)
    with tempfile.TemporaryDirectory(dir="/dependencies" if tmpdir is None else tmpdir) as tmp_dir:
        _log.info(f"writing sim data to {tmp_dir}")

        remote_case_data = "daily-data/case_data_" + str(execution_date) + ".tar.gz"
        remote_divi_data = "daily-data/divi_data_" + str(execution_date) + ".tar.gz"
        remote_vaccination_data = "daily-data/vaccination_data_" + str(execution_date) + ".tar.gz"

        local_case_data = str(Path(tmp_dir) / Path("case_data.tar.gz"))
        local_divi_data = str(Path(tmp_dir) / Path("divi_data.tar.gz"))
        local_vaccination_data = str(Path(tmp_dir) / Path("vaccination_data.tar.gz"))

        # fetches case data, DIVI data, vacc data and population data
        _log.info(f"Downloading case data with execution date: {execution_date}")
        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name=remote_case_data,
            local_name=local_case_data,
            logger=_log,
        )
        _log.info(f"Downloading divi data with execution date: {execution_date}")
        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name=remote_divi_data,
            local_name=local_divi_data,
            logger=_log,
        )
        _log.info(f"Downloading vaccination data with execution date: {execution_date}")
        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name=remote_vaccination_data,
            local_name=local_vaccination_data,
            logger=_log,
        )

        # os.mkdir(pydata_dir)
        _log.info(f"Unpacking case data")
        shutil.unpack_archive(local_case_data, extract_dir=tmp_dir)
        _log.info(f"Unpacking divi data")
        shutil.unpack_archive(local_divi_data, extract_dir=tmp_dir)
        _log.info(f"Unpacking vaccination data")
        shutil.unpack_archive(local_vaccination_data, extract_dir=tmp_dir)

        # fetches contact data
        contacts_path = Path(tmp_dir) / Path("contacts.zip")
        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name="cached/" + contacts_path.name,
            local_name=str(contacts_path),
            logger=_log,
        )
        shutil.unpack_archive(str(contacts_path), extract_dir=tmp_dir)

        # fetches mobility data
        mobility_path = Path(tmp_dir) / Path("mobility.zip")
        s3.download(
            s3_client=s3_client,
            bucket=bucket,
            remote_name="cached/" + mobility_path.name,
            local_name=str(mobility_path),
            logger=_log,
        )
        shutil.unpack_archive(str(mobility_path), extract_dir=tmp_dir)

        cwd = os.getcwd()
        os.chdir("memilio_src")
        pydata_dir = str(Path(tmp_dir) / Path("pydata"))
        _log.info(f"Simulating baseline for {pydata_dir}")
        run_baseline_simulation(
            start_date=date.fromisoformat(start_date),
            path_to_data=tmp_dir,
            num_days_sim=num_days_sim_baseline,
            num_runs=num_runs,
        )
        os.chdir(cwd)

        os.chdir(str(Path(*Path(tmp_dir).parts[:2])))

        prepare_simulation_data_for_backend(
            start_date_baseline=date.fromisoformat(start_date),
            num_days_sim_baseline=num_days_sim_baseline,
            path_to_data=Path(*Path(tmp_dir).parts[2:]),
        )
        os.chdir(cwd)

        _log.info(f"Starting upload of simulated scenario")
        s3_client.upload_file(
            str(Path(tmp_dir) / Path("baseline_simulation.zip")),
            bucket,
            "computed/baseline_simulation.zip".replace(".", "_" + str(execution_date) + "."),
        )
        _log.info("done")


if __name__ == "__main__":
    ESID_compute_baseline_scenario()
