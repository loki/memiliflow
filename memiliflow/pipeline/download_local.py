import logging
import shutil
import tempfile
import time
from datetime import date
from pathlib import Path

import click
from memiliflow.data.case_data import CaseData

_log = logging.getLogger(__file__)


@click.command()
@click.option(
    "--data_dir", help="where to store downloaded case_data parquet files", required=True, type=str
)
@click.option("--moving_average", default=7)
@click.option(
    "--start_date", help="start date for which case_data should be fetched", required=True, type=str
)
@click.option(
    "--end_date", help="end date for which case_data should be fetched", required=True, type=str
)
@click.option(
    "--execution_date",
    help="date on which this function was called. E.g. 2020-07-15",
    required=True,
    type=str,
)
@click.option(
    "--file_format",
    default="json_timeasstring",
    required=True,
    type=str,
)
@click.option(
    "--country",
    default="Germany",
    required=True,
    type=str,
)
def download(data_dir, moving_average, start_date, end_date, execution_date, file_format, country):
    logging.basicConfig()
    _log.setLevel(logging.INFO)
    data_dir = Path(data_dir)
    if not data_dir.exists():
        raise FileNotFoundError(f"Data_dir {data_dir} does not exist.")

    output_dir = data_dir / Path(execution_date)
    if output_dir.exists():
        _log.warning("overwriting existing case_data in %s", output_dir)
        shutil.rmtree(output_dir)

    output_dir.mkdir()

    _log.info("start download")
    start = time.time()
    CaseData.download_to_filesystem(
        data_dir=output_dir,
        moving_average=moving_average,
        start_date=date.fromisoformat(start_date),
        end_date=date.fromisoformat(end_date),
        file_format=file_format,
    )
    end = time.time()
    download_time = end - start
    _log.info("finished download after %.1f seconds", download_time)


if __name__ == "__main__":
    download()
