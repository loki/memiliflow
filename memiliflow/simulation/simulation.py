from typing import Callable

import numpy as np
import pytensor.tensor as pt
import torch
from memiliflow.memilio.oseir import Oseir
from memiliflow.memilio.secir import Secir
from memiliflow.memilio.secirvvs import Secirvvs


def wrap_simulator_for_sbi(simulator: Callable):
    def wrapped_simulator(parameters):
        result = simulator(parameters)
        return torch.from_numpy(result)

    return wrapped_simulator


class SimulatorForPyMC(pt.Op):
    itypes = [pt.dvector]  # expects a vector of parameter values when called
    otypes = [pt.dvector]  # outputs a single scalar value (the log likelihood)

    def __init__(self, simulator: Callable):
        self.simulator = simulator

    def perform(self, node, inputs, outputs):
        # the method that is used when calling the Op
        (theta,) = inputs
        result = self.simulator(theta)
        outputs[0][0] = result


def create_oseir_new_cases_simulator(days_to_simulate, total_population):
    """
    Creates OSEIR simulator with known initial condition, no contact damping and 7 parameters:
        parameter[0] -> initial exposed
        parameter[1] -> initial infected
        parameter[2] -> initial recovered
        parameter[3] -> rate of E->I. 1/parameter[0] is time exposed.
        parameter[4] -> rate of I->R. 1/parameter[1] is time infected.
        parameter[5] -> transmission probability on contact. Limited to [0, 1]!
        parameter[6] -> contact rates
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :return:
    """
    _sim = create_oseir_confirmed_cases_simulator(
        days_to_simulate=days_to_simulate, total_population=total_population
    )

    def simulator(parameters):
        result = _sim(parameters=parameters)
        return np.ediff1d(result)

    return simulator


def create_oseir_confirmed_cases_simulator(days_to_simulate, total_population):
    """
    Creates SECIR simulator with known initial condition, no contact damping and 7 parameters:
        parameter[0] -> initial exposed
        parameter[1] -> initial infected
        parameter[2] -> initial recovered
        parameter[3] -> rate of E->I. 1/parameter[0] is time exposed.
        parameter[4] -> rate of I->R. 1/parameter[1] is time infected.
        parameter[5] -> transmission probability on contact. Limited to [0, 1]!
        parameter[6] -> contact rates
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :return:
    """

    _sim = create_oseir_simulator(
        days_to_simulate=days_to_simulate, total_population=total_population
    )

    def simulator(parameters):
        result = _sim(parameters=parameters)
        result["Confirmed"] = result["Infected"] + result["Recovered"]
        result = result["Confirmed"].to_numpy()
        return result

    return simulator


def create_oseir_simulator(days_to_simulate, total_population):
    """
    Creates OSEIR simulator with known initial condition, no contact damping and 7 parameters:
        parameter[0] -> initial exposed
        parameter[1] -> initial infected
        parameter[2] -> initial recovered
        parameter[3] -> rate of E->I. 1/parameter[0] is time exposed.
        parameter[4] -> rate of I->R. 1/parameter[1] is time infected.
        parameter[5] -> transmission probability on contact. Limited to [0, 1]!
        parameter[6] -> contact rates
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :return:
    """

    def simulator(parameters):
        initial_condition = {
            Oseir.Compartment.SUSCEPTIBLE: total_population
            - parameters[0]
            - parameters[1]
            - parameters[2],
            Oseir.Compartment.EXPOSED: parameters[0],
            Oseir.Compartment.INFECTED: parameters[1],
            Oseir.Compartment.RECOVERED: parameters[2],
        }

        param_dict = {
            Oseir.Parameter.TIME_EXPOSED: 1.0 / parameters[3],
            Oseir.Parameter.TIME_INFECTED: 1.0 / parameters[4],
            Oseir.Parameter.TRANSMISSION_PROBABILITY: 1,
            Oseir.Parameter.CONTACT_BASELINE: parameters[5],
            Oseir.Parameter.CONTACT_MINIMUM: 0.0,
            Oseir.Parameter.CONTACT_DAMPING: 0.0,
        }

        result = Oseir.simulate_dicts(
            initial_condition=initial_condition, parameters=param_dict, duration=days_to_simulate
        )
        return result

    return simulator


def create_secir_new_cases_simulator(days_to_simulate, total_population):
    """
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :return:
    """
    _sim = create_secir_confirmed_cases_simulator(
        days_to_simulate=days_to_simulate, total_population=total_population
    )

    def simulator(parameters):
        result = _sim(parameters=parameters)
        return np.ediff1d(result)

    return simulator


def create_secir_confirmed_cases_simulator(days_to_simulate, total_population):
    """
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :return:
    """

    _sim = create_secir_simulator(
        days_to_simulate=days_to_simulate, total_population=total_population
    )

    def simulator(parameters):
        result = _sim(parameters=parameters)

        result["Confirmed"] = (
            result["InfectedNoSymptomsConfirmed"]
            + result["InfectedSymptomsConfirmed"]
            + result["InfectedSevere"]
            + result["InfectedCritical"]
            + result["Recovered"]
            + result["Dead"]
        )
        result = result["Confirmed"].to_numpy()
        return result

    return simulator


def create_secir_simulator(days_to_simulate, total_population, serial_interval=4.2675):
    """
    Creates SECIR simulator with known initial condition, no contact damping and 7 parameters:
        parameter[0] -> initial exposed
        parameter[1] -> initial infected no symptoms
        parameter[2] -> initial infected symptoms
        parameter[3] -> initial infected severe
        parameter[4] -> initial infected critical
        parameter[5] -> initial recovered
        parameter[6] -> initial dead
        parameter[7] -> incubation time
        parameter[8] -> time infected no symptoms
        parameter[9] -> time infected symptoms
        parameter[10] -> time infected severe
        parameter[11] -> time infected critical
        parameter[12] -> relative transmission no symptoms
        parameter[13] -> transmission probability on contact
        parameter[14] -> recovered per infected no symptoms
        parameter[15] -> risk of infection from symptomatic
        parameter[16] -> severe per infected symptoms
        parameter[17] -> critical per severe
        parameter[18] -> deaths per critical
        parameter[19] -> deaths per critical
        parameter[20] -> contact rate
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :param serial_interval: this parameter should be fixed. Default is mean of range in from our literature study, see
                            https://codebase.helmholtz.cloud/loki/parameter-research-ode-models/-/blob/main/LOKI_SECIR_parameter_20231130.xlsx?ref_type=heads
    :return:
    """

    def simulator(parameters):
        # TODO fix hard coded age group 0
        initial_condition = {
            0: {
                Secir.Compartment.SUSCEPTIBLE: total_population
                - parameters[0]
                - parameters[1]
                - parameters[2],
                Secir.Compartment.EXPOSED: parameters[0],
                Secir.Compartment.INFECTED_NO_SYMPTOMS: parameters[1],
                Secir.Compartment.INFECTED_SYMPTOMS: parameters[2],
                Secir.Compartment.INFECTED_SEVERE: parameters[3],
                Secir.Compartment.INFECTED_CRITICAL: parameters[4],
                Secir.Compartment.RECOVERED: parameters[5],
                Secir.Compartment.DEAD: parameters[6],
            }
        }

        # TODO fix hard coded age group 0
        param_dict = {
            0: {
                Secir.Parameter.TIME_EXPOSED: parameters[7],
                Secir.Parameter.TIME_INFECTED_NO_SYMPTOMS: parameters[8],
                Secir.Parameter.TIME_INFECTED_SYMPTOMS: parameters[9],
                Secir.Parameter.TIME_INFECTED_SEVERE: parameters[10],
                Secir.Parameter.TIME_INFECTED_CRITICAL: parameters[11],
                Secir.Parameter.RELATIVE_TRANSMISSION_NO_SYMPTOMS: parameters[12],
                Secir.Parameter.TRANSMISSION_PROBABILITY: parameters[13],
                Secir.Parameter.RECOVERED_PER_INFECTED_NO_SYMPTOMS: parameters[14],
                Secir.Parameter.RISK_OF_INFECTION_FROM_SYMPTOMATIC: parameters[15],
                Secir.Parameter.SEVERE_PER_INFECTED_SYMPTOMS: parameters[16],
                Secir.Parameter.CRITICAL_PER_SEVERE: parameters[17],
                Secir.Parameter.DEATHS_PER_CRITICAL: parameters[18],
                Secir.Parameter.MAX_RISK_OF_INFECTION_FROM_SYMPTOMATIC: parameters[19],
                Secir.Parameter.CONTACT_MATRIX: parameters[20],
            }
        }

        # Only 1 age group for now
        return Secir.simulate_dicts(
            initial_condition=initial_condition, parameters=param_dict, duration=days_to_simulate
        )

    return simulator


def create_secirvvs_new_cases_simulator(days_to_simulate, total_population):
    """
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :return:
    """
    _sim = create_secirvvs_confirmed_cases_simulator(
        days_to_simulate=days_to_simulate, total_population=total_population
    )

    def simulator(parameters):
        result = _sim(parameters=parameters)
        return np.ediff1d(result)

    return simulator


def create_secirvvs_confirmed_cases_simulator(days_to_simulate, total_population):
    """
    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :return:
    """

    _sim = create_secirvvs_simulator(
        days_to_simulate=days_to_simulate, total_population=total_population
    )

    def simulator(parameters):
        result = _sim(parameters=parameters)

        result["Confirmed"] = (
            result["InfectedNoSymptomsConfirmed"]
            + result["InfectedSymptomsConfirmed"]
            + result["InfectedSevere"]
            + result["InfectedCritical"]
            + result["Recovered"]
            + result["Dead"]
        )
        result = result["Confirmed"].to_numpy()
        return result

    return simulator


def create_secirvvs_simulator(days_to_simulate, total_population):
    """
    Creates SECIRVVS simulator with known initial condition, no contact damping and 7 parameters:
        parameter[0] -> initial susceptible partial immunity
        parameter[1] -> initial susceptible improved immunity
        parameter[2] -> initial exposed naive
        parameter[3] -> initial exposed partial immunity
        parameter[4] -> initial exposed improved immunity
        parameter[5] -> initial infected no symptoms naive
        parameter[6] -> initial infected no symptoms partial immunity
        parameter[7] -> initial infected no symptoms improved immunity
        parameter[8] -> initial infected symptoms naive
        parameter[9] -> initial infected symptoms partial immunity
        parameter[10] -> initial infected symptoms improved immunity
        parameter[11] -> initial infected severe naive
        parameter[12] -> initial infected severe partial immunity
        parameter[13] -> initial infected severe improved immunity
        parameter[14] -> initial infected critical naive
        parameter[15] -> initial infected critical partial immunity
        parameter[16] -> initial infected critical improved immunity
        parameter[17] -> initial dead naive
        parameter[18] -> initial dead partial immunity
        parameter[19] -> initial dead improved immunity
        parameter[20] -> time exposed
        parameter[21] -> time infected no symptoms
        parameter[22] -> time infected symptoms
        parameter[23] -> time infected severe
        parameter[24] -> time infected critical
        parameter[25] -> transmission probability on contact
        parameter[26] -> relative transmission no symptoms
        parameter[27] -> risk of infection from symptomatic
        parameter[28] -> recovered per infected no symptoms
        parameter[29] -> severe per infected symptoms
        parameter[30] -> critical per infected severe
        parameter[31] -> deaths per critical infected
        parameter[32] -> reduced exposed partial immunity
        parameter[33] -> reduced exposed improved immunity
        parameter[34] -> reduced time infected mild
        parameter[35] -> reduced infected symptoms partial immunity
        parameter[36] -> reduced infected symptoms improved immunity
        parameter[37] -> reduced infected severe partial immunity
        parameter[38] -> reduced infected severe improved immunity
        parameter[39] -> reduced infected critical partial immunity
        parameter[41] -> reduced infected critical improved immunity
        parameter[42] -> contact scaling

    :param days_to_simulate: how long to simulate
    :param total_population: total number of people in all compartments
    :param serial_interval: this parameter should be fixed. Default is mean of range in from our literature study, see
                            https://codebase.helmholtz.cloud/loki/parameter-research-ode-models/-/blob/main/LOKI_SECIR_parameter_20231130.xlsx?ref_type=heads
    :return:
    """

    def simulator(parameters):
        # TODO fix hard coded age group 0
        initial_condition = {
            0: {
                Secirvvs.Compartment.SUSCEPTIBLE: total_population
                - parameters[0]
                - parameters[1]
                - parameters[2],
                Secirvvs.Compartment.EXPOSED: parameters[0],
                Secirvvs.Compartment.INFECTED_NO_SYMPTOMS: parameters[1],
                Secirvvs.Compartment.INFECTED_SYMPTOMS: parameters[2],
                Secirvvs.Compartment.INFECTED_SEVERE: parameters[3],
                Secirvvs.Compartment.INFECTED_CRITICAL: parameters[4],
                Secirvvs.Compartment.RECOVERED: parameters[5],
                Secirvvs.Compartment.DEAD: parameters[6],
            }
        }

        # TODO fix hard coded age group 0
        param_dict = {
            0: {
                Secirvvs.Parameter.TIME_EXPOSED: parameters[7],
                Secirvvs.Parameter.TIME_INFECTED_NO_SYMPTOMS: parameters[8],
                Secirvvs.Parameter.TIME_INFECTED_SYMPTOMS: parameters[9],
                Secirvvs.Parameter.TIME_INFECTED_SEVERE: parameters[10],
                Secirvvs.Parameter.TIME_INFECTED_CRITICAL: parameters[11],
                Secirvvs.Parameter.TRANSMISSION_PROBABILITY: parameters[12],
                Secirvvs.Parameter.TRANSMISSION_PROBABILITY: parameters[12],
                Secirvvs.Parameter.RECOVERED_PER_INFECTED_NO_SYMPTOMS: parameters[13],
                Secirvvs.Parameter.RISK_OF_INFECTION_FROM_SYMPTOMATIC: parameters[14],
                Secirvvs.Parameter.SEVERE_PER_INFECTED_SYMPTOMS: parameters[15],
                Secirvvs.Parameter.CRITICAL_PER_SEVERE: parameters[16],
                Secirvvs.Parameter.DEATHS_PER_CRITICAL: parameters[17],
                Secirvvs.Parameter.CONTACT_MATRIX: parameters[18],
            }
        }

        # Only 1 age group for now
        return Secirvvs.simulate_dicts(
            initial_condition=initial_condition, parameters=param_dict, duration=days_to_simulate
        )

    return simulator
