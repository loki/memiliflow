import os


def thread_pid_id(obj: object) -> str:
    """A process- and thread- specific identifier of an object."""
    return f"{id(obj)}-{os.getpid()}"  # -{threading.get_ident()}"
