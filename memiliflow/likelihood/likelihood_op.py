import logging

import numpy as np
import pandas as pd
import pytensor.tensor as pt
from memiliflow.data.case_data import CaseData
from memiliflow.likelihood.oseir_likelihood import (  # make_pyross_oseir_likelihood,
    make_memilio_oseir_SDE_approx_likelihood,
    make_py_oseir_SDE_approx_likelihood,
)
from memiliflow.memilio.oseir import Oseir

_log = logging.getLogger(__file__)


class LikelihoodOseir(pt.Op):
    itypes = [pt.dvector]  # expects a vector of parameter values when called
    otypes = [pt.dscalar]  # outputs a single scalar value (the log likelihood)

    def __init__(self, case_data: pd.DataFrame, total_population):
        self.case_data = case_data
        self.total_population = total_population

    def perform(self, node, inputs, outputs):
        # the method that is used when calling the Op
        (theta,) = inputs  # this will contain my variables

        compartments = {
            Oseir.Compartment.SUSCEPTIBLE: self.total_population - theta[0] - theta[1] - theta[2],
            Oseir.Compartment.EXPOSED: theta[0],
            Oseir.Compartment.INFECTED: theta[1],
            Oseir.Compartment.RECOVERED: theta[2],
        }

        parameters = {
            Oseir.Parameter.TIME_EXPOSED: theta[3],
            Oseir.Parameter.TIME_INFECTED: theta[4],
            Oseir.Parameter.TRANSMISSION_PROBABILITY: theta[5],
        }
        contact_patterns = {
            Oseir.Conctact.BASELINE: np.array(theta[6]),
        }

        model = Oseir.setup_model(
            parameters=parameters, compartments=compartments, contact_patterns=contact_patterns
        )
        logl = Oseir.likelihood(
            model,
            case_data=self.case_data,
            detection_delay_confirmed=theta[7],
            detection_delay_confirmed_std=theta[8],
        )

        outputs[0][0] = np.array(logl)  # output the log-likelihood


# define a pytensor Op for our likelihood function
class LikelihoodOpForPyMC(pt.Op):
    """
    see https://www.pymc.io/projects/examples/en/latest/case_studies/blackbox_external_likelihood_numpy.html
    Specify what type of object will be passed and returned to the Op when it is
    called. In our case we will be passing it a vector of values (the parameters
    that define our model) and returning a single "scalar" value (the
    log-likelihood)
    """

    itypes = [pt.dvector]  # expects a vector of parameter values when called
    otypes = [pt.dscalar]  # outputs a single scalar value (the log likelihood)

    def __init__(self, loglike):
        """
        Initialise the Op with various things that our log-likelihood function
        requires. Below are the things that are needed in this particular
        example.

        Parameters
        ----------
        loglike:
            The log-likelihood (or whatever) function we've defined
        model:
            The "observed" data that our log-likelihood function takes in
        x:
            The dependent variable (aka 'x') that our model requires
        sigma:
            The noise standard deviation that our function requires.
        """

        # add inputs as class attributes
        self.likelihood = loglike

    def perform(self, node, inputs, outputs):
        # the method that is used when calling the Op
        (theta,) = inputs  # this will contain my variables

        # call the log-likelihood function
        logl = self.likelihood(theta)

        outputs[0][0] = np.array(logl)  # output the log-likelihood


class MemilioOseirLikelihoodOpForHopsy:
    def __init__(self, priors, model, observations):
        self._t_exposed_mean = priors[Oseir.Parameter.TIME_EXPOSED]["mean"]
        self._t_exposed_sigma = priors[Oseir.Parameter.TIME_EXPOSED]["sigma"]
        self._t_infected_mean = priors[Oseir.Parameter.TIME_INFECTED]["mean"]
        self._t_infected_sigma = priors[Oseir.Parameter.TIME_INFECTED]["sigma"]
        self._transmission_p_mean = priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["mean"]
        self._transmission_p_sigma = priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["sigma"]

        self.start = np.array(
            [
                [self._t_exposed_mean],
                [self._t_infected_mean],
                [self._transmission_p_mean],
            ]
        )
        self.model = model
        self.observations = observations
        self.likelihood_fun = make_memilio_oseir_likelihood(
            self.model, observations=self.observations
        )

    def __getstate__(self):
        attributes = self.__dict__.copy()
        del attributes["likelihood_fun"]
        return attributes

    def __setstate__(self, state):
        self.__dict__ = state
        self.likelihood_fun = make_memilio_oseir_likelihood(
            self.model, observations=self.observations
        )

    def compute_negative_log_likelihood(self, x):
        # TODO FJ test whether we have to recreate this every time
        likelihood_fun = make_memilio_oseir_SDE_approx_likelihood(
            self.model, observations=self.observations
        )
        t_exposed_prior = np.exp(
            -((x[0] - self._t_exposed_mean) ** 2) / self._t_exposed_sigma**2 / 2
        ) / (np.sqrt(2 * np.pi))
        t_infected_prior = np.exp(
            -((x[1] - self._t_exposed_mean) ** 2) / self._t_infected_sigma**2 / 2
        )
        transmission_prior = np.exp(
            -((x[1] - self._t_exposed_mean) ** 2) / self._transmission_p_sigma**2 / 2
        )

        sig = self._t_exposed_sigma
        mu = self._t_exposed_mean
        y = x[0]
        t_exposed_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )
        sig = self._t_infected_sigma
        mu = self._t_infected_mean
        y = x[1]
        t_infected_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )
        sig = self._transmission_p_sigma
        mu = self._transmission_p_mean
        y = x[2]
        transmission_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )

        # TODO FJ: readd prior terms?
        # return t_exposed_prior + t_infected_prior + transmission_prior - likelihood_fun(x)
        return -self.likelihood_fun(x)


class MemilioPyOseirLikelihoodOpForHopsy:
    def __init__(self, priors, model, observations):
        self._t_exposed_mean = priors[Oseir.Parameter.TIME_EXPOSED]["mean"]
        self._t_exposed_sigma = priors[Oseir.Parameter.TIME_EXPOSED]["sigma"]
        self._t_infected_mean = priors[Oseir.Parameter.TIME_INFECTED]["mean"]
        self._t_infected_sigma = priors[Oseir.Parameter.TIME_INFECTED]["sigma"]
        self._transmission_p_mean = priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["mean"]
        self._transmission_p_sigma = priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["sigma"]

        self.start = np.array(
            [
                [self._t_exposed_mean],
                [self._t_infected_mean],
                [self._transmission_p_mean],
            ]
        )
        self.model = model
        self.observations = observations

    def compute_negative_log_likelihood(self, x):
        # TODO FJ test whether we have to recreate this every time
        likelihood_fun = make_py_oseir_SDE_approx_likelihood(
            self.model, observations=self.observations
        )
        t_exposed_prior = np.exp(
            -((x[0] - self._t_exposed_mean) ** 2) / self._t_exposed_sigma**2 / 2
        ) / (np.sqrt(2 * np.pi))
        t_infected_prior = np.exp(
            -((x[1] - self._t_exposed_mean) ** 2) / self._t_infected_sigma**2 / 2
        )
        transmission_prior = np.exp(
            -((x[1] - self._t_exposed_mean) ** 2) / self._transmission_p_sigma**2 / 2
        )

        sig = self._t_exposed_sigma
        mu = self._t_exposed_mean
        y = x[0]
        t_exposed_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )
        sig = self._t_infected_sigma
        mu = self._t_infected_mean
        y = x[1]
        t_infected_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )
        sig = self._transmission_p_sigma
        mu = self._transmission_p_mean
        y = x[2]
        transmission_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )

        # TODO FJ: readd prior terms?
        # return t_exposed_prior + t_infected_prior + transmission_prior - likelihood_fun(x)
        return -likelihood_fun(x)


class PyrossOseirLikelihoodOpForHopsy:
    def __init__(self, priors, Tf, observations):
        self._t_exposed_mean = priors[Oseir.Parameter.TIME_EXPOSED]["mean"]
        self._t_exposed_sigma = priors[Oseir.Parameter.TIME_EXPOSED]["sigma"]
        self._t_infected_mean = priors[Oseir.Parameter.TIME_INFECTED]["mean"]
        self._t_infected_sigma = priors[Oseir.Parameter.TIME_INFECTED]["sigma"]
        self._transmission_p_mean = priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["mean"]
        self._transmission_p_sigma = priors[Oseir.Parameter.TRANSMISSION_PROBABILITY]["sigma"]

        self.start = np.array(
            [
                [self._t_exposed_mean],
                [self._t_infected_mean],
                [self._transmission_p_mean],
            ]
        )
        self.Tf = Tf
        self.observations = observations

    def compute_negative_log_likelihood(self, x):
        # TODO FJ test whether we have to recreate this every time
        likelihood_fun = make_pyross_oseir_likelihood(
            observations=self.observations, Tf_inference=self.Tf
        )
        t_exposed_prior = np.exp(
            -((x[0] - self._t_exposed_mean) ** 2) / self._t_exposed_sigma**2 / 2
        ) / (np.sqrt(2 * np.pi))
        t_infected_prior = np.exp(
            -((x[1] - self._t_exposed_mean) ** 2) / self._t_infected_sigma**2 / 2
        )
        transmission_prior = np.exp(
            -((x[1] - self._t_exposed_mean) ** 2) / self._transmission_p_sigma**2 / 2
        )

        sig = self._t_exposed_sigma
        mu = self._t_exposed_mean
        y = x[0]
        t_exposed_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )
        sig = self._t_infected_sigma
        mu = self._t_infected_mean
        y = x[1]
        t_infected_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )
        sig = self._transmission_p_sigma
        mu = self._transmission_p_mean
        y = x[2]
        transmission_prior = (
            -np.log(1.0 / (np.sqrt(2.0 * np.pi) * sig)) + np.power((y - mu) / sig, 2.0) / 2
        )

        # TODO FJ: readd prior terms?
        # return t_exposed_prior + t_infected_prior + transmission_prior - likelihood_fun(x)
        return -likelihood_fun(x)
