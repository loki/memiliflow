import logging
import time
from typing import Sequence

import numpy as np
import pyross
from memiliflow.data.case_data import CaseData
from memiliflow.memilio.oseir import Oseir
from memilio.simulation import TimeSeries
from pyross.utils import solve_symmetric_close_to_singular
from scipy.integrate import solve_ivp

_log = logging.getLogger(__file__)


def _estimate_cond_cov(ti, tf, model: Oseir, steps=4, rtol=1e-4, ode_solver_method="LSODA"):
    def rhs(t, sig_vec):
        jac = model.compute_jacobian(t)
        B = model.compute_noise_correlation(t)
        dsigdt = (
            np.dot(jac, sig_vec.reshape((jac.shape[0], jac.shape[1])))
            + np.dot(sig_vec.reshape((jac.shape[0], jac.shape[1])), jac.T)
            + B
        )
        return dsigdt.reshape(sig_vec.shape)

    res = solve_ivp(
        fun=rhs,
        t_span=(ti, tf),
        y0=np.zeros(model.nCompartments**2),
        t_eval=[tf],
        method=ode_solver_method,
        first_step=(tf - ti) / steps,
        max_step=(tf - ti) / steps,
        rtol=rtol,
    )
    covariances = res.y[:, 0]
    return covariances


def make_py_oseir_SDE_approx_likelihood(
    model: Oseir, observations: np.ndarray, ode_solver_method="LSODA", simulation_step=1e-1
):
    """Creates a likelihood function for memilio in python (slow)"""
    start_index = 1

    def logp_func(parameters: Sequence[np.ndarray]) -> np.ndarray:
        # Names are from  memilio InfectionState (not available in python bindings yet)
        parameter_names = [
            Oseir.Parameter.TIME_EXPOSED,
            Oseir.Parameter.TIME_INFECTED,
            Oseir.Parameter.TRANSMISSION_PROBABILITY,
        ]
        ts = observations[0, :]

        if np.any(np.isnan(parameters)) or not np.all(np.isfinite(parameters)):
            return np.finfo(np.float64).min
        if np.any(np.where(parameters < 0)):
            return np.finfo(np.float64).min

        logp = 0.0
        for i in range(len(ts) - 1):
            obs = observations[start_index:, i]
            next_obs = observations[start_index:, i + 1]
            t = ts[i]
            next_t = ts[i + 1]

            pred = model.simulate(
                parameters=parameters,
                parameter_names=parameter_names,
                interpolation_target=np.array([next_t]),
                simulation_duration=next_t,
                t0=t,
                y0=obs,
                simulation_step=simulation_step,
            )

            cov = _estimate_cond_cov(t, next_t, model, ode_solver_method=ode_solver_method)
            deviation = np.subtract(pred[start_index:, :].flatten(), next_obs)

            cov = cov.reshape((model.nCompartments, model.nCompartments))

            inv_cov_x, log_determinant = solve_symmetric_close_to_singular(cov, deviation)

            logp += -np.dot(deviation, inv_cov_x) / 2 - log_determinant / 2

        return logp

    return logp_func


def make_memilio_oseir_SDE_approx_likelihood(model: Oseir, observations: np.ndarray):
    """Creates a likelihood function for memilio using memilio python bindings (fast)"""
    time_index = 0
    num_compartments = 4

    def logp_func(parameters: Sequence[np.ndarray]) -> np.float64:
        # start = time.perf_counter()
        if np.any(np.isnan(parameters)) or not np.all(np.isfinite(parameters)):
            return np.finfo(np.float64).min
        if np.any(np.where(parameters < 0)):
            return np.finfo(np.float64).min
        # Names are from memilio InfectionState (not available in python bindings yet)
        parameter_names = [
            Oseir.Parameter.TIME_EXPOSED,
            Oseir.Parameter.TIME_INFECTED,
            Oseir.Parameter.TRANSMISSION_PROBABILITY,
        ]

        timeseries = TimeSeries(num_compartments)
        for i in range(observations.shape[1]):
            timeseries.add_time_point(
                observations[time_index, i], observations[time_index + 1 :, i]
            )

        val = model.likelihood(timeseries, parameter_names=parameter_names, parameters=parameters)
        # end = time.perf_counter()
        # _log.info(f'execution time {end-start}')
        return val

    return logp_func


def make_pyross_oseir_SDE_approx_likelihood(
    observations: np.ndarray, Tf_inference=None, C=np.array([[1.0]])
):
    model_spec = {
        "classes": ["S", "E", "I", "R"],
        "S": {
            "infection": [["I", "S", "-beta"]],
        },
        "E": {"linear": [["E", "-gE"]], "infection": [["I", "S", "beta"]]},
        "I": {
            "linear": [
                ["E", "gE"],
                ["I", "-gI"],
            ],
        },
        "R": {
            "linear": [["I", "gI"]],
        },
    }

    inference_parameters = {"beta": 1.0, "gE": 1.0, "gI": 1.0}
    total_population = np.sum(observations[1:, 0])
    estimator = pyross.inference.Model(
        model_spec, inference_parameters, 1, total_population * np.ones(1)
    )

    observations = observations[1:, :].transpose()

    def contactMatrix(t):
        return C

    def logp_func(parameters: Sequence[np.ndarray]) -> np.ndarray:
        parameters_dict = {
            "gE": 1.0 / parameters[0],
            "gI": 1.0 / parameters[1],
            "beta": parameters[2],
        }
        logp = estimator.obtain_minus_log_p(
            parameters_dict, observations, Tf_inference, contactMatrix
        )
        return -logp

    return logp_func


def make_py_oseir_likelihood(model: Oseir, observations: CaseData):
    """Creates a likelihood function for memilio in python using an observation model"""
    print(observations.data.keys())
    print(" ")
    print(observations.data)
    confirmed = observations.data["cases_all_germany"]

    def logp_func(parameters: Sequence[np.ndarray]) -> np.ndarray:
        # # Names are from  memilio InfectionState (not available in python bindings yet)
        # parameter_names = [
        #     Oseir.Parameter.TIME_EXPOSED,
        #     Oseir.Parameter.TIME_INFECTED,
        #     Oseir.Parameter.TRANSMISSION_PROBABILITY,
        #     Oseir.Parameter.E0,
        #     Oseir.Parameter.I0,
        #     Oseir.Parameter.R0,
        # ]
        # ts = observations[0, :]
        #
        # if np.any(np.isnan(parameters)) or not np.all(np.isfinite(parameters)):
        #     return np.finfo(np.float64).min
        # if np.any(np.where(parameters < 0)):
        #     return np.finfo(np.float64).min
        # model.set_parameters(parameter_names, parameters)
        # pred = model.simulate(t0=ts[0], simulation_duration=ts[-1], interpolation_target=ts)
        logp = 0
        return logp

    return logp_func
