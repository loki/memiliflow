#/bin/bash

while getopts c: flag
do
  case "${flag}" in
    c) MEMILIO_COMMIT_HASH=${OPTARG};;
  esac
done

mkdir memilio_src
cd memilio_src
# Downloads the "memilio" pandemic simulation package by DLR
echo "fetching memilio with commit ${MEMILIO_COMMIT_HASH}"
git config --global init.defaultBranch main && \
    git init && \
    git remote add origin https://github.com/DLR-SC/memilio.git && \
    # git remote add origin	https://github.com/qacwnfq/memilio.git && \
    git fetch --depth 1 origin ${MEMILIO_COMMIT_HASH} && \
    git checkout FETCH_HEAD

mkdir build

cd build
cmake -DCMAKE_BUILD_TYPE=Release ../cpp
cmake --build . --target generate_extrapolated simulate_scenario -j4
echo rm _deps -rf && rm _deps -rf
cd ..
echo "Finished compiling generate_graph_from_data"

# Installs python bindings for memilio-epidata
cd pycode/memilio-epidata
python3 -m pip install -r requirements-dev.txt
python3 setup.py bdist_wheel
python3 -m pip install dist/* --force-reinstall
echo "Finished compiling MEmilio-epidata"
# Installs python bindings for memilio-simulation
cd ../memilio-simulation
python3 -m pip install -r requirements-dev.txt
python3 setup.py bdist_wheel
echo rm _skbuild/linux-x86_64-*/cmake-build/_deps -rf && rm _skbuild/linux-x86_64-*/cmake-build/_deps -rf
python3 -m pip install dist/* --force-reinstall
echo "Finished compiling MEmilio-simulation"
