#!/bin/bash

docker ps -a | grep memilio_manylinux_build_env && (docker stop memilio_manylinux_build_env; docker rm memilio_manylinux_build_env)

docker run -d -t --name memilio_manylinux_build_env -v $(pwd):/io quay.io/pypa/manylinux2014_x86_64

for py in "cp312-cp312";
do
#    docker exec memilio_manylinux_build_env /bin/bash -c  \
#        "/opt/python/"$py"/bin/pip install -r /io/requirements.txt"
    docker exec memilio_manylinux_build_env /bin/bash -c  "ls /io/memilio_src/pycode/ -la"
    docker exec memilio_manylinux_build_env /bin/bash -c  \
        "/opt/python/"$py"/bin/pip wheel --no-deps /io/memilio_src/pycode/memilio-simulation/ -w /io/memilio_src/pycode/memilio-simulation/dist/"
    docker exec memilio_manylinux_build_env /bin/bash -c  \
        "auditwheel repair /io/memilio_src/pycode/memilio-simulation/dist/* -w /io/memilio_src/pycode/memilio-simulation/dist/"
done

docker stop memilio_manylinux_build_env
docker rm memilio_manylinux_build_env

# upload with
#      python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
